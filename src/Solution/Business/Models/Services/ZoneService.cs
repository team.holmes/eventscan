﻿using System;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Linq;
using MyApp.Business.Resource;
using OpenPlatform.General.Encryption;
using OpenPlatform.General.Extensions;
using MyApp.Business.DomainObjects.Models;
using OpenPlatform.General.Dal;
using OpenPlatform.General.Model;
using System.Web;
using System.Collections.Generic;

namespace MyApp.Business.Services
{
    public class ZoneService : IZoneService
    {
        private IRepository _Repository;
        private IConfiguration _Configuration;
        private ILog _Log;

        public ZoneService(IConfiguration configuration, ILog log, IRepository repository)
        {
            _Configuration = configuration;
            _Log = log;
            _Repository = repository;
        }


        public List<Zone> GetZonesOrderedByName(int userprofileid, Boolean inclueInactive)
        {
            _Log.Debug(string.Format("GetZonesOrderedByName {0} | {1}", userprofileid, inclueInactive));

            if (inclueInactive)
            {
                return _Repository.GetFiltered<Zone>(x => x.UserProfileId == userprofileid).OrderBy(x => x.Name).ToList();
            }

            return _Repository.GetFiltered<Zone>(x => x.UserProfileId == userprofileid && x.IsActive == true).OrderBy(x => x.Name).ToList();

        }

        public Zone GetZoneById(int zoneid)
        {
            _Log.Debug(string.Format("GetZoneById {0} {1}", zoneid, ""));

            return _Repository.GetById<Zone>(zoneid);
        }

        public void DeleteByZoneId(int zoneid)
        {
            _Log.Debug(string.Format("DeleteByZoneId {0} {1}", zoneid, ""));

            _Repository.DeleteById<Zone>(zoneid);

        }

        public int GetTotalEngagementsByZoneId(int zoneid)
        {
            _Log.Debug(string.Format("GetTotalEngagementsByZoneId {0} {1}", zoneid, ""));

            return _Repository.GetFiltered<ZoneEngagement>(x => x.ZoneId == zoneid).Count();
        }

        public int GetTotalUnregisteredEngagementsByZoneId(int zoneid)
        {
            _Log.Debug(string.Format("GetTotalUnregisteredEngagementsByZoneId {0} {1}", zoneid, ""));

            return _Repository.GetFiltered<ZoneEngagement>(x => x.ZoneId == zoneid && !x.UserProfileId.HasValue).Count();
        }


        public int GetTotalRegisteredEngagementsByZoneId(int zoneid)
        {
            _Log.Debug(string.Format("GetTotalRegisteredEngagementsByZoneId {0} {1}", zoneid, ""));

            return _Repository.GetFiltered<ZoneEngagement>(x => x.ZoneId == zoneid && x.UserProfileId.HasValue).Count();
        }

        public BusinessEnum.ZoneAdd Add(Zone zone)
        {
            _Log.Debug(string.Format("Add {0} {1}", zone.Name, ""));

            if (DoesZoneNameAlreadyExist(zone.Name, zone.UserProfileId))
            {
                return BusinessEnum.ZoneAdd.DuplicateName;
            }

            int addeddbid = _Repository.Add<Zone>(zone);

            if (addeddbid > 0)
            {
                Zone newlyaddedzone = GetZoneById(addeddbid);
                string uniqueidentifier = string.Format("{0}{1}", _Configuration.QRCodeZonePrefix, addeddbid);
                newlyaddedzone.Identifier = uniqueidentifier.Encrypt();
                Update(newlyaddedzone);

                return BusinessEnum.ZoneAdd.Success;
            }

            return BusinessEnum.ZoneAdd.Fail;
        }

        public BusinessEnum.ZoneAdd Update(Zone zone)
        {
            _Log.Debug(string.Format("Update {0} {1}", zone.Identifier, ""));

            Zone zonewithsamename = _Repository.GetFiltered<Zone>(x => x.UserProfileId == zone.UserProfileId && zone.IsActive == true && x.Name.Equals(zone.Name, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (zonewithsamename != null && zonewithsamename.Id > 0 && zonewithsamename.Id != zone.Id)
            {
                return BusinessEnum.ZoneAdd.DuplicateName;
            }

            if (_Repository.Update<Zone>(zone.Id, zone))
            {
                return BusinessEnum.ZoneAdd.Success;

            }

            return BusinessEnum.ZoneAdd.Fail;
        }

        public Boolean DoesZoneNameAlreadyExist(string zonename, int userprofileID)
        {
            _Log.Debug(string.Format("DoesZoneNameAlreadyExist {0} {1}", zonename, userprofileID));

            if (_Repository.GetFiltered<Zone>(x => x.UserProfileId == userprofileID && x.IsActive == true && x.Name.Equals(zonename, StringComparison.InvariantCultureIgnoreCase)).Any())
            {
                return true;
            }
            return false;
        }

        public List<ZoneEngagement> GetEngagementsByZone(int zoneid)
        {
            _Log.Debug(string.Format("GetEngagementsByZone {0} {1}", zoneid, ""));

            return _Repository.GetFiltered<ZoneEngagement>(x => x.ZoneId == zoneid).OrderBy(x => x.DateAdded).ToList();
        }


        private Boolean IsNumberUnregisteredRequestedUnderPerRequestAmount(int numrequested)
        {
            _Log.Debug(string.Format("IsNumberUnregistredRequestedAbovePerRequestAmount {0} {1}", numrequested, ""));
            if (numrequested <= _Configuration.MaxUnregisteredZonesAllowedPerRequest)
            {
                return true;
            }

            return false;
        }




    }
}
