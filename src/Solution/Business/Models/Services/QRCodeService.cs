﻿using System;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Linq;
using MyApp.Business.Resource;
using OpenPlatform.General.Encryption;
using OpenPlatform.General.Extensions;
using MyApp.Business.DomainObjects.Models;
using OpenPlatform.General.Dal;
using OpenPlatform.General.Model;
using System.Web;
using System.Collections.Generic;
using MessagingToolkit.QRCode;
using MessagingToolkit.QRCode.Codec;
using System.Drawing.Imaging;
using Gma.QrCodeNet;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System.Drawing;
using System.IO;
using Aspose.Words;
using Aspose.Words.Tables;
using Ionic.Zip;

namespace MyApp.Business.Services
{
    public class QRCodeService : IQRCodeService
    {

        private ILog _Log;
        private IConfiguration _Configuration;

        public QRCodeService(ILog log, IConfiguration config)
        {
            _Log = log;
            _Configuration = config;
        }

        public byte[] GenerateQRCode(string input, int scale)
        {
            _Log.Debug(string.Format("GenerateQRCode : {0} | {1}", input, scale));
            // reference
            // http://qrcodenet.codeplex.com/SourceControl/latest#Gma.QrCodeNet/Gma.QrCodeNet.Demo/UsageSample.cs

            if (!input.IsNotNullOrEmpty())
            {
                return new Byte[64];
            }

            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.M);

            QrCode qrCode = qrEncoder.Encode(input.TrimCheckForNull());

            GraphicsRenderer renderer = new GraphicsRenderer(new FixedModuleSize(scale, QuietZoneModules.Two), Brushes.Black, Brushes.White);


            using (MemoryStream ms = new MemoryStream())
            {
                renderer.WriteToStream(qrCode.Matrix, ImageFormat.Png, ms);
                return ms.ToArray();
            }

        }



        public MemoryStream GeneratePDFForUser(string documentpathandfilenmae, UserProfile uprofile)
        {
            _Log.Debug(string.Format("GeneratePDFForUser : {0} | {1}", documentpathandfilenmae, uprofile.Id));

            Aspose.Words.License license = new Aspose.Words.License();
            license.SetLicense("Aspose.Words.lic");

            List<string> listOfPropertyNames = new List<string>();
            List<object> listOfPropertyValues = new List<object>();

            string identifier = string.Format("{0}{1}", _Configuration.QRCodeUserPrefix, uprofile.Id);

            listOfPropertyNames.Add("SECTION1");
            listOfPropertyValues.Add(String.Format("QRCode for user '{0}' - Identifier : {1}", uprofile.EmailAddress, identifier));

            var doc = new Aspose.Words.Document(documentpathandfilenmae);

            NodeCollection shapes = doc.GetChildNodes(NodeType.Shape, true);

            foreach (Aspose.Words.Drawing.Shape nshape in shapes)
            {
                nshape.ImageData.SetImage(GenerateQRCode(identifier, 11));
            }

            // perform the mailmerge
            doc.MailMerge.Execute(listOfPropertyNames.ToArray(), listOfPropertyValues.ToArray());

            MemoryStream ms = new MemoryStream();
            doc.Save(ms, SaveFormat.Pdf);

            return ms;
        }

        public string GenerateUnregisteredEncryptedLink(int unregisteredgroupid, int currentindex, string baseurl)
        {
            _Log.Debug(string.Format("GenerateUnregisteredEncryptedLink : {0} | {1} | {2}", unregisteredgroupid, currentindex, baseurl));

            string url = String.Format("{0}{1}", baseurl, _Configuration.UnregisteredUrlContains);

            string encryptedindex = String.Format("{0}_{1}", unregisteredgroupid, currentindex);

#if !DEBUG
{
            encryptedindex = encryptedindex.Encrypt();
}
#endif

            return String.Format("{0}{1}", url, encryptedindex);
        }


        public MemoryStream GenerateMultiPartPDFZipFOrGroupUnregistered(string documentpathandfilenmae, Unregistered unreggroup, string baseurl)
        {
            _Log.Debug(string.Format("GeneratePDFForZoneUnregistered : {0} | {1}", documentpathandfilenmae, unreggroup.Id));

            System.IO.MemoryStream zms = new System.IO.MemoryStream();

            ZipFile zip = new ZipFile("");

            zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;

            int maxqrcodesperdocument = 240;  // must be in multiples of 3 you get 12 per page

            int maxdocuments = (int)Math.Ceiling((double)unreggroup.UnregisteredCodeIndex / (double)maxqrcodesperdocument);

            MemoryStream msPDF;

            for (int index = 0; index < maxdocuments; index++)
            {
                int fromcounter = (index * maxqrcodesperdocument) + 1;
                int tocounter = (index * maxqrcodesperdocument) + maxqrcodesperdocument + 1;

                string filename = String.Format("Codes_{0}_{1}.pdf", fromcounter, tocounter - 1);

                msPDF = new MemoryStream();

                msPDF = GeneratePDFForGroupUnregistered(documentpathandfilenmae, unreggroup, baseurl, fromcounter, tocounter);

                msPDF.Position = 0;

                ZipEntry ent = zip.AddEntry(filename, msPDF.ToArray());

                msPDF = null;
            }
            zip.Save(zms);

            zms.Seek(0, SeekOrigin.Begin);
            zms.Flush();

            return zms;

        }

        public MemoryStream GeneratePDFForGroupUnregistered(string documentpathandfilenmae, Unregistered unreggroup, string baseurl, int currentIndex, int itemsperdocument)
        {
            _Log.Debug(string.Format("GeneratePDFForZoneUnregistered : {0} | {1} | {2} | {3}", documentpathandfilenmae, unreggroup.Id, currentIndex, itemsperdocument));

            Aspose.Words.License license = new Aspose.Words.License();
            license.SetLicense("Aspose.Words.lic");

            List<string> listOfPropertyNames = new List<string>();
            List<object> listOfPropertyValues = new List<object>();

            var doc = new Aspose.Words.Document(documentpathandfilenmae);

            Table table = (Table)doc.GetChild(NodeType.Table, 0, true);

            int cellsperrow = 3;

            listOfPropertyNames.Add("ZONENAME");
            listOfPropertyValues.Add(unreggroup.Name);


            if (table != null)
            {
                Row coderou = table.Rows[1];

                int counter = 1;
                int qrcodeindex = currentIndex;
                //int maxrows = (int)Math.Ceiling((double)unreggroup.UnregisteredCodeIndex / (double)cellsperrow);
                int maxrows = (int)Math.Ceiling(((double)Math.Abs(itemsperdocument - currentIndex) / (double)cellsperrow));

                for (int i = 0; i < maxrows; i++)
                {

                    coderou = table.Rows[0];

                    coderou = (Row)coderou.Clone(true);

                    NodeCollection shapes;

                    Boolean drawRow = true;
                    for (int cellcounter = 1; cellcounter <= cellsperrow; cellcounter++)
                    {

                        // Boolean isovermaxrequested = ((i * cellsperrow) + cellcounter > (itemsperdocument * currentIndex) ? true : false);
                        Boolean isovermaxrequested = qrcodeindex > unreggroup.UnregisteredCodeIndex ? true : false;
                        Boolean isoverdocumentmax = qrcodeindex > (itemsperdocument - 1) ? true : false;

                        //int currentindexqr = (i * cellsperrow) + cellcounter + (itemsperdocument * currentIndex);
                        int currentindexqr = qrcodeindex;

                        string uniquecode = GenerateUnregisteredEncryptedLink(unreggroup.Id, currentindexqr, baseurl);

                        shapes = coderou.Cells[cellcounter - 1].Paragraphs[0].GetChildNodes(NodeType.Shape, true);

                        if (!isovermaxrequested && !isoverdocumentmax)
                        {
                            coderou.Cells[cellcounter - 1].Paragraphs[0].Runs[0].Text = String.Format("#{0} : {1}", currentindexqr, uniquecode);

                            foreach (Aspose.Words.Drawing.Shape nshape in shapes)
                            {
                                nshape.ImageData.SetImage(GenerateQRCode(string.Format("{0}", uniquecode), 11));
                            }
                            qrcodeindex++;
                        }
                        else
                        {
                            coderou.Cells[cellcounter - 1].Paragraphs[0].Runs[0].Text = "";

                            foreach (Aspose.Words.Drawing.Shape nshape in shapes)
                            {

                                nshape.Remove();
                            }
                            //qrcodeindex++;
                        }

                    }

                    table.Rows.Insert(counter, coderou);

                    Boolean isovermaxrequested1 = (qrcodeindex) > unreggroup.UnregisteredCodeIndex ? true : false;
                    Boolean isoverdocumentmax1 = (qrcodeindex) > (itemsperdocument - 1) ? true : false;

                    if (isovermaxrequested1 || isoverdocumentmax1)
                    {
                        break;
                    }

                    counter++;
                }

                table.Rows[0].Remove();

            }

            // perform the mailmerge
            doc.MailMerge.Execute(listOfPropertyNames.ToArray(), listOfPropertyValues.ToArray());

            MemoryStream ms = new MemoryStream();
            doc.Save(ms, SaveFormat.Pdf);

            return ms;
        }

        //public MemoryStream GeneratePDFForGroupUnregistered(string documentpathandfilenmae, Unregistered unreggroup, string baseurl)
        //{
        //    _Log.Debug(string.Format("GeneratePDFForZoneUnregistered : {0} | {1}", documentpathandfilenmae, unreggroup.Id));

        //    Aspose.Words.License license = new Aspose.Words.License();
        //    license.SetLicense("Aspose.Words.lic");

        //    List<string> listOfPropertyNames = new List<string>();
        //    List<object> listOfPropertyValues = new List<object>();

        //    var doc = new Aspose.Words.Document(documentpathandfilenmae);

        //    Table table = (Table)doc.GetChild(NodeType.Table, 0, true);

        //    int cellsperrow = 3;

        //    listOfPropertyNames.Add("ZONENAME");
        //    listOfPropertyValues.Add(unreggroup.Name);


        //    if (table != null)
        //    {
        //        Row coderou = table.Rows[1];

        //        int counter = 1;
        //        int maxrows = (int)Math.Ceiling((double)unreggroup.UnregisteredCodeIndex / (double)cellsperrow);

        //        for (int i = 0; i < maxrows; i++)
        //        {

        //            coderou = table.Rows[0];

        //            coderou = (Row)coderou.Clone(true);

        //            NodeCollection shapes;

        //            for (int cellcounter = 1; cellcounter <= cellsperrow; cellcounter++)
        //            {

        //                Boolean isovermaxrequested = ((i * cellsperrow) + cellcounter > unreggroup.UnregisteredCodeIndex ? true : false);

        //                int currentindex = (i * cellsperrow) + cellcounter;

        //                string uniquecode = GenerateUnregisteredEncryptedLink(unreggroup.Id, currentindex, baseurl);

        //                shapes = coderou.Cells[cellcounter - 1].Paragraphs[0].GetChildNodes(NodeType.Shape, true);

        //                if (!isovermaxrequested)
        //                {
        //                    coderou.Cells[cellcounter - 1].Paragraphs[0].Runs[0].Text = String.Format("#{0} : {1}", currentindex, uniquecode);

        //                    foreach (Aspose.Words.Drawing.Shape nshape in shapes)
        //                    {
        //                        nshape.ImageData.SetImage(GenerateQRCode(string.Format("{0}", uniquecode), 11));
        //                    }
        //                }
        //                else
        //                {
        //                    coderou.Cells[cellcounter - 1].Paragraphs[0].Runs[0].Text = "";

        //                    foreach (Aspose.Words.Drawing.Shape nshape in shapes)
        //                    {

        //                        nshape.Remove();
        //                    }
        //                }

        //            }

        //            table.Rows.Insert(counter, coderou);
        //            counter++;
        //        }

        //        table.Rows[0].Remove();

        //    }

        //    // perform the mailmerge
        //    doc.MailMerge.Execute(listOfPropertyNames.ToArray(), listOfPropertyValues.ToArray());

        //    MemoryStream ms = new MemoryStream();
        //    doc.Save(ms, SaveFormat.Pdf);

        //    return ms;
        //}

        public MemoryStream GeneratePDFForZone(string documentpathandfilenmae, Zone zone)
        {
            _Log.Debug(string.Format("GeneratePDFForZone : {0} | {1}", documentpathandfilenmae, zone.Identifier));

            Aspose.Words.License license = new Aspose.Words.License();
            license.SetLicense("Aspose.Words.lic");

            List<string> listOfPropertyNames = new List<string>();
            List<object> listOfPropertyValues = new List<object>();

            listOfPropertyNames.Add("SECTION1");
            listOfPropertyValues.Add(String.Format("QRCode for zone '{0}' - Identifier : {1}", zone.Name, zone.Identifier));

            var doc = new Aspose.Words.Document(documentpathandfilenmae);

            NodeCollection shapes = doc.GetChildNodes(NodeType.Shape, true);

            foreach (Aspose.Words.Drawing.Shape nshape in shapes)
            {
                nshape.ImageData.SetImage(GenerateQRCode(zone.Identifier, 11));
            }

            // perform the mailmerge
            doc.MailMerge.Execute(listOfPropertyNames.ToArray(), listOfPropertyValues.ToArray());

            // TODO - we are getting a memorytstream error on 4000 saved to pdf. - investigate.
            MemoryStream ms = new MemoryStream();
            doc.Save(ms, SaveFormat.Pdf);

            return ms;
        }




    }
}
