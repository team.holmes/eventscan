﻿using System;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Linq;
using MyApp.Business.Resource;
using OpenPlatform.General.Encryption;
using OpenPlatform.General.Extensions;
using MyApp.Business.DomainObjects.Models;
using OpenPlatform.General.Dal;
using OpenPlatform.General.Model;
using System.Web;
using System.Collections.Generic;

namespace MyApp.Business.Services
{
    public class UnregisteredService : IUnregisteredService
    {
        private IRepository _Repository;
        private IConfiguration _Configuration;
        private ILog _Log;

        public UnregisteredService(IConfiguration configuration, ILog log, IRepository repository)
        {
            _Configuration = configuration;
            _Log = log;
            _Repository = repository;
        }


        public List<Unregistered> GetUnregisteredOrderedByName(int userprofileid, Boolean inclueInactive)
        {
            _Log.Debug(string.Format("GetUnregisteredOrderedByName {0} | {1}", userprofileid, inclueInactive));

            if (inclueInactive)
            {
                return _Repository.GetFiltered<Unregistered>(x => x.UserProfileId == userprofileid).OrderBy(x => x.Name).ToList();
            }

            return _Repository.GetFiltered<Unregistered>(x => x.UserProfileId == userprofileid && x.IsActive == true).OrderBy(x => x.Name).ToList();

        }

        public Unregistered GetUnregisteredById(int unregisteredif)
        {
            _Log.Debug(string.Format("GetUnregisteredById {0} {1}", unregisteredif, ""));

            return _Repository.GetById<Unregistered>(unregisteredif);
        }

        public void DeleteByUnregisteredId(int unregisteredid)
        {
            _Log.Debug(string.Format("DeleteByUnregisteredId {0} {1}", unregisteredid, ""));

            _Repository.DeleteById<Unregistered>(unregisteredid);

        }

        public BusinessEnum.UnregisteredAdd Add(Unregistered item)
        {
            _Log.Debug(string.Format("Add {0} {1}", item.Name, ""));

            if (DoesUnregisteredNameAlreadyExist(item.Name, item.UserProfileId))
            {
                return BusinessEnum.UnregisteredAdd.DuplicateName;
            }

            item.MaxUnregisteredCodesAllowed = _Configuration.MaxUnregisteredZonesAllowed;

            int addeddbid = _Repository.Add<Unregistered>(item);

            if (addeddbid > 0)
            {
                return BusinessEnum.UnregisteredAdd.Success;
            }

            return BusinessEnum.UnregisteredAdd.Fail;
        }

        public Boolean DoesUnregisteredGroupHaveAnyEngagements(int unreggroupid)
        {
            _Log.Debug(string.Format("DoesUnregisteredGroupHaveAnyEngagements {0} {1}", unreggroupid, ""));

            string zonrcomparison = String.Format("{0}_", unreggroupid);

            int matches = _Repository.GetFiltered<ZoneEngagement>(x => x.UnregisteredId.StartsWith(zonrcomparison)).Count();

            return (matches > 0 ? true : false);
        }

        public BusinessEnum.UnregisteredAdd Update(Unregistered unregistered)
        {
            _Log.Debug(string.Format("Update {0} {1}", unregistered.Id, ""));

            Unregistered zonewithsamename = _Repository.GetFiltered<Unregistered>(x => x.UserProfileId == unregistered.UserProfileId && unregistered.IsActive == true && x.Name.Equals(unregistered.Name, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (zonewithsamename != null && zonewithsamename.Id > 0 && zonewithsamename.Id != unregistered.Id)
            {
                return BusinessEnum.UnregisteredAdd.DuplicateName;
            }

            if (_Repository.Update<Unregistered>(unregistered.Id, unregistered))
            {
                return BusinessEnum.UnregisteredAdd.Success;
            }

            return BusinessEnum.UnregisteredAdd.Fail;
        }

        public Boolean DoesUnregisteredNameAlreadyExist(string name, int userprofileID)
        {
            _Log.Debug(string.Format("DoesUnregisteredNameAlreadyExist {0} {1}", name, userprofileID));

            if (_Repository.GetFiltered<Unregistered>(x => x.UserProfileId == userprofileID && x.IsActive == true && x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)).Any())
            {
                return true;
            }
            return false;
        }


        public BusinessEnum.UnregisteredRequestIndex UpdateUnregisteredRequestIndex(int numrequested, Unregistered unregistered)
        {

            if (!IsNumberUnregisteredRequestedUnderPerRequestAmount(numrequested))
            {
                return BusinessEnum.UnregisteredRequestIndex.TooMuchRequested;

            }

            if (!IsUnregisteredAmountRequestBelowMaxAllowed(numrequested, unregistered))
            {
                return BusinessEnum.UnregisteredRequestIndex.ExceedMax;
            }

            unregistered.UnregisteredCodeIndex += numrequested;

            Boolean result = _Repository.Update<Unregistered>(unregistered.Id, unregistered);

            if (result == true) return BusinessEnum.UnregisteredRequestIndex.Success;

            return BusinessEnum.UnregisteredRequestIndex.Fail;

        }


        public Boolean IsUnregisteredAmountRequestBelowMaxAllowed(int numrequested, Unregistered unreg)
        {
            _Log.Debug(string.Format("IsUnregisteredAmountRequestBelowMaxAllowed {0} {1}", numrequested, unreg.Id));
            if ((numrequested + unreg.UnregisteredCodeIndex) > unreg.MaxUnregisteredCodesAllowed)
            {
                return false;
            }

            return true;
        }

        private Boolean IsNumberUnregisteredRequestedUnderPerRequestAmount(int numrequested)
        {
            _Log.Debug(string.Format("IsNumberUnregistredRequestedAbovePerRequestAmount {0} {1}", numrequested, ""));
            if (numrequested <= _Configuration.MaxUnregisteredZonesAllowedPerRequest)
            {
                return true;
            }

            return false;
        }


    }
}
