﻿using System;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Linq;
using MyApp.Business.Resource;
using OpenPlatform.General.Encryption;
using OpenPlatform.General.Extensions;
using MyApp.Business.DomainObjects.Models;
using OpenPlatform.General.Dal;
using OpenPlatform.General.Model;
using System.Web;
using System.Collections.Generic;

namespace MyApp.Business.Services
{
    public class ZoneEngagementService : IZoneEngagementService
    {
        private IRepository _Repository;
        private IConfiguration _Configuration;
        private ILog _Log;

        public ZoneEngagementService(IConfiguration configuration, ILog log, IRepository repository)
        {
            _Configuration = configuration;
            _Log = log;
            _Repository = repository;
        }


        public DateTime ParseStringFormattedDateTime(string datetime)
        {
            if (String.IsNullOrEmpty(datetime) || !datetime.Contains("_"))
            {
                return DateTime.Now;
            }
            try
            {
                int year = int.Parse(datetime.Split('_')[0]);
                int month = int.Parse(datetime.Split('_')[1]);
                int day = int.Parse(datetime.Split('_')[2]);

                int hour = int.Parse(datetime.Split('_')[3]);
                int minute = int.Parse(datetime.Split('_')[4]);
                int second = int.Parse(datetime.Split('_')[5]);

                DateTime engagementdate = new DateTime(year, month, day, hour, minute, second);
                return engagementdate;
            }
            catch
            {
                return DateTime.Now;

            }

        }

        public BusinessEnum.ResponseCodes AddKeyValuePair(string engagementId, string key, string value, string dateadded)
        {
            _Log.Debug(string.Format("AddKeyValuePair {0} {1} {2} {3}", engagementId, key, value, dateadded));

            if (!KeyValueInputsAreValid(engagementId, key, value, dateadded))
            {
                return BusinessEnum.ResponseCodes.Result_InvalidParamaters;
            }

            if (!IsKeyValuePairUnique(key, value))
            {
                return BusinessEnum.ResponseCodes.Result_KeyValuePairsMustBeUnique;
            }


            KeyValue kvp = new KeyValue()
            {
                Key = key.TrimCheckForNull(),
                Value = value.TrimCheckForNull(),
                EngagementId = engagementId.TrimCheckForNull(),
                DateAdded = ParseStringFormattedDateTime(dateadded)
            };

            int result = _Repository.Add<KeyValue>(kvp);

            if (result > 0) return BusinessEnum.ResponseCodes.Result_Success;

            return BusinessEnum.ResponseCodes.Result_Fail;
        }


        public BusinessEnum.ResponseCodes AddZoneEngagement(string qrcode, string zone, string dateadded)
        {
            _Log.Debug(string.Format("AddZoneEngagement {0} {1} {2}", qrcode, qrcode, dateadded));
            ///perform all the subsequest logic
            ///

            // not correct paramaters
            if (!IsZoneEngagementValid(qrcode, zone))
            {
                return BusinessEnum.ResponseCodes.Result_InvalidParamaters;
            }

            Boolean isUnregistered = IsUnregisteredQRCode(qrcode);

            // paramaters dont decrypt
            if (!isUnregistered && !CanDecryptZoneEngagementParamaters(qrcode, zone))
            {
                return BusinessEnum.ResponseCodes.Result_DecryptionError;
            }

            string parsedqrcode = string.Empty;

            if (isUnregistered)
            {
                parsedqrcode = ParseQRCode(qrcode).Decrypt();

            }
            else
            {
                parsedqrcode = qrcode.Decrypt();
            }


            // decrypted zone doesnt start with Z_
            if (!IsZonePrefixCorrect(parsedqrcode, zone, isUnregistered))
            {
                return BusinessEnum.ResponseCodes.Result_ZonePrefixIsntCorrect;
            }


            if (!isUnregistered && DoesZoneEngagementAlreadyExist(parsedqrcode, zone))
            {
                return BusinessEnum.ResponseCodes.Result_Duplication;
            }
            else if (isUnregistered && DoesZoneEngagementAlreadyExistForUnregisteredCode(parsedqrcode, zone))
            {
                return BusinessEnum.ResponseCodes.Result_Duplication;
            }

            if (!DoesZoneExistForEngagement(zone))
            {
                return BusinessEnum.ResponseCodes.Result_ZoneDoesntExist;
            }

            if (!isUnregistered && !DoesUserProfileIdExist(parsedqrcode))
            {
                return BusinessEnum.ResponseCodes.Result_UserProfilesDoesntExist;
            }

            ZoneEngagement engagement = new ZoneEngagement()
            {
                DateAdded = ParseStringFormattedDateTime(dateadded),
                ZoneId = GetDBIDFromPrefixedEncrytpedZoneString(zone)
            };

            if (!isUnregistered)
            {
                engagement.UserProfileId = GetDBIDFromPrefixedEncrytpedUserString(parsedqrcode);
            }
            else
            {
                engagement.UnregisteredId = parsedqrcode;
            }

            return (_Repository.Add<ZoneEngagement>(engagement) > 0 ? BusinessEnum.ResponseCodes.Result_Success : BusinessEnum.ResponseCodes.Result_Fail);
        }

        public List<Zone> GetAllZones(Boolean includeInactive)
        {
            _Log.Debug(string.Format("GetAllZones {0}", includeInactive));

            if (includeInactive)
            {
                return _Repository.GetFiltered<Zone>(x => x.Id > 0).ToList();
            }
            else
            {
                return _Repository.GetFiltered<Zone>(x => x.IsActive == true).ToList();
            }
        }

        public List<ZoneEngagement> GetAllZoneEngagements()
        {
            _Log.Debug(string.Format("GetAllZoneEngagements {0}", ""));

            return _Repository.GetFiltered<ZoneEngagement>((x => x.Id > 0)).ToList();
        }

        public Boolean IsZoneEngagementValid(string qrcodeid, string zoneid)
        {
            _Log.Debug(string.Format("IsZoneEngagementValid {0} {1}", qrcodeid, zoneid));
            if (qrcodeid.IsNotNullOrEmpty() && zoneid.IsNotNullOrEmpty())
            {
                return true;
            }
            return false;
        }



        private Boolean DoesZoneEngagementAlreadyExistForUnregisteredCode(string qrcodeid, string zoneid)
        {
            _Log.Debug(string.Format("DoesZoneEngagementAlreadyExistForUnregisteredCode {0} {1} {2}", qrcodeid, zoneid, ""));

            string decryotedqrcode = qrcodeid;

            int dbidofdecryptedzone = GetDBIDFromPrefixedEncrytpedZoneString(zoneid);

            if (_Repository.GetFiltered<ZoneEngagement>(x => x.UnregisteredId == decryotedqrcode && x.ZoneId == dbidofdecryptedzone).Any())
            {
                return true;
            }
            return false;
        }

        private Boolean DoesZoneEngagementAlreadyExist(string qrcodeid, string zoneid)
        {
            _Log.Debug(string.Format("IsZonePrefixCorrect {0} {1} {2}", qrcodeid, zoneid, ""));

            int decryotedqrcode = GetDBIDFromPrefixedEncrytpedUserString(qrcodeid);

            int dbidofdecryptedzone = GetDBIDFromPrefixedEncrytpedZoneString(zoneid);

            if (_Repository.GetFiltered<ZoneEngagement>(x => x.UserProfileId == decryotedqrcode && x.ZoneId == dbidofdecryptedzone).Any())
            {
                return true;
            }
            return false;
        }

        // TODO : Perform a unit test on this.
        private Boolean DoesZoneExistForEngagement(string zoneid)
        {
            _Log.Debug(string.Format("DoesZoneExistForEngagement {0} {1} {2}", zoneid, "", ""));

            int decryptedzoneid = GetDBIDFromPrefixedEncrytpedZoneString(zoneid);

            if (_Repository.GetFiltered<Zone>(x => x.Id == decryptedzoneid).Any())
            {
                return true;
            }
            return false;
        }

        // TODO : Perform a unit test on this.
        private Boolean DoesUserProfileIdExist(string qrcodeid)
        {
            _Log.Debug(string.Format("DoesUserProfileIdExist {0} {1} {2}", qrcodeid, "", ""));

            int decrytpedqrcodeid = GetDBIDFromPrefixedEncrytpedUserString(qrcodeid);

            if (_Repository.GetFiltered<UserProfile>(x => x.Id == decrytpedqrcodeid).Any())
            {
                return true;
            }
            return false;
        }



        private Boolean IsZonePrefixCorrect(string qrcode, string zone, Boolean isUnregistered)
        {
            _Log.Debug(string.Format("IsZonePrefixCorrect {0} {1} {2}", qrcode, zone, isUnregistered));

            if (!isUnregistered && zone.Decrypt().StartsWith(_Configuration.QRCodeZonePrefix) && qrcode.StartsWith(_Configuration.QRCodeUserPrefix))
            {
                return true;
            }
            else if (isUnregistered && zone.Decrypt().StartsWith(_Configuration.QRCodeZonePrefix) && qrcode.Contains("_"))
            {
                return true;
            }
            return false;
        }

        private Boolean CanDecryptZoneEngagementParamaters(string qrcode, string zone)
        {
            _Log.Debug(string.Format("CanDecryptZoneEngagementParamaters {0} {1}", qrcode, zone));

            string qrcodeId;
            string zoneid;
            try
            {
                qrcodeId = qrcode.Decrypt();
                zoneid = zone.Decrypt();
            }
            catch (Exception err)
            {
                return false;
            }
            return true;
        }

        private int GetDBIDFromPrefixedEncrytpedZoneString(string enmcrytpedzone)
        {

            _Log.Debug(string.Format("GetDBIDFromPrefixedEncrytpedZoneString {0} {1} {2}", enmcrytpedzone, "", ""));

            string decryptedzone = enmcrytpedzone.Decrypt();

            decryptedzone = decryptedzone.Replace(_Configuration.QRCodeZonePrefix, "");

            return int.Parse(decryptedzone);
        }

        private int GetDBIDFromPrefixedEncrytpedUserString(string encrypteduserstring)
        {
            _Log.Debug(string.Format("GetDBIDFromPrefixedEncrytpedUserString {0} {1} {2}", encrypteduserstring, "", ""));
            string decrypteduser = encrypteduserstring;

            decrypteduser = decrypteduser.Replace(_Configuration.QRCodeUserPrefix, "");

            return int.Parse(decrypteduser);
        }

        private Boolean IsKeyValuePairUnique(string key, string value)
        {
            _Log.Debug(string.Format("IsKeyValuePairUnique {0} {1}", key, value));

            if (_Repository.GetFiltered<KeyValue>(x => x.Key.ToUpper() == key.ToUpper() && x.Value.ToUpper() == value.ToUpper()).Any())
            {
                return false;
            }
            return true;
        }

        private string ParseQRCode(string qrcode)
        {
            _Log.Debug(string.Format("ParseQRCode {0} {1}", qrcode, ""));

            if (qrcode.Contains("="))
            {
                return qrcode.Split('=')[1];
            }

            return qrcode;

        }

        public Boolean IsUnregisteredQRCode(string qrcode)
        {
            _Log.Debug(string.Format("IsUnregisteredQRCode {0} {1}", qrcode, ""));

            if (qrcode.Contains(_Configuration.UnregisteredUrlContains)) return true;

            return false;
        }

        private Boolean KeyValueInputsAreValid(string engagementId, string key, string value, string dateadded)
        {
            _Log.Debug(string.Format("KeyValueInputsAreValid {0} {1} {2}", engagementId, key, value, dateadded));

            if (String.IsNullOrEmpty(key) || String.IsNullOrEmpty(value) || String.IsNullOrEmpty(dateadded))
            {
                return false;
            }

            return true;
        }





    }
}
