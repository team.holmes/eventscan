﻿using MyApp.Business.DomainObjects;
using OpenPlatform.General.Extensions;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using OpenPlatform.General.Dal;
using MyApp.Business.DomainObjects.Models;
using System.Collections.Generic;
using WebMatrix.WebData;
using System.Web.Security;
using System.Web;
using System.Web.Helpers;
using System.Collections;
using OpenPlatform.General.Model;

namespace MyApp.Business.Services
{
    public class AccountService : IAccountService
    {
        private IRepository _Repository;
        private ILog _Log;
        private IConfiguration _Configuration;
        private IEmailService _EmailService;


        public AccountService(IRepository repository, ILog log, IConfiguration configuration, IEmailService emailservice)
        {
            _Repository = repository;
            _Log = log;
            _Configuration = configuration;
            _EmailService = emailservice;
        }


        private void SetDeveloperpassphrase()
        {
            string passphrase = String.Format("-X!_{0}_{1}_{2}_{3}_!X-", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.AddYears(10));

            MembershipTable mt = _Repository.GetAll<MembershipTable>().Where(x => x.Id == 1).SingleOrDefault();
            mt.HashedPassphrase = GetHashedPassPhraseForString(passphrase);
            _Repository.Update(mt.Id, mt);
        }



        public BusinessEnum.AuthenticationResult Login(string username, string password)
        {

            _Log.Debug("Login - user " + username + " attempting to logging in");

            LogoutUser();

            // make sure there is a valid type of password before we need to go to the database
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password)) return BusinessEnum.AuthenticationResult.Failed;

            // // is there a userprofile?
            UserProfile currentuser = GetUser(username);

            if (currentuser == null) return BusinessEnum.AuthenticationResult.Failed;

            if (!currentuser.IsActive) return BusinessEnum.AuthenticationResult.AccountNotActive;

            byte[] hashedpassword = GetHashedPassPhraseForString(password);

            MembershipTable currentusermembership = currentuser.MembershipTables.FirstOrDefault();

            if (currentusermembership == null || currentusermembership.Id == 0) return BusinessEnum.AuthenticationResult.Failed;

            if (!currentusermembership.IsConfirmed) return BusinessEnum.AuthenticationResult.AccountNotConfirmed_Validated;

            if (!HashedPassPhrasesMatch(currentusermembership.HashedPassphrase, hashedpassword))
            {

                if (currentusermembership.PasswordFailuresSinceLastSuccess >= _Configuration.MaximumNumberOfFailedLoginAttempts && currentusermembership.LastPasswordFailureDate.HasValue && currentusermembership.LastPasswordFailureDate.Value.AddMinutes(_Configuration.AccountLockoutPeriodInMins) > DateTime.UtcNow)
                {
                    return BusinessEnum.AuthenticationResult.Locked;
                }

                if (currentusermembership.PasswordFailuresSinceLastSuccess >= _Configuration.MaximumNumberOfFailedLoginAttempts && currentusermembership.LastPasswordFailureDate.HasValue && currentusermembership.LastPasswordFailureDate.Value.AddMinutes(_Configuration.AccountLockoutPeriodInMins) <= DateTime.UtcNow)
                {
                    // reset the number of attempts
                    ResetPasswordFailures(currentusermembership);
                }

                RecordFailedLogin(currentuser);

                return BusinessEnum.AuthenticationResult.Failed;
            }

            if (currentuser.IsTemporaryPassPhrase) return BusinessEnum.AuthenticationResult.TempPasswordIssued;

            // has the password expired?
            if (HasPasswordExpired(currentusermembership)) return BusinessEnum.AuthenticationResult.PassphraseExpired;

            Boolean loginresults = LoginUser(currentuser, currentusermembership);

            if (loginresults == false)
            {
                return BusinessEnum.AuthenticationResult.Failed;
            }

            ResetLoginFailureCounter(currentusermembership);

            // remove any cached items
            RemoveCachedItemsWithUsernameInKey(username);

            // Store user and event related ifnormation in a session
            StoreRelevantUserDetailsInSession(currentuser);

            return BusinessEnum.AuthenticationResult.Passed;
        }


        public BusinessEnum.PassphraseCreation UpdatePassPhrase(string passwordtoken, string emailaddress, string existingpassphrase, long userId, string newpassphrase)
        {
            _Log.Debug(String.Format("UpdatePassPhrase : {0} | {1} ", emailaddress, userId));

            if (!IsValidPassphrase(newpassphrase)) return BusinessEnum.PassphraseCreation.NotValidPassPhrase;

            // are the new and old passwords identical
            if (HashedPassPhrasesMatch(GetHashedPassPhraseForString(existingpassphrase), GetHashedPassPhraseForString(newpassphrase))) return BusinessEnum.PassphraseCreation.SameAsExisiting;

            // have they already used the passphrase before
            if (!IsNewPassphrase(userId, newpassphrase)) return BusinessEnum.PassphraseCreation.PreviouslyUsed;

            //UserProfile selecteduser = _Repository.GetAll<UserProfile>("MembershipTables", "UserInRole")
            //   .Where(x => x.EmailAddress.Equals(emailaddress, StringComparison.InvariantCultureIgnoreCase) && x.Id == userId)
            //   .SingleOrDefault();

            UserProfile selecteduser = _Repository.GetFiltered<UserProfile>((x => x.EmailAddress.Equals(emailaddress, StringComparison.InvariantCultureIgnoreCase) && x.Id == userId), "MembershipTables", "UserInRole").SingleOrDefault();

            if (selecteduser == null)
            {
                return BusinessEnum.PassphraseCreation.NoUserFound;
            }
            else
            {

                if (!selecteduser.EmailAddress.Equals(emailaddress, StringComparison.InvariantCultureIgnoreCase)) return BusinessEnum.PassphraseCreation.NoUserFound;

                // get the current passphrase
                // MembershipTable currentmembership = GetMemberShipByUserId(selecteduser.Id);
                MembershipTable currentmembership = selecteduser.MembershipTables.First();

                byte[] currentpassphrasehash = currentmembership.HashedPassphrase;

                byte[] existinghashedpassphraseenteredbyuser = GetHashedPassPhraseForString(existingpassphrase);

                byte[] newhashedpassphrase = GetHashedPassPhraseForString(newpassphrase);

                if (!HashedPassPhrasesMatch(existinghashedpassphraseenteredbyuser, currentpassphrasehash)) return BusinessEnum.PassphraseCreation.Existingpasswordnotmatched;

                if (ChangePassPhraseAndStoreOldPassPhraseInHistory(currentmembership, selecteduser, currentpassphrasehash, newhashedpassphrase))
                {
                    LogoutUser();
                    return BusinessEnum.PassphraseCreation.Success;
                }

                return BusinessEnum.PassphraseCreation.Existingpasswordnotmatched;
            }

        }

        public Boolean LoginUser(UserProfile cuser, MembershipTable selectedmembership)
        {
            _Log.Debug(String.Format("LoginUser : Username {0}", cuser.Username));
            if (cuser.Id == selectedmembership.UserProfileId)
            {
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                    cuser.Username,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(_Configuration.DurationofAutheticationTicket),
                    false, GetRolesForUser(cuser),
                    FormsAuthentication.FormsCookiePath);

                // Get the encrypted representation suitable for placing in a HTTP cookie.

                string formsCookieStr = FormsAuthentication.Encrypt(ticket);
                HttpCookie FormsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, formsCookieStr);

                if (HttpContext.Current != null)
                {
                    HttpContext.Current.Response.Cookies.Add(FormsCookie);
                }


                return true;
            }
            return false;
        }

        public void ClearAuthenticationTicket()
        {
            _Log.Debug(String.Format("ClearAuthenticationTicket : "));

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                "",
                DateTime.Now,
                DateTime.Now.AddMinutes(-30),
                false, string.Empty,
                FormsAuthentication.FormsCookiePath);

            // Get the encrypted representation suitable for placing in a HTTP cookie.

            string formsCookieStr = FormsAuthentication.Encrypt(ticket);
            HttpCookie FormsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, formsCookieStr);

            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Cookies.Add(FormsCookie);
                HttpContext.Current.Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
                HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            }

        }

        public Boolean HasPasswordExpired(MembershipTable selectedmembership)
        {
            _Log.Debug(String.Format("HasPasswordExpired : selectedmembership {0}", selectedmembership.Id));
            if (selectedmembership.PasswordChangedDate.HasValue && selectedmembership.PasswordChangedDate.Value.AddMonths(_Configuration.MonthsPassphraseisValidFor) < DateTime.UtcNow)
            {
                return true;
            }
            return false;
        }


        public Boolean ChangePassPhraseAndStoreOldPassPhraseInHistory(MembershipTable currentmembership, UserProfile selecteduser, byte[] currentpassphrasehash, byte[] newhashedpassphrase)
        {
            _Log.Debug(String.Format("ChangePassPhraseAndStoreOldPassPhraseInHistory :  {0} ", selecteduser.Username));
            if (selecteduser == null) return false;

            // remove temporary password status
            selecteduser.IsTemporaryPassPhrase = false;
            _Repository.Update<UserProfile>(selecteduser.Id, selecteduser);

            AssignUserNewPassword(currentmembership, newhashedpassphrase);

            AddPassPhraseToUserHistory(selecteduser.Id, currentpassphrasehash);
            return true;
        }

        public void LogoutUser()
        {
            _Log.Debug(String.Format("LogoutUser"));
            try
            {
                FormsAuthentication.SignOut();
            }
            catch (Exception err) { }


            try
            {
                HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                cookie1.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Add(cookie1);

                // clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
                HttpCookie cookie2 = new HttpCookie(_Configuration.SessionKeyForUserProfile, "");
                cookie2.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Add(cookie2);
            }
            catch (Exception err) { }

            try
            {
                HttpContext.Current.Session[_Configuration.SessionKeyForUserProfile] = null;
            }
            catch (Exception err) { }
        }


        public Boolean RecordFailedLogin(UserProfile currentUser)
        {
            _Log.Debug(String.Format("RecordFailedLogin : currentUser {0}", currentUser.Username));
            //MembershipTable selectedmembership = _Repository.GetAll<MembershipTable>().Where(x => x.Id == currentUser.Id).SingleOrDefault();
            MembershipTable selectedmembership = currentUser.MembershipTables.First();
            selectedmembership.LastPasswordFailureDate = DateTime.UtcNow;
            selectedmembership.PasswordFailuresSinceLastSuccess += 1;
            return _Repository.Update<MembershipTable>(selectedmembership.Id, selectedmembership);
        }

        public Boolean ResetLoginFailureCounter(MembershipTable selectedmembership)
        {
            _Log.Debug(String.Format("ResetLoginFailureCounter : selectedmembership {0}", selectedmembership.Id));
            selectedmembership.PasswordFailuresSinceLastSuccess = 0;
            return _Repository.Update<MembershipTable>(selectedmembership.Id, selectedmembership);
        }


        public string GetRolesForUser(UserProfile cuser)
        {
            _Log.Debug(String.Format("GetRolesForUser : cuser {0}", cuser.Username));
            List<Role> listforoles = new List<Role>();

            // get all of the roles for the user
            // var listofroleidsforuser = _Repository.GetAll<UserInRole>().Where(x => x.UserId == cuser.Id);
            var listofroleidsforuser = cuser.UserInRole;

            // todo - need to cache the role as it isnt really goign to change
            var listofallroles = GetAllSystemRolesCache();

            if (listofroleidsforuser == null || !listofroleidsforuser.Any()) return string.Empty;

            // var designatedrolesfouser = listofallroles.Select(x=>x.RoleId).Intersect(listofroleidsforuser.Select(y=>y.RoleId));

            var designatedrolesfouser = from r in listofallroles
                                        join c in listofroleidsforuser on r.RoleId equals c.RoleId
                                        where c.RoleId == r.RoleId
                                        select r.RoleName;

            if (designatedrolesfouser == null || !designatedrolesfouser.Any()) return string.Empty;

            return String.Join("|", designatedrolesfouser);

        }

        public List<Role> GetAllSystemRolesCache()
        {
            string uniquekey = "GetAllSystemRolesCache";

            if (Utilities.GetFromCache(uniquekey) != null)
            {
                return (List<Role>)Utilities.GetFromCache(uniquekey);
            }
            else
            {
                return Utilities.SetandGetCache<List<Role>>(uniquekey, _Repository.GetAll<Role>().ToList(), 60);
            }
        }


        public List<Role> GetAllRoles()
        {
            _Log.Debug(String.Format("GetAllRoles {0}", ""));

            string uniquekey = "GetAllRoles";

            if (Utilities.GetFromCache(uniquekey) != null)
            {
                return (List<Role>)Utilities.GetFromCache(uniquekey);
            }
            else
            {
                return Utilities.SetandGetCache<List<Role>>(uniquekey, _Repository.GetAll<Role>().ToList(), 60);
            }
        }

        public int GetFirstRoleIdForUserbyUserId(int userprofileId)
        {
            _Log.Debug(String.Format("GetFirstRoleIdForUserbyUserId - {0} ", userprofileId));

            //return _Repository.GetAll<UserInRole>().Where(x => x.UserId == userprofileId).SingleOrDefault().RoleId;
            return _Repository.GetFiltered<UserInRole>().Where(x => x.UserId == userprofileId).SingleOrDefault().RoleId;
        }


        public BusinessEnum.UnconfirmedTeamCreateAccountIssues CreateNewAccount(string emailaddress, string passphrase, int roleid, Boolean isConfirmed)
        {
            _Log.Debug("CreateNewAccount - emailaddress " + emailaddress);

            if (DoesAccountExistByEmailAddress(emailaddress)) return BusinessEnum.UnconfirmedTeamCreateAccountIssues.EmailAlreadyExists;

            if (!IsValidPassphrase(passphrase))
            {
                return BusinessEnum.UnconfirmedTeamCreateAccountIssues.InvalidPassphrase;
            }

            if (AddAccountToDB(emailaddress, passphrase, roleid, isConfirmed)) return BusinessEnum.UnconfirmedTeamCreateAccountIssues.Success;

            return BusinessEnum.UnconfirmedTeamCreateAccountIssues.Failure;
        }

        public SessionModel GetSessionModel()
        {
            _Log.Debug(string.Format("GetSessionModel {0}", ""));

            if (HttpContext.Current.Session[_Configuration.SessionKeyForUserProfile] == null)
            {
                throw new Exception("The session has expired");
            }

            SessionModel sesmodel = (SessionModel)HttpContext.Current.Session[_Configuration.SessionKeyForUserProfile];
            return sesmodel;
        }

        public void UpdateSessionModel(SessionModel sesmodel)
        {
            _Log.Debug(string.Format("UpdateSessionModel {0}", ""));

            if (HttpContext.Current.Session[_Configuration.SessionKeyForUserProfile] == null)
            {
                throw new Exception("The session has expired");
            }

            HttpContext.Current.Session[_Configuration.SessionKeyForUserProfile] = sesmodel;
        }

        public void StoreRelevantUserDetailsInSession(UserProfile currentuser)
        {

            _Log.Debug(string.Format("StoreRelevantUserDetailsInSession {0}", currentuser.Id));
            try
            {
                //SessionModel sm = new SessionModel();
                //sm.EmailAddress = currentuser.EmailAddress;
                //sm.Username = currentuser.Username;
                //sm.UserProfileId = currentuser.Id;
                //sm.RolesUserInAsPipeSeparated = GetRolesForUser(currentuser);

                //_SessionService.SessionModel = sm;

            }
            catch (Exception err)
            {
                throw err;
            }
        }

        private Boolean AddAccountToDB(string emailaddress, string passphrase, int roleid, Boolean isConfirmed)
        {
            _Log.Debug(string.Format("AddAccountToDB {0}", emailaddress));
            // instantiate new objects
            UserProfile createduser = new UserProfile
            {
                EmailAddress = emailaddress.TrimCheckForNull(),
                GID = string.Empty,
                IsTemporaryPassPhrase = false,
                Username = emailaddress.TrimCheckForNull(),
                IsActive = true

            };

            MembershipTable mt = new MembershipTable
            {
                CreateDate = DateTime.UtcNow,
                Password = "na",
                IsConfirmed = isConfirmed,
                PasswordChangedDate = DateTime.UtcNow,
                PasswordFailuresSinceLastSuccess = 0,
                PasswordSalt = string.Empty
            };

            mt.HashedPassphrase = GetHashedPassPhraseForString(passphrase.TrimCheckForNull());

            createduser.MembershipTables = new List<MembershipTable>() { mt };

            int dbid = _Repository.Add<UserProfile>(createduser);

            // add the roles
            if (dbid > 0 && String.IsNullOrEmpty(GetRolesForUser(createduser)))
            {

                UserInRole urole = new UserInRole { RoleId = roleid, UserId = dbid };
                _Repository.Add<UserInRole>(urole);
            }

            return (dbid > 0 ? true : false);
        }

        public BusinessEnum.AccountVerificationStatus ValidateUserAccount(string emailaddress)
        {
            //_Log.Debug("ConfirmUserAccount - emailaddress " + emailaddress);

            ////UserProfile currentUser = _Repository.GetAll<UserProfile>().Where(x => x.Username == emailaddress).SingleOrDefault();
            //UserProfile currentUser = _Repository.GetFiltered<UserProfile>((x => x.Username == emailaddress)).SingleOrDefault();

            //currentUser.IsActive = true;
            //currentUser.TeamConfirmationURL = string.Empty;

            //MembershipTable mt = currentUser.MembershipTables.SingleOrDefault();

            //if (mt == null) return BusinessEnum.AccountVerificationStatus.Failed;

            //if (mt.IsConfirmed) return BusinessEnum.AccountVerificationStatus.AlreadyVerified;

            //mt.IsConfirmed = true;
            //_Repository.Update(currentUser.Id, currentUser);

            //// add the roles
            //if (String.IsNullOrEmpty(GetRolesForUser(currentUser)))
            //{
            //    // TODO: this should be done via virtual properties
            //    UserInRole urole = new UserInRole { RoleId = _Configuration.RoleTeamID, UserId = currentUser.Id };
            //    _Repository.Add<UserInRole>(urole);
            //}
            return BusinessEnum.AccountVerificationStatus.Success;


        }

        public Boolean UpdateSystemAccount(UserProfile uprofile)
        {
            _Log.Debug("UpdateSystemAccount - uprofile " + uprofile.Id);

            return _Repository.Update<UserProfile>(uprofile.Id, uprofile);
        }

        public Boolean UpdateExistingRoleForUserProfile(int userprofileId, int roleidtochangeto)
        {
            _Log.Debug(String.Format("UpdateExistingRoleForUserProfile : {0} {1} ", userprofileId, roleidtochangeto));

            //UserInRole userRole = _Repository.GetAll<UserInRole>().Where(x => x.Id == userprofileId).SingleOrDefault();
            UserInRole userRole = _Repository.GetFiltered<UserInRole>().Where(x => x.Id == userprofileId).SingleOrDefault();

            if (userRole != null)
            {
                userRole.RoleId = roleidtochangeto;
                return _Repository.Update<UserInRole>(userRole.Id, userRole);
            }

            return false;

        }

        public BusinessEnum.CreateSystemAccountStatus CreateSystemAccount(UserProfile uprofile, int roleId)
        {
            _Log.Debug(String.Format("CreateSystemAccount : {0} {1}", uprofile.Id, roleId));

            if (DoesAccountExistByEmailAddress(uprofile.EmailAddress)) return BusinessEnum.CreateSystemAccountStatus.EmailAlreadyExists;

            MembershipTable mt = new MembershipTable
            {
                CreateDate = DateTime.UtcNow,
                Password = "na",
                IsConfirmed = true,
                PasswordChangedDate = DateTime.UtcNow,
                PasswordFailuresSinceLastSuccess = 0,
                PasswordSalt = string.Empty
            };

            string tempassphrase = GenerateRandomPassPhraseFromDatabase();

            mt.HashedPassphrase = GetHashedPassPhraseForString(tempassphrase);

            uprofile.MembershipTables = new List<MembershipTable>() { mt };

            int dbid = _Repository.Add<UserProfile>(uprofile);

            if (dbid <= 0) return BusinessEnum.CreateSystemAccountStatus.Fail;

            UserInRole urole = new UserInRole()
            {
                RoleId = roleId,
                UserId = dbid
            };

            if (String.IsNullOrEmpty(GetRolesForUser(uprofile)))
            {
                _Repository.Add<UserInRole>(urole);
            }

            // _EmailService.EmailSystemAccountTheirTemporaryPassPhase(uprofile.EmailAddress, tempassphrase, "TBD", dbid);

            return BusinessEnum.CreateSystemAccountStatus.Success;

        }

        public void ResendAccountSetupEmailAndResetPassphraseToSystemUser(UserProfile uprofile)
        {

            _Log.Debug(String.Format("ResendAccountSetupEmailAndResetPassphraseToSystemUser : {0}", uprofile.Id));

            string tempassphrase = GenerateRandomPassPhraseFromDatabase();

            MembershipTable mt = GetMemberShipByUserId(uprofile.Id);

            mt.HashedPassphrase = GetHashedPassPhraseForString(tempassphrase);

            _Repository.Update<MembershipTable>(mt.Id, mt);

            // _EmailService.EmailSystemAccountTheirTemporaryPassPhase(uprofile.EmailAddress, tempassphrase, "TBD", uprofile.Id);
        }


        public bool IsNewPassphrase(long userId, string passphrase)
        {
            _Log.Debug(String.Format("IsNewPassphrase  {0} | {1}", userId, passphrase));
            //List<PassphraseHistory> oldpassphrases = _Repository.GetAll<PassphraseHistory>().Where(x => x.UserId == userId).OrderByDescending(x => x.DateOfChange).Take(_Configuration.NumberHistoricPassphrasestoRecover).ToList();
            List<PassphraseHistory> oldpassphrases = _Repository.GetFiltered<PassphraseHistory>((x => x.UserId == userId)).OrderByDescending(x => x.DateOfChange).Take(_Configuration.NumberHistoricPassphrasestoRecover).ToList();

            if (oldpassphrases == null || !oldpassphrases.Any()) return true;

            byte[] hashedpassphrase = GetHashedPassPhraseForString(passphrase);

            foreach (PassphraseHistory history in oldpassphrases)
            {
                // if (GetHashedPassPhraseForString(passphrase) == history.PassphraseHash) return false; //// use this for string comparisons
                if (HashedPassPhrasesMatch(history.PassphraseHash, hashedpassphrase)) return false;
            }
            return true;
        }


        public bool IsValidPassphrase(string passphrase)
        {
            _Log.Debug("IsValidPassphrase");

            if (string.IsNullOrEmpty(passphrase)) return false;

            if (passphrase.Length < _Configuration.PassphraseMinimumLength) return false;

            return true;
        }


        //public bool DisableUser(UserProfile user)
        //{
        //    //_Log.Debug("DisableUser - user " + user.Username);

        //    //UserProfile currentUser = _Repository.GetAll<UserProfile>().Where(x => x.Username == user.Username).SingleOrDefault();

        //    //if (currentUser == null)
        //    //    throw new UserDoesNotExistsException();

        //    //currentUser.IsEnabled = false;

        //    //return _Repository.Update(currentUser.Id, currentUser);
        //    return true;
        //}


        //public bool PassphraseErrorSetLockOutOnAccount(UserProfile user, Boolean isLocked)
        //{
        //    //_Log.Debug("PassphraseLockOutUser - user " + user.Username);

        //    // UserProfile currentUser = _Repository.GetAll<UserProfile>().Where(x => x.Username == user.Username).SingleOrDefault();

        //    //if (currentUser == null)
        //    //    throw new UserDoesNotExistsException();

        //    //currentUser.IsLocked = isLocked;

        //    //if (!isLocked) currentUser.FailedAttempts = 0;

        //    //return _Repository.Update(currentUser.Id, currentUser);
        //    return true;
        //}


        //public bool EnableUser(UserProfile user)
        //{
        //    //_Log.Debug("EnableUser - user " + user.Username);

        //    //UserProfile enabledUser = _Repository.GetAll<UserProfile>().Where(x => x.Username == user.Username).SingleOrDefault();

        //    //if (enabledUser == null)
        //    //    throw new UserDoesNotExistsException();

        //    //enabledUser.IsEnabled = true;

        //    //return _Repository.Update(enabledUser.Id, enabledUser);
        //    return false;
        //}


        public Boolean ResetPasswordFailures(MembershipTable member)
        {
            _Log.Debug("ResetPasswordFailures ");

            member.PasswordFailuresSinceLastSuccess = 0;
            return _Repository.Update<MembershipTable>(member.Id, member);
        }

        public UserProfile GetUser(string username)
        {
            _Log.Debug("GetUser - user " + username);
            // eagerly load the membershiptable and the userinroles
            return _Repository.GetFiltered<UserProfile>((x => x.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase)), "MembershipTables")
               .SingleOrDefault();
        }

        public UserProfile GetUserCache(string username)
        {
            _Log.Debug("GetUserCache " + username);

            string uniquekey = "GetUserCache_" + username;

            if (Utilities.GetFromCache(uniquekey) != null)
            {
                return (UserProfile)Utilities.GetFromCache(uniquekey);
            }
            else
            {
                UserProfile selecteduser = _Repository.GetFiltered<UserProfile>((x => x.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase)))
               .SingleOrDefault();
                return Utilities.SetandGetCache<UserProfile>(uniquekey, selecteduser, 15);
            }
        }


        public UserProfile GetUserByEmailAddress(string emailaddress)
        {
            _Log.Debug("GetUserByEmailAddress - emailaddress " + emailaddress);

            return _Repository.GetFiltered<UserProfile>((x => x.EmailAddress.Equals(emailaddress, StringComparison.InvariantCultureIgnoreCase)), "MembershipTables", "UserInRole").SingleOrDefault();
        }

        public Boolean DoesAccountExistByEmailAddress(string emailaddress)
        {
            _Log.Debug("DoesAccountExist - emailaddress " + emailaddress);

            if (string.IsNullOrEmpty(emailaddress)) return true;

            string trimmedemailaddress = emailaddress.TrimCheckForNull();

            //UserProfile selecteduser = _Repository.GetAll<UserProfile>().Where(x => x.EmailAddress.Equals(trimmedemailaddress, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
            UserProfile selecteduser = _Repository.GetFiltered<UserProfile>((x => x.EmailAddress.Equals(trimmedemailaddress, StringComparison.InvariantCultureIgnoreCase))).SingleOrDefault();

            if (selecteduser == null) return false;

            return true;
        }



        public Boolean DoesAccountExistByUsername(string username)
        {
            _Log.Debug("DoesAccountExistByUsername - username " + username);

            if (string.IsNullOrEmpty(username)) return true;

            //UserProfile selecteduser = _Repository.GetAll<UserProfile>().Where(x => x.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
            UserProfile selecteduser = _Repository.GetFiltered<UserProfile>((x => x.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))).SingleOrDefault();

            if (selecteduser == null) return false;

            return true;
        }

        public Boolean HashedPassPhrasesMatch(byte[] p1, byte[] p2)
        {
            _Log.Debug("HashedPassPhrasesMatch");
            return p1.SequenceEqual(p2);
        }

        public byte[] GetHashedPassPhraseForString(string stringtohash)
        {
            _Log.Debug("GetHashedPassPhraseFromString");

            if (string.IsNullOrEmpty(stringtohash))
                return null;

            SHA256 hash = SHA256.Create();

            return hash.ComputeHash(UnicodeEncoding.Default.GetBytes(stringtohash.TrimCheckForNull()));
        }


        public BusinessEnum.PassphraseReset EmailTemporaryPassPhrase(string emailaddress, string baseURL)
        {
            _Log.Debug("EmailTemporaryPassPhrase  " + emailaddress);

            string tempassphrase = GenerateRandomPassPhraseFromDatabase();

            UserProfile selecteduser = GetUserByEmailAddress(emailaddress);

            if (selecteduser == null) return BusinessEnum.PassphraseReset.NoSuchUser;

            MembershipTable mt = selecteduser.MembershipTables.First();
            if (mt.IsConfirmed == false) return BusinessEnum.PassphraseReset.Accountnotvalidated;

            string passwordtoken = "TBD";

            if (_EmailService.SendTemporaryPassphraseEmail(emailaddress, tempassphrase, passwordtoken, selecteduser.Id, baseURL))
            {
                return ((ChangeUserToTemporaryPasswordIssue(selecteduser, tempassphrase) == true) ? BusinessEnum.PassphraseReset.Success : BusinessEnum.PassphraseReset.Fail);
            }

            return BusinessEnum.PassphraseReset.Fail;

        }

        public Boolean ChangeUserToTemporaryPasswordIssue(UserProfile selecteduser, string tempassphrase)
        {
            _Log.Debug(string.Format("ChangeUserToTemporaryPasswordIssue {0}", selecteduser.Id));

            selecteduser.IsTemporaryPassPhrase = true;
            _Repository.Update<UserProfile>(selecteduser.Id, selecteduser);

            //MembershipTable mt = GetMemberShipByUserId(selecteduser.Id);
            MembershipTable mt = selecteduser.MembershipTables.First();

            AddPassPhraseToUserHistory(mt.Id, mt.HashedPassphrase);

            return AssignUserNewPassword(mt, GetHashedPassPhraseForString(tempassphrase));
        }

        private Boolean AssignUserNewPassword(MembershipTable mt, byte[] passphrase)
        {
            _Log.Debug(string.Format("AssignUserNewPassword {0}", mt.Id));

            mt.PasswordChangedDate = DateTime.UtcNow;
            mt.HashedPassphrase = passphrase;

            return _Repository.Update<MembershipTable>(mt.Id, mt);
        }


        public MembershipTable GetMemberShipByUserId(int userId)
        {
            _Log.Debug(String.Format("GetMemberShipByUserId - userId {0} ", userId));

            //return _Repository.GetAll<MembershipTable>().Where(x => x.UserProfileId == userId).SingleOrDefault();
            return _Repository.GetFiltered<MembershipTable>((x => x.UserProfileId == userId)).SingleOrDefault();
        }

        public long AddPassPhraseToUserHistory(int userId, byte[] hashedpassphrase)
        {
            _Log.Debug(String.Format("AddPassPhraseToUserHistory - userId {0}", userId));

            TrimPassphraseHistory(userId);

            PassphraseHistory pt = new PassphraseHistory();
            pt.UserId = userId;
            pt.DateOfChange = DateTime.UtcNow;
            pt.PassphraseHash = hashedpassphrase;
            return _Repository.Add<PassphraseHistory>(pt);
        }

        public BusinessEnum.ResendAccountSignupEmails SendAccountConfirmationEmailBasedOnEmailAddress(string emailaddress, string baseUrl)
        {
            _Log.Debug(string.Format("SendAccountConfirmationEmailBasedOnEmailAddress {0}", emailaddress));

            UserProfile up = GetUserByEmailAddress(emailaddress.TrimCheckForNull());

            if (up == null || up.Id <= 0) return BusinessEnum.ResendAccountSignupEmails.Fail;

            if (up.MembershipTables.SingleOrDefault().IsConfirmed) return BusinessEnum.ResendAccountSignupEmails.Accountalreadyvalidated;


            if (up.UserInRole.Where(x => x.RoleId == _Configuration.RoleUserID || x.RoleId == _Configuration.RoleAdminID).Any())
            {
                // the account is a shell system user hence fire of welcome email.
                ResendAccountSetupEmailAndResetPassphraseToSystemUser(up);
            }
            else
            {
                // check that they are a team
                int countryid = 0;
                int eventid = 0;
                //if (up.Teams != null && up.Teams.Count > 0)
                //{
                //    countryid = up.Teams.SingleOrDefault().CountryId;
                //}
                // else their countryid is in the temparary confirmation url


                //tem=6D59DCD0E98A5A0242DACF32EB9EFDBD4D7A3A30ABA30AB8C038735354900C9E&tev=6637EFD25A451A6C&ti=8B731E4ECCA8B5A8&ci=1CF70CCFA3C5D375&p=5367CD6E9B7AAC6C&dcid=E6D26F0586AF9F05
                foreach (string str in up.TeamConfirmationURL.Split('&'))
                {
                    if (str.Contains("ci="))
                    {
                        countryid = (str.Split('=')[1]).DecryptToInt();
                        break;
                    }
                }

                foreach (string str in up.TeamConfirmationURL.Split('&'))
                {
                    if (str.Contains("tev="))
                    {
                        eventid = (str.Split('=')[1]).DecryptToInt();
                        break;
                    }
                }

                // _EmailService.SendAccountConfirmation(countryid, emailaddress.TrimCheckForNull(), up.TeamConfirmationURL, baseUrl, eventid);
            }
            return BusinessEnum.ResendAccountSignupEmails.Success;
        }


        public void StoreConfirmationURLInUserProfileUntilValidated(int dbid, string urllink)
        {
            _Log.Debug(string.Format("StoreConfirmationURLInUserProfileUntilValidated {0} | {1}", dbid, urllink));

            UserProfile up = GetUserProfileById(dbid);
            up.TeamConfirmationURL = urllink;

            _Repository.Update<UserProfile>(up.Id, up);

        }

        public void TrimPassphraseHistory(int userId)
        {
            _Log.Debug(String.Format("TrimPassphraseHistory - userId {0}", userId));

            List<PassphraseHistory> oldpassphrases = _Repository.GetAll<PassphraseHistory>().Where(x => x.UserId == userId).OrderByDescending(x => x.DateOfChange).Take(_Configuration.NumberHistoricPassphrasestoRecover).ToList();

            if (oldpassphrases != null && oldpassphrases.Any() && oldpassphrases.Count > _Configuration.NumberHistoricPassphrasestoRecover)
            {
                int counter = 0;
                try
                {
                    for (counter = _Configuration.NumberHistoricPassphrasestoRecover; counter < oldpassphrases.Count; counter++)
                    {
                        _Repository.DeleteById<PassphraseHistory>(oldpassphrases[counter].Id);
                    }
                }
                catch (Exception err)
                {
                    _Log.Error(String.Format("TrimPassphraseHistory - counter {0} {1}}", counter, err.Message));
                }
            }
        }

        public Boolean IsUserProfileActive(int userid)
        {
            _Log.Debug(string.Format("IsUserProfileActive {0}", userid));

            //return _Repository.GetAll<UserProfile>().Where(x => x.Id == userid).SingleOrDefault().IsActive;
            return _Repository.GetFiltered<UserProfile>((x => x.Id == userid)).SingleOrDefault().IsActive;
        }

        public Boolean SetActiveStateByUserProfileId(int userid, Boolean isActive)
        {
            _Log.Debug(string.Format("SetActiveStateByUserProfileId {0} {1}", userid, isActive));

            //UserProfile uprofile = _Repository.GetAll<UserProfile>().Where(x => x.Id == userid).SingleOrDefault();
            UserProfile uprofile = _Repository.GetFiltered<UserProfile>((x => x.Id == userid)).SingleOrDefault();
            uprofile.IsActive = isActive;
            return _Repository.Update<UserProfile>(uprofile.Id, uprofile);
        }

        public Boolean UpdateIsConfirmedStatusByUserProfileId(int userprofileId)
        {
            _Log.Debug(string.Format("UpdateIsConfirmedStatusByUserProfileId {0} {1}", userprofileId, ""));

            // MembershipTable mt = _Repository.GetAll<MembershipTable>().Where(x => x.UserProfileId == userprofileId).SingleOrDefault();
            MembershipTable mt = _Repository.GetFiltered<MembershipTable>((x => x.UserProfileId == userprofileId)).SingleOrDefault();

            if (mt.UserProfileId == userprofileId && mt.IsConfirmed == false)
            {
                mt.IsConfirmed = true;
                return _Repository.Update<MembershipTable>(mt.Id, mt);
            }
            return false;
        }

        public List<UserProfile> GetAllUsers()
        {
            _Log.Debug(string.Format("GetAllUsers {0}", ""));

            List<UserProfile> listofusers = _Repository.GetAll<UserProfile>().ToList();

            return listofusers;
        }

        public List<UserProfile> GetAllSystemUsers()
        {
            _Log.Debug(string.Format("GetAllSystemUsers {0}", ""));

            List<UserInRole> listofUsersinroles = _Repository.GetAll<UserInRole>().Where(x => x.RoleId == _Configuration.RoleAdminID || x.RoleId == _Configuration.RoleUserID).ToList();

            // TODO - use eager laodign to make trhis more efficient
            List<UserProfile> listofusers = _Repository.GetAll<UserProfile>().Join(listofUsersinroles, e => e.Id, m => m.UserId, (e, m) => e).OrderBy(e => e.Surname).ToList();

            return listofusers;
        }

        public UserProfile GetUserProfileById(int id)
        {
            return _Repository.GetAll<UserProfile>("MembershipTables", "UserInRole").Where(x => x.Id == id).SingleOrDefault();
        }


        public List<UserProfile> GetAllUserProfileWhoHaventConfirmedAccount()
        {
            _Log.Debug(string.Format("GetAllUserProfileWhoHaventConfirmedAccount {0}", ""));
            //return _Repository.GetAll<UserProfile>().Where(x => x.TeamConfirmationURL.IsNotNullOrEmpty()).ToList();
            return _Repository.GetFiltered<UserProfile>((x => x.TeamConfirmationURL != null && x.TeamConfirmationURL.Length > 0)).ToList();
        }

        public void RemoveCachedItemsWithUsernameInKey(string username)
        {

            _Log.Debug(string.Format("RemoveCachedItemsWithUsernameInKey {0}", username));

            if (String.IsNullOrEmpty(username)) return;

            try
            {
                HttpContext oc = HttpContext.Current;
                foreach (var c in oc.Cache)
                {
                    string key = ((DictionaryEntry)c).Key.ToString();
                    if (key.Contains(username.ToUpper()))
                    {
                        oc.Cache.Remove(key);
                    }
                }
            }
            catch (Exception)
            {

            }
        }



        //private bool DoesUserAlreadyExist(string username)
        //{
        //    _Log.Debug("DoesUserAlreadyExist - user " + username);

        //    return GetUser(username) != null;
        //}

        //public byte[] GetHash(string stringtohash)
        //{
        //    if (string.IsNullOrEmpty(stringtohash))
        //        return null;

        //    SHA256 hash = SHA256.Create();

        //    return hash.ComputeHash(UnicodeEncoding.Default.GetBytes(stringtohash));
        //}


        public string GenerateRandomPassPhraseFromDatabase()
        {

            _Log.Debug(String.Format("GenerateRandomPassPhraseFromDatabase {0}", ""));
            List<PassphraseWord> groups = _Repository.GetAll<PassphraseWord>().ToList();

            int[] order = new int[4];
            var cryptoProvider = new RNGCryptoServiceProvider();

            var randomBytes = new byte[4];
            cryptoProvider.GetBytes(randomBytes);
            int randomNumber = Math.Abs(BitConverter.ToInt32(randomBytes, 0));

            for (var x = 0; x != 4; x++)
            {
                //get a random number 
                var number = (randomNumber % 4) + 1;
                while (order.All(o => order.Contains(number)))
                {
                    //the number is in the list so get other one.
                    randomBytes = new byte[4];
                    cryptoProvider.GetBytes(randomBytes);
                    randomNumber = Math.Abs(BitConverter.ToInt32(randomBytes, 0));
                    number = (randomNumber % 4) + 1;
                }
                order[x] = number;

            }

            StringBuilder sb = new StringBuilder();
            for (var x = 0; x != 4; x++)
            {
                randomBytes = new byte[4];
                cryptoProvider.GetBytes(randomBytes);
                randomNumber = Math.Abs(BitConverter.ToInt32(randomBytes, 0));
                int location = (randomNumber % groups.Count) + 1;
                switch (order[x])
                {
                    case 1:
                        sb.Append(groups[location - 1].Group1.Decrypt());
                        break;
                    case 2:
                        sb.Append(groups[location - 1].Group2.Decrypt());
                        break;
                    case 3:
                        sb.Append(groups[location - 1].Group3.Decrypt());
                        break;
                    case 4:
                        sb.Append(groups[location - 1].Group4.Decrypt());
                        break;
                }
                sb.Append(" ");
            }

            return sb.ToString().TrimEnd();
        }


        public void EncryptPassphraseDB()
        {
            _Log.Debug(string.Format("EncryptPassphraseDB {0}", ""));

            List<PassphraseWord> listofwords = _Repository.GetAll<PassphraseWord>().ToList();

            foreach (PassphraseWord pw in listofwords)
            {
                pw.Group1 = pw.Group1.Encrypt();
                pw.Group2 = pw.Group2.Encrypt();
                pw.Group3 = pw.Group3.Encrypt();
                pw.Group4 = pw.Group4.Encrypt();
            }

            _Repository.UpdateAll<PassphraseWord>(listofwords);
        }


        public void GenerateNewPassphraseDB()
        {
            _Log.Debug(string.Format("GeneratePassphraseDB {0}", ""));

            List<PassphraseWord> listofwords = _Repository.GetAll<PassphraseWord>().ToList();
            foreach (PassphraseWord pw in listofwords)
            {
                _Repository.DeleteById<PassphraseWord>(pw.Id);
            }

            string[] word1 = new string[] { "Aardvark", "Ant", "Alligator", "Sheep", "Monkey", "Fox", "Walrus", "Cat", "Dog", "Lion", "Tiger", "Gerbil", "Antelope", "Squirrel", "Goat", "Eleven", "Fifteen", "Calculator" };
            string[] word2 = new string[] { "Table", "Chair", "Sofa", "Floor", "Ceiling", "Carpet", "Wall", "Window", "Curtain", "Door", "Stair", "Cupboard", "Hallway", "Kitchen", "Recliner", "Twelve", "Sixteen", "Computer" };
            string[] word3 = new string[] { "Carrot", "Pea", "Turnip", "Leek", "Barley", "Onion", "Asparagus", "Garlic", "Cloves", "Herb", "Celery", "Potatoes", "Lettuice", "Pickle", "Mushroom", "Thirteen", "Seventen", "Abacus" };
            string[] word4 = new string[] { "Blue", "Green", "Yellow", "Pink", "Cyan", "Beige", "Vanilla", "Fuscia", "Red", "Teal", "Black", "White", "Magenta", "Grey", "Clear", "Fourteen", "Eighteen", "Pencil" };

            for (int i = 0; i < word1.Length; i++)
            {
                PassphraseWord pw = new PassphraseWord()
                {
                    Group1 = word1[i].Encrypt(),
                    Group2 = word2[i].Encrypt(),
                    Group3 = word3[i].Encrypt(),
                    Group4 = word4[i].Encrypt()
                };
                _Repository.Add<PassphraseWord>(pw);
            }
        }

        public void DecryptPassphraseDB()
        {
            _Log.Debug(string.Format("DecryptPassphraseDB {0}", ""));

            List<PassphraseWord> listofwords = _Repository.GetAll<PassphraseWord>().ToList();

            foreach (PassphraseWord pw in listofwords)
            {
                pw.Group1 = pw.Group1.Decrypt();
                pw.Group2 = pw.Group2.Decrypt();
                pw.Group3 = pw.Group3.Decrypt();
                pw.Group4 = pw.Group4.Decrypt();
            }

            _Repository.UpdateAll<PassphraseWord>(listofwords);
        }


        #region Helper methods

        public string BuildPassphrase(string part1, string part2, string part3, string part4)
        {
            _Log.Debug("BuildPassphrase ");
            return string.Format("{0}{1}{2}{3}", part1, part2, part3, part4);
        }

        #endregion

    }
}
