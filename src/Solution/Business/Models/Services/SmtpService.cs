﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace MyApp.Business.Services
{
    public class SmtpService : ISmtpService
    {
        private IConfiguration _Configuration;
        private ILog _Log;

        public SmtpService(ILog log, IConfiguration configuration)
        {
            _Configuration = configuration;
            _Log = log;
        }

        public Boolean Send(string to, string fromAddress, string fromName, string subject, string body, string[] cc = null, bool isHTML = false, List<Attachment> attachments = null)
        {

            _Log.Debug(string.Format("Send : {0} | {1} | {2} | {3}", to, fromAddress, fromName, subject));
            try
            {
                MailMessage message = new MailMessage
                {
                    From = new MailAddress(fromAddress, fromName),
                    Subject = subject,
                    Body = body // + Resource.EmailResource.emailfooter,
                };

                message.IsBodyHtml = isHTML;

                if (cc != null)
                {
                    foreach (string ccEmail in cc)
                    {
                        message.CC.Add(new MailAddress(ccEmail));
                    }
                }

                message.To.Add(new MailAddress(to));

                if (attachments != null)
                {

                    foreach (Attachment item in attachments)
                    {
                        message.Attachments.Add(item);
                    }
                }

                SmtpClient smtpClient = CreateSmtpClient();
                smtpClient.Send(message);
                return true;
            }
            catch (Exception err)
            {
                return false;
            }
        }

        private SmtpClient CreateSmtpClient()
        {
            SmtpClient smtpClient = new SmtpClient(_Configuration.SmtpHost, _Configuration.SmtpPort);

            smtpClient.EnableSsl = _Configuration.SmtpSmtpRequiresSsl;

            if (_Configuration.SmtpCredentials != null)
            {
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = _Configuration.SmtpCredentials;
            }

            return smtpClient;
        }
    }
}
