﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenPlatform.General;

namespace MyApp.Business.DomainObjects.Models
{
    public class ZoneEngagement
    {
        public int Id { get; set; }

        public int? UserProfileId { get; set; }

        public string UnregisteredId { get; set; }

        public int ZoneId { get; set; }

        public DateTime DateAdded { get; set; }

        public virtual Zone Zone { get; set; }

        public virtual UserProfile UserProfile { get; set; }
        public ZoneEngagement()
        {

        }

    }



}
