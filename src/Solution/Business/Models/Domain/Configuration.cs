﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MyApp.Business.Services;
using System.Web;

namespace MyApp.Business.DomainObjects.Models
{
    public class Configuration : IConfiguration
    {

        private IAppConfigurationService _appConfigurationService;
        private ILog _Log;
        private System.Configuration.AppSettingsReader _SettingsReader;


        public Configuration(IAppConfigurationService appConfigurationService, ILog _log)
        {
            _appConfigurationService = appConfigurationService;
            _Log = _log;
            _SettingsReader = new System.Configuration.AppSettingsReader();
        }

        public string AppVersion
        {
            get
            {
                return "2.09 16/10/2013 SVN 337";
            }
        }


        public string QRCodeZonePrefix
        {
            get
            {
                return "ZC_";
            }
        }

        public string QRCodeUserPrefix
        {
            get
            {
                return "UC_";
            }
        }

        public string SmtpSenderEmailAddress
        {
            get
            {
                return _SettingsReader.GetValue("SmtpSenderEmailAddress", typeof(String)).ToString();
            }
        }

        public string SmtpSenderName
        {
            get
            {
                return _SettingsReader.GetValue("SmtpSenderName", typeof(String)).ToString();
            }
        }

        public string FromEmailAddress
        {
            get
            {
                return _appConfigurationService.GetConfigurationByKey<string>("FromEmailAddress");
                //return _SettingsReader.GetValue("FromEmailAddress", typeof(String)).ToString();

            }
        }


        public string SmtpHost
        {
            get
            {
                return _SettingsReader.GetValue("SmtpHost", typeof(String)).ToString();
            }
        }

        public int SmtpPort
        {
            get
            {
                return int.Parse(_SettingsReader.GetValue("SmtpPort", typeof(string)).ToString());
            }
        }

        public int MaxUnregisteredZonesAllowed
        {
            get
            {
                return 4000;
            }
        }

        public int MaxUnregisteredZonesAllowedPerRequest
        {
            get
            {
                return 500;
            }
        }

        public string UnregisteredUrlContains
        {
            get
            {
                return "/Account/Unregistered?eid=";
            }
        }



        public bool SmtpSmtpRequiresSsl
        {
            get
            {
                return Boolean.Parse(_SettingsReader.GetValue("SmtpSmtpRequiresSsl", typeof(string)).ToString());

            }
        }

        public NetworkCredential SmtpCredentials
        {
            get
            {


                return new NetworkCredential(_SettingsReader.GetValue("SmtpCredentials_Username", typeof(String)).ToString(), _SettingsReader.GetValue("SmtpCredentials_Password", typeof(String)).ToString());
            }
        }


        public int PassphraseMinimumLength
        {
            get
            {
                return 15;
            }
        }


        public int MonthsPassphraseisValidFor
        {
            get
            {
                return 12;
            }
        }

        public int NumberHistoricPassphrasestoRecover
        {
            get
            {
                return 14;
            }
        }

        public int MaximumNumberOfFailedLoginAttempts
        {
            get
            {
                return 5;
            }
        }
        public int AccountLockoutPeriodInMins
        {
            get
            {
                return 15;
            }
        }

        public string RoleDev
        {
            get
            {
                return "DEV";
            }
        }

        public int RoleDevID
        {
            get
            {
                return 1;
            }
        }

        public string RoleAdmin
        {
            get
            {
                return "ADMIN";
            }
        }

        public int RoleAdminID
        {
            get
            {
                return 2;
            }
        }

        public string RoleUser
        {
            get
            {
                return "USER";
            }
        }

        public int RoleUserID
        {
            get
            {
                return 4;
            }
        }

        public string RoleSuperUser
        {
            get
            {
                return "SUPERUSER";
            }
        }

        public int RoleSuperUserID
        {
            get
            {
                return 3;
            }
        }

        public string BaseURL
        {
            get
            {
                return string.Format(@"{0}://{1}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Authority);
            }
        }


        public int DurationofAutheticationTicket
        {
            get
            {
                return 30;
            }
        }

        public string PostinstalaltionCheckEmailDestinationemailaddress
        {
            get
            {
                return _SettingsReader.GetValue("Postinstllationtestemailaddress_to", typeof(String)).ToString();
            }
        }

        // DO NOT CHANGE THIS AT ALL - IF YOU DO -SEARCH FOR ANY HARDCODED REFERENCES
        public string SessionKeyForUserProfile
        {
            get
            {
                return "SESSION_EVENTTEAM_OP2013";
            }
        }

        // DO NOT CHANGE THIS AT ALL - IF YOU DO -SEARCH FOR ANY HARDCODED REFERENCES
        public string SessionKeyForCaptchaRandomText
        {
            get
            {
                return "SESSION_CAPTCHARANDOM_OP2013";
            }
        }




        public int Thumbnailwidth
        {
            get
            {
                return 48;
            }
        }

        public int Thumbnailheight
        {
            get
            {
                return 48;
            }
        }

        public string HomePageSystemInfo
        {
            get
            {
                return _appConfigurationService.GetConfigurationByKey<string>("HomePageSystemInfo");
            }
        }

        public Boolean DisableLogin
        {
            get
            {
                return _appConfigurationService.GetConfigurationByKey<Boolean>("DisableLogin");
            }
        }

        public Boolean SendInformationEmails
        {
            get
            {
                return _appConfigurationService.GetConfigurationByKey<Boolean>("SendInformationEmails");
            }
        }

    }
}
