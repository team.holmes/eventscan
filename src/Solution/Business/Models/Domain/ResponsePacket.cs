﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenPlatform.General;

namespace MyApp.Business.DomainObjects.Models
{

    public class ResponsePacket
    {
       
        public int ResultCode { get; set; }

        public object ResultData { get; set; }

        public string ResultMessage { get; set; }

        public DateTime ResultDateTime
        {
            get
            {
                return DateTime.Now;

            }
        }

        public ResponsePacket()
        {
        }

    }



}
