﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenPlatform.General;

namespace MyApp.Business.DomainObjects.Models
{
    public class PassphraseHistory 
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public DateTime DateOfChange { get; set; }

        public byte[] PassphraseHash { get; set; }


        public PassphraseHistory()
        {

        }

    }


     
}
