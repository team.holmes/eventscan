﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenPlatform.General;

namespace MyApp.Business.DomainObjects.Models
{
    public class BusinessEnum
    {


        public enum UnconfirmedTeamCreateAccountIssues
        {
            EmailAlreadyExists = -1,
            Success = -2,
            InvalidPassphrase = -3,
            Failure = -4
        };


        public enum UnregisteredZoneIndex
        {
            Success = 1,
            Fail = 2,
            ExceedMax = 3,
            TooMuchRequested = 4
        };


        public enum UnregisteredRequestIndex
        {
            Success = 1,
            Fail = 2,
            ExceedMax = 3,
            TooMuchRequested = 4
        };



        public enum AuthenticationResult
        {
            Failed = 1,
            Locked = 2,
            PassphraseExpired = 3,
            Passed = 4,
            AccountValidated = 5,
            TempPasswordIssued = 6,
            AccountNotActive = 7,
            AccountNotConfirmed_Validated = 8
        };

        public enum PassphraseCreation
        {
            PreviouslyUsed = 1,
            NotValidPassPhrase = 2,
            NoUserFound = 3,
            SameAsExisiting = 4,
            Success = 5,
            Failure = 6,
            Existingpasswordnotmatched = 7
        };


     

        public enum AccountVerificationStatus
        {
            Success = 1,
            AlreadyVerified = 2,
            Failed = 3

        };


        public enum PassphraseReset
        {
            Success = 1,
            Fail = 2,
            NoSuchUser = 3,
            Accountnotvalidated = 4

        };

        public enum ZoneAdd
        {
            Success = 1,
            Fail = 2,
            DuplicateName = 3
        };



        public enum UnregisteredAdd
        {
            Success = 1,
            Fail = 2,
            DuplicateName = 3
        };



        public enum CreateSystemAccountStatus
        {
            Success = 1,
            EmailAlreadyExists = 2,
            Fail = 3

        };

        public enum ResendAccountSignupEmails
        {
            Success = 1,
            Fail = 2,
            Accountalreadyvalidated = 3

        };


        public enum FormAction
        {
            Add = 1,
            Edit = 2

        };

        public enum ResponseCodes
        {
            Result_Fail = -1,
            Result_InvalidParamaters = -2,
            Result_DecryptionError = -3,
            Result_Duplication = -4,
            Result_ZoneDoesntExist = -5,
            Result_ZonePrefixIsntCorrect = -6,
            Result_UserProfilesDoesntExist = -7,
            Result_KeyValuePairsMustBeUnique = -8,
            Result_Success = 1
        }




        public BusinessEnum()
        {

        }

    }



}
