﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenPlatform.General;

namespace MyApp.Business.DomainObjects.Models
{
    public class Zone
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Identifier { get; set; }

        public Boolean IsActive { get; set; }

        public int UserProfileId { get; set; }

        public virtual List<ZoneEngagement> ZoneEngagements { get; set; }

        public Zone()
        {

        }

    }



}
