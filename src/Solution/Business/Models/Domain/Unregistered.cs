﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenPlatform.General;

namespace MyApp.Business.DomainObjects.Models
{
    public class Unregistered
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Boolean IsActive { get; set; }

        public int UserProfileId { get; set; }

        public int UnregisteredCodeIndex { get; set; }

        public int MaxUnregisteredCodesAllowed { get; set; }

        public Unregistered()
        {

        }

    }



}
