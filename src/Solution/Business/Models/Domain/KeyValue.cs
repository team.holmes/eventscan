﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenPlatform.General;

namespace MyApp.Business.DomainObjects.Models
{
    public class KeyValue
    {
        public int Id { get; set; }

        public string EngagementId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public DateTime DateAdded { get; set; }

        public KeyValue()
        {

        }

    }



}
