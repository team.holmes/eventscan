using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using MyApp.Business.DomainObjects.Mapping;
using MyApp.Business.DomainObjects.Models;

namespace MyApp.DAL.Repository
{
    internal class DBContext : DbContext
    {
        static DBContext()
        {
            Database.SetInitializer<DBContext>(null);
        }

        public DBContext()
            : base("Name=DefaultConnection")
        {

        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<AppConfiguration> AppConfigurations { get; set; }

        public DbSet<PassphraseHistory> PassphraseHistories { get; set; }
        public DbSet<PassphraseWord> PassphraseWords { get; set; }

        public DbSet<MembershipTable> MembershipTables { get; set; }

        public DbSet<Role> Roles { get; set; }
        public DbSet<UserInRole> UserInRoles { get; set; }

        public DbSet<Zone> Zones { get; set; }
        public DbSet<Unregistered> Unregisterds { get; set; }
        public DbSet<KeyValue> KeyValues { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Configurations.Add(new UserProfileMap());
            modelBuilder.Configurations.Add(new MembershipMap());
            modelBuilder.Configurations.Add(new AppConfigurationMap());


            modelBuilder.Configurations.Add(new PassphraseHistoryMap());
            modelBuilder.Configurations.Add(new PassphraseWordMap());

            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new UserInRoleMap());

            modelBuilder.Configurations.Add(new ZoneMap());
            modelBuilder.Configurations.Add(new ZoneEngagementMap());
            modelBuilder.Configurations.Add(new CompanyMap());
            modelBuilder.Configurations.Add(new UnregisteredMap());
            modelBuilder.Configurations.Add(new KeyValueMap());
            



            modelBuilder.Entity<UserInRole>().HasRequired(t => t.UserProfile).WithMany(u => u.UserInRole).HasForeignKey(t => t.UserId);

        }
    }
}
