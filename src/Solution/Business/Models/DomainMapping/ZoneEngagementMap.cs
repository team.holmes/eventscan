﻿using System.Data.Entity.ModelConfiguration;
using MyApp.Business.DomainObjects.Models;

namespace MyApp.Business.DomainObjects.Mapping
{
    internal class ZoneEngagementMap : EntityTypeConfiguration<ZoneEngagement>
    {
        internal ZoneEngagementMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("ZoneEngagement");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserProfileId).HasColumnName("UserProfileId");
            this.Property(t => t.ZoneId).HasColumnName("ZoneId");
            this.Property(t => t.DateAdded).HasColumnName("DateAdded");
        }
    }
}
