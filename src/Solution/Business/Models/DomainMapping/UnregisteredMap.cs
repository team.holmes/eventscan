﻿using System.Data.Entity.ModelConfiguration;
using MyApp.Business.DomainObjects.Models;

namespace MyApp.Business.DomainObjects.Mapping
{
    internal class UnregisteredMap : EntityTypeConfiguration<Unregistered>
    {
        internal UnregisteredMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Unregistered");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.UserProfileId).HasColumnName("UserProfileId");
            this.Property(t => t.UnregisteredCodeIndex).HasColumnName("UnregisteredCodeIndex");
            this.Property(t => t.MaxUnregisteredCodesAllowed).HasColumnName("MaxUnregisteredCodesAllowed");
            
            
            
        }
    }
}
