﻿using System.Data.Entity.ModelConfiguration;
using MyApp.Business.DomainObjects.Models;

namespace MyApp.Business.DomainObjects.Mapping
{
    internal class ZoneMap : EntityTypeConfiguration<Zone>
    {
        internal ZoneMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Zone");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Identifier).HasColumnName("Identifier");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.UserProfileId).HasColumnName("UserProfileId");


        }
    }
}
