﻿using System.Data.Entity.ModelConfiguration;
using MyApp.Business.DomainObjects.Models;

namespace MyApp.Business.DomainObjects.Mapping
{
    internal class KeyValueMap : EntityTypeConfiguration<KeyValue>
    {
        internal KeyValueMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("KeyValue");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.EngagementId).HasColumnName("EngagementId");
            this.Property(t => t.Key).HasColumnName("Key");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.DateAdded).HasColumnName("DateAdded");
        }
    }
}
