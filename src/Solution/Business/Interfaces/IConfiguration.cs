﻿using System;
using System.Net;
namespace MyApp.Business.Services
{
    public interface IConfiguration
    {
        string SmtpHost { get; }
        int SmtpPort { get; }
        string SmtpSenderEmailAddress { get; }
        string SmtpSenderName { get; }
        bool SmtpSmtpRequiresSsl { get; }
        NetworkCredential SmtpCredentials { get; }
        int PassphraseMinimumLength { get; }
        int MonthsPassphraseisValidFor { get; }
        int NumberHistoricPassphrasestoRecover { get; }
        int AccountLockoutPeriodInMins { get; }
        int MaximumNumberOfFailedLoginAttempts { get; }
        int MaxUnregisteredZonesAllowed { get; }
        int MaxUnregisteredZonesAllowedPerRequest { get; }
        string RoleDev { get; }
        int RoleDevID { get; }
        string RoleAdmin { get; }
        int RoleAdminID { get; }
        string RoleUser { get; }
        int RoleUserID { get; }
        string UnregisteredUrlContains { get; }
        string QRCodeZonePrefix { get; }

        string QRCodeUserPrefix { get; }

        string RoleSuperUser { get; }
        int RoleSuperUserID { get; }

        string FromEmailAddress { get; }



        string BaseURL { get; }
        int DurationofAutheticationTicket { get; }
        string PostinstalaltionCheckEmailDestinationemailaddress { get; }
        string SessionKeyForUserProfile { get; }
        string AppVersion { get; }
        int Thumbnailheight { get; }
        int Thumbnailwidth { get; }
        string HomePageSystemInfo { get; }
        Boolean DisableLogin { get; }





        Boolean SendInformationEmails { get; }



    }
}
