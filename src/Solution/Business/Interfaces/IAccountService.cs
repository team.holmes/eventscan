﻿using MyApp.Business.DomainObjects.Models;
using System;
using System.Collections.Generic;
namespace MyApp.Business.Services
{
    public interface IAccountService
    {
        long AddPassPhraseToUserHistory(int userId, byte[] hashedpassphrase);
        string BuildPassphrase(string part1, string part2, string part3, string part4);
        Boolean ChangePassPhraseAndStoreOldPassPhraseInHistory(MembershipTable currentmembership, UserProfile selecteduser, byte[] currentpassphrasehash, byte[] newhashedpassphrase);
        bool DoesAccountExistByEmailAddress(string emailaddress);
        BusinessEnum.PassphraseReset EmailTemporaryPassPhrase(string emailaddress, string baseURL);
        string GenerateRandomPassPhraseFromDatabase();
        MembershipTable GetMemberShipByUserId(int userId);
        UserProfile GetUser(string username);
        UserProfile GetUserByEmailAddress(string emailaddress);
        bool HasPasswordExpired(global::MyApp.Business.DomainObjects.Models.MembershipTable selectedmembership);
        bool IsNewPassphrase(long userId, string passphrase);
        bool IsValidPassphrase(string passphrase);
        BusinessEnum.AuthenticationResult Login(string username, string password);
        bool LoginUser(global::MyApp.Business.DomainObjects.Models.UserProfile cuser, MembershipTable selectedmembership);
        void LogoutUser();
        bool RecordFailedLogin(UserProfile currentUser);
        bool ResetLoginFailureCounter(MembershipTable selectedmembership);
        bool ResetPasswordFailures(MembershipTable member);
        void TrimPassphraseHistory(int userId);
        BusinessEnum.PassphraseCreation UpdatePassPhrase(string passwordtoken, string emailaddress, string existingpassphrase, long userId, string newpassphrase);
        Boolean ChangeUserToTemporaryPasswordIssue(UserProfile selecteduser, string tempassphrase);
        List<Role> GetAllRoles();
        byte[] GetHashedPassPhraseForString(string stringtohash);
        Boolean HashedPassPhrasesMatch(byte[] p1, byte[] p2);
        BusinessEnum.UnconfirmedTeamCreateAccountIssues CreateNewAccount(string emailaddress, string passphrase, int roleid, Boolean isConfirmed);
        Boolean DoesAccountExistByUsername(string username);
        BusinessEnum.AccountVerificationStatus ValidateUserAccount(string emailaddress);
        Boolean IsUserProfileActive(int userid);
        Boolean SetActiveStateByUserProfileId(int userid, Boolean isActive);
        List<UserProfile> GetAllSystemUsers();
        UserProfile GetUserProfileById(int id);
        BusinessEnum.CreateSystemAccountStatus CreateSystemAccount(UserProfile uprofile, int roleId);
        Boolean UpdateSystemAccount(UserProfile uprofile);
        void RemoveCachedItemsWithUsernameInKey(string username);
        Boolean UpdateExistingRoleForUserProfile(int userprofileId, int roleidtochangeto);
        int GetFirstRoleIdForUserbyUserId(int userprofileId);
        string GetRolesForUser(UserProfile cuser);
        void ResendAccountSetupEmailAndResetPassphraseToSystemUser(UserProfile uprofile);
        BusinessEnum.ResendAccountSignupEmails SendAccountConfirmationEmailBasedOnEmailAddress(string emailaddress, string baseUrl);
        void StoreConfirmationURLInUserProfileUntilValidated(int dbid, string urllink);
        void StoreRelevantUserDetailsInSession(UserProfile currentuser);
        SessionModel GetSessionModel();
        void UpdateSessionModel(SessionModel sesmodel);
        void EncryptPassphraseDB();
        void DecryptPassphraseDB();
        void ClearAuthenticationTicket();
        List<UserProfile> GetAllUserProfileWhoHaventConfirmedAccount();
        Boolean UpdateIsConfirmedStatusByUserProfileId(int userprofileId);
        List<UserProfile> GetAllUsers();

        UserProfile GetUserCache(string username);

        void GenerateNewPassphraseDB();

    }
}
