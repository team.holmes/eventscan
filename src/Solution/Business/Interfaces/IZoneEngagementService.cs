﻿using System;
using MyApp.Business.DomainObjects.Models;
using System.Collections.Generic;
namespace MyApp.Business.Services
{
    public interface IZoneEngagementService
    {
        BusinessEnum.ResponseCodes AddZoneEngagement(string qrcode, string zone, string dateadded);

        BusinessEnum.ResponseCodes AddKeyValuePair(string engagementId, string key, string value, string dateadded);
        List<Zone> GetAllZones(Boolean includeInactive);

        List<ZoneEngagement> GetAllZoneEngagements();

        Boolean IsUnregisteredQRCode(string qrcode);

        DateTime ParseStringFormattedDateTime(string datetime);

    }
}
