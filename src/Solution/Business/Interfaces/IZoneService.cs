﻿using System;
using MyApp.Business.DomainObjects.Models;
using System.Collections.Generic;
namespace MyApp.Business.Services
{
    public interface IZoneService
    {
        Zone GetZoneById(int zoneid);
        BusinessEnum.ZoneAdd Add(Zone zone);
        BusinessEnum.ZoneAdd Update(Zone zone);
        List<Zone> GetZonesOrderedByName(int userprofileid, Boolean inclueInactive);
        int GetTotalEngagementsByZoneId(int zoneid);
        Boolean DoesZoneNameAlreadyExist(string zonename, int userprofileID);
        List<ZoneEngagement> GetEngagementsByZone(int zoneid);
        void DeleteByZoneId(int zoneid);
        int GetTotalUnregisteredEngagementsByZoneId(int zoneid);
        int GetTotalRegisteredEngagementsByZoneId(int zoneid);


    }
}
