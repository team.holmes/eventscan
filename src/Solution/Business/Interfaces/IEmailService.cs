﻿using System;
using MyApp.Business.DomainObjects.Models;
namespace MyApp.Business.Services
{
    public interface IEmailService
    {

        Boolean SendTemporaryPassphraseEmail(string toemailaddress, string temppassphrase, string passwordtoken, long userid, string baseurl);
        Boolean SendAccountCreationConfirmation(int identityid, string emailaddressofrecipient, string baseurl);
        
        
    }
}
