﻿using MyApp.Business.DomainObjects.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace MyApp.Business.Services
{
    public interface IQRCodeService
    {
        //byte[] GenerateQRCode(string input, int qrlevel, int scale);
        byte[] GenerateQRCode(string input, int scale);
        MemoryStream GeneratePDFForZone(string documentpathandfilenmae, Zone zone);
        MemoryStream GeneratePDFForUser(string documentpathandfilenmae, UserProfile uprofile);
        // MemoryStream GeneratePDFForGroupUnregistered(string documentpathandfilenmae, Unregistered unreggroup, string baseurl);
        MemoryStream GeneratePDFForGroupUnregistered(string documentpathandfilenmae, Unregistered unreggroup, string baseurl, int currentIndex, int itemsperdocument);
        string GenerateUnregisteredEncryptedLink(int unregisteredgroupid, int currentindex, string baseurl);
        MemoryStream GenerateMultiPartPDFZipFOrGroupUnregistered(string documentpathandfilenmae, Unregistered unreggroup, string baseurl);
    }
}
