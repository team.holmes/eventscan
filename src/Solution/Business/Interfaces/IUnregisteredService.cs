﻿using System;
using MyApp.Business.DomainObjects.Models;
using System.Collections.Generic;
namespace MyApp.Business.Services
{
    public interface IUnregisteredService
    {

        List<Unregistered> GetUnregisteredOrderedByName(int userprofileid, Boolean inclueInactive);
        Unregistered GetUnregisteredById(int unregisteredif);

        void DeleteByUnregisteredId(int unregisteredid);

        BusinessEnum.UnregisteredAdd Add(Unregistered item);

        Boolean DoesUnregisteredNameAlreadyExist(string name, int userprofileID);
        BusinessEnum.UnregisteredAdd Update(Unregistered unregistered);

        BusinessEnum.UnregisteredRequestIndex UpdateUnregisteredRequestIndex(int numrequested, Unregistered unregistered);
        Boolean IsUnregisteredAmountRequestBelowMaxAllowed(int numrequested, Unregistered unreg);
        Boolean DoesUnregisteredGroupHaveAnyEngagements(int unreggroupid);

    }
}
