﻿using MyApp.Web.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using MyApp.Business.DomainObjects.Models;
using System.Web.Mvc;

namespace MyApp.Web.ViewModels
{

    public class UnregisteredListViewModel 
    {

        public string EncryptedId { get; set; }

        public string Name { get; set; }

        public int UnregisteredCodesRequested { get; set; }

        public int TotalCodesAvailable { get; set; }

        public Boolean CanDownloadPDF { get; set; }

        public Boolean CanAllocateMore { get; set; }

        public Boolean CanDeleteUnregisteredGroup { get; set; }

        public Boolean DoesZoneHaveEngagements { get; set; }


        public UnregisteredListViewModel()
        {
        }
    }

}
