﻿using MyApp.Web.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using MyApp.Business.DomainObjects.Models;
using System.Web.Mvc;

namespace MyApp.Web.ViewModels
{

    public class UnregisteredGroupCodesViewModel :BaseViewModel
    {

        public int TotalRequestedUnregisteredCodes { get; set; }

        public int MaxRequestedUnregisteredCodes { get; set; }

        [Required(ErrorMessageResourceName = "requiredfield", ErrorMessageResourceType = typeof(EVAResource))]
        [Range(1,9999)]
        [Display(Name = "requestedunregisteredcodes", ResourceType = typeof(EVAResource))]
        public int? Newlyrequested { get; set; }

        public string EncryptedId { get; set; }

        public Boolean CanAddMoreUnregistered { get; set; }



        public UnregisteredGroupCodesViewModel()
        {
        }
    }

}
