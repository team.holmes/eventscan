﻿using MyApp.Web.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using MyApp.Business.DomainObjects.Models;
using System.Web.Mvc;

namespace MyApp.Web.ViewModels
{

    public class UnregisteredIndexViewModel : BaseViewModel
    {

        public List<UnregisteredListViewModel> ListOfUnregistered { get; set; }

        public UnregisteredIndexViewModel()
        {
        }
    }

}
