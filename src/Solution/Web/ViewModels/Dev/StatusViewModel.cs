﻿using MyApp.Web.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using MyApp.Business.DomainObjects.Models;
using System.Web.Mvc;

namespace MyApp.Web.ViewModels
{

    public class StatusViewModel : BaseViewModel
    {

        public int QRCodeId { get; set; }

        public int ZoneId { get; set; }
        public string ZoneName { get; set; }
        public string EmailAddress { get; set; }

        public string DateAAdded { get; set; }

        public StatusViewModel()
        {

        }
    }

}
