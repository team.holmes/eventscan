﻿using MyApp.Web.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using MyApp.Business.DomainObjects.Models;
using System.Web.Mvc;

namespace MyApp.Web.ViewModels
{

    public class SuperUserIndexViewModel : BaseViewModel
    {

        public string QrCodeData { get; set; }

        public int Isencrypted { get; set; }

        public int QrCodeScale { get; set; }

        public SuperUserIndexViewModel()
        {


        }
    }

}
