﻿using MyApp.Web.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using MyApp.Business.DomainObjects.Models;
using System.Web.Mvc;

namespace MyApp.Web.ViewModels
{

    public class ZoneListViewModel 
    {

        public string EncryptedId { get; set; }

        public string Name { get; set; }

        public string Identifier { get; set; }

        public string IsActive { get; set; }

        public int TotalEngagement { get; set; }

        public int TotalUnregisteredEngagement { get; set; }

        public int TotalRegisteredEngagement { get; set; }

        public int UnregisteredCodesRequested { get; set; }

        public Boolean DoesZoneHaveEngagements { get; set; }

      

        public ZoneListViewModel()
        {
        }
    }

}
