﻿using MyApp.Web.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using MyApp.Business.DomainObjects.Models;
using System.Web.Mvc;

namespace MyApp.Web.ViewModels
{

    public class EngagementViewModel
    {

        public int Id { get; set; }
        public DateTime DateAdded { get; set; }

        public string Emailaddress { get; set; }

        public string UnregisteredCode { get; set; }


        public EngagementViewModel()
        {
        }
    }

}
