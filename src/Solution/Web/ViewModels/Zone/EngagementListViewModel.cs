﻿using MyApp.Web.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using MyApp.Business.DomainObjects.Models;
using System.Web.Mvc;

namespace MyApp.Web.ViewModels
{

    public class EngagementListViewModel : BaseViewModel
    {

        public List<EngagementViewModel> ListOfEngagements { get; set; }

        public string Zonename { get; set; }

        public EngagementListViewModel()
        {
        }
    }

}
