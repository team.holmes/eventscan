﻿using MyApp.Web.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using MyApp.Business.DomainObjects.Models;
using System.Web.Mvc;

namespace MyApp.Web.ViewModels
{

    public class ZoneAddEditViewModel : BaseViewModel
    {

        public string EncryptedId { get; set; }

        [Required(ErrorMessageResourceName = "requiredfield", ErrorMessageResourceType = typeof(EVAResource))]
        [Display(Name = "zonename", ResourceType = typeof(EVAResource))]
        [StringLength(100)]
        public string Name { get; set; }

        public string Identifier { get; set; }

        [Display(Name = "active", ResourceType = typeof(EVAResource))]
        public Boolean IsActive { get; set; }

      

        public ZoneAddEditViewModel()
        {
        }
    }

}
