﻿using Microsoft.Practices.Unity;
using OpenPlatform.General.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MyApp.Business.Services;
using WebMatrix.WebData;
using System.Web.Security;
using System.Collections;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Web.Helpers;
using Microsoft.Practices.ServiceLocation;


namespace MyApp.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private IUnityContainer _container;
        private ILog _log;
        private string _KeyUsersOnline = "OPUsersOnlineSEM2013";

        protected void Application_Start()
        {

            AntiForgeryConfig.SuppressIdentityHeuristicChecks = true;

            Application[_KeyUsersOnline] = 0;

            try
            {
                FormsAuthentication.SignOut();
            }
            catch
            {

            }

            if (!WebMatrix.WebData.WebSecurity.Initialized)
            {
                WebMatrix.WebData.WebSecurity.InitializeDatabaseConnection(
                    "DefaultConnection",
                    "UserProfile",
                    "Id",
                    "Username", false);
            }

            _container = UnityBuilder.Instance;

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            ControllerBuilder.Current.SetControllerFactory(new UnityControllerFactory(_container));
        }

        protected void Application_End()
        {

            Application[_KeyUsersOnline] = 0;
        }

        protected void Application_Error(object sender, EventArgs e)
        {


            Exception objErr = Server.GetLastError().GetBaseException();

            string appkey = "LastIdentifiedError";

            if (Application[appkey] == null || Application[appkey].ToString() != objErr.Message.ToString())
            {
                Application[appkey] = objErr.Message.ToString();

                string err = Request.Url.ToString() +
                        " Error Message: " + objErr.Message.ToString() +
                        " Stack Trace: " + objErr.StackTrace.ToString() +
                        " DateTime: " + DateTime.Now;

                //_log.Error(err);
            }


        }

        protected void Session_Start(Object sender, EventArgs E)
        {

            try
            {
                Application[_KeyUsersOnline] = Convert.ToInt32(Application[_KeyUsersOnline]) + 1;
            }
            catch (Exception err) { }

            if (Session.IsNewSession)
            {

            }

        }

        protected void Session_End(Object sender, EventArgs E)
        {

            try
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
            catch (Exception)
            {

            }

            try
            {
                HttpContext.Current.Session["SESSION_EVENTTEAM_OP2013"] = null;
            }
            catch (Exception)
            {

            }

            try
            {
                Application[_KeyUsersOnline] = Convert.ToInt32(Application[_KeyUsersOnline]) - 1;
            }
            catch (Exception err) { }
        }


        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            if (HttpContext.Current.Session != null)
            {
                _log.Info(String.Format("Application_BeginRequest HttpContext.Current.Session != null {0}", DateTime.Now.ToString()));
                FormsAuthentication.RedirectToLoginPage();
            }
        }





        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public void Application_AuthorizeRequest(Object sender, EventArgs e)
        {

        }




        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null && authCookie.Value != null && !String.IsNullOrEmpty(authCookie.Value))
            {
                FormsAuthenticationTicket objOrigTicket = FormsAuthentication.Decrypt(authCookie.Value);

                if (objOrigTicket.Expiration < DateTime.Now.AddMinutes(5))
                {
                    FormsAuthenticationTicket newticket = new FormsAuthenticationTicket(1,
                    objOrigTicket.Name,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(30),
                    false, objOrigTicket.UserData,
                    FormsAuthentication.FormsCookiePath);

                    string formsCookieStr = FormsAuthentication.Encrypt(newticket);

                    HttpCookie FormsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, formsCookieStr);

                    if (HttpContext.Current != null)
                    {
                        HttpContext.Current.Response.Cookies.Add(FormsCookie);
                    }
                }
            }
        }

    }
}
