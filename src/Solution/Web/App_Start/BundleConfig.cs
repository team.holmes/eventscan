﻿using System.Web;
using System.Web.Optimization;

namespace MyApp.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/bootstrapmin").Include(
            //            "~/Scripts/bootstrap.min.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //           "~/Scripts/jquery-1.10.2.min.js"));

            //bundles.Add(new ScriptBundle("~/bundles/globalize").Include(
            //           "~/Scripts/jquery.globalize/globalize.js",
            //           "~/Scripts/jquery.globalize/cultures/globalize*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate.min.js", "~/Scripts/jquery.validate.unobtrusive.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/shims").Include(
                    "~/Scripts/json3.min.js"));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/bootstrap.css",
                "~/Content/bootstrap-responsive.css",
                 "~/Content/app.css")
               );

        }
    }
}