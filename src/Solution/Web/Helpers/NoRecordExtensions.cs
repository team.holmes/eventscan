﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyApp.Web.Helpers
{
    public static class NoRecordExtensions
    {
        public static IHtmlString NoRecordsExist(this HtmlHelper helper)
        {
            string descriptor = @MyApp.Web.Resources.GeneralResource.norecordsexist;
            return helper.Raw(String.Format("<div class=\"validation-summary-errors\">{0}</div>", descriptor));
        }

    }
}