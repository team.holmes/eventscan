﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyApp.Web.Helpers
{
    public static class ButtonExtensions
    {
        public static IHtmlString SaveButton(this HtmlHelper helper)
        {
            return helper.Raw("<input type=\"submit\" id=\"SaveButton\" name=\"NavigationButton\" value=\"Save\" class=\"cancel\"/>");
        }


        public static IHtmlString SubmitButton(this HtmlHelper helper)
        {
            return helper.Raw(String.Format("<input type=\"submit\" class=\"btn\" id=\"SubmitButton\" value=\"{0}\" name=\"submitbutton\" />", @MyApp.Web.Resources.GeneralResource.submit));

        }
        /// <summary>
        /// General Submit button
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="displaytext">Sets teh text for the button - please ntoe if you are testing aginst form subitssion buttons then the nmae really does count and if you change it you will nedd ot update your controller.</param>
        /// <returns></returns>
        public static IHtmlString SubmitButton(this HtmlHelper helper, string displaytext = "")
        {
            // set the default text programatically
            if (String.IsNullOrEmpty(displaytext))
            {
                displaytext = @MyApp.Web.Resources.GeneralResource.submit;
            }

            // return helper.Raw("<input type=\"submit\" id=\"SubmitButton\" name=\"NavigationButton\" value=\"Submit\" />");
            return helper.Raw(String.Format("<input type=\"submit\" class=\"btn\" id=\"SubmitButton\" value=\"{0}\" name=\"submitbutton\" />", displaytext));

        }

        public static IHtmlString SubmitButton(this HtmlHelper helper, string displaytext = "", string Id = "")
        {

            if (String.IsNullOrEmpty(displaytext))
            {
                displaytext = @MyApp.Web.Resources.GeneralResource.submit;
            }

            if (String.IsNullOrEmpty(Id))
            {
                Id = "SubmitButton";
            }

            return helper.Raw(String.Format("<input type=\"submit\" class=\"btn\" id=\"{1}\" value=\"{0}\" name=\"submitbutton\" />", displaytext, Id));

        }

        public static IHtmlString SubmitButton(this HtmlHelper helper, string displaytext = "", string Id = "", string roles = "")
        {
            if (String.IsNullOrEmpty(displaytext))
            {
                displaytext = @MyApp.Web.Resources.GeneralResource.submit;
            }

            if (String.IsNullOrEmpty(Id))
            {
                Id = "SubmitButton";
            }

            if (String.IsNullOrEmpty(roles))
            {
                return helper.Raw(String.Format("<input type=\"submit\" class=\"btn\" id=\"{1}\" value=\"{0}\" name=\"submitbutton\" />", displaytext, Id));
            }
            else
            {
                var Session = HttpContext.Current.Session["SESSION_EVENTTEAM_OP2013"];
                string usersRoles = ((MyApp.Business.DomainObjects.Models.SessionModel)Session).RolesUserInAsPipeSeparated;
               
                foreach (string userRole in usersRoles.Split('|'))
                {
                    if (roles.ToUpper().Contains(userRole.ToUpper())){
                        return helper.Raw(String.Format("<input type=\"submit\" class=\"btn\" id=\"{1}\" value=\"{0}\" name=\"submitbutton\" />", displaytext, Id));
                    }

                }
                return helper.Raw(String.Format("<input type=\"submit\" class=\"btn\" id=\"{1}\" value=\"{0}\" name=\"submitbutton\" disabled=\"disabled\" />", displaytext, Id));
            }

        }


        public static IHtmlString BackButton(this HtmlHelper helper)
        {
            return helper.Raw("<input type=\"submit\" name=\"NavigationButton\" value=\"Back\" />");
        }

        public static IHtmlString GeneralButton(this HtmlHelper helper, string displaytext = "",  string roles = "", string onclick ="", string html="", string Class="" )
        {

            if (String.IsNullOrEmpty(displaytext))
            {
                displaytext = @MyApp.Web.Resources.GeneralResource.submit;
            }

            if (String.IsNullOrEmpty(roles))
            {
                return helper.Raw(String.Format("<button class=\"{0}\" type=\"button\" onclick=\"{2}\"  {3}  />{4}</button>",Class, onclick, html, displaytext));
            }
            else
            {
                var Session = HttpContext.Current.Session["SESSION_EVENTTEAM_OP2013"];
                string usersRoles = ((MyApp.Business.DomainObjects.Models.SessionModel)Session).RolesUserInAsPipeSeparated;

                foreach (string userRole in usersRoles.Split('|'))
                {
                    if (roles.ToUpper().Contains(userRole))
                    {
                        return helper.Raw(String.Format("<button class=\"{0}\" type=\"button\" onclick=\"{2}\"  {3}  />{4}</button>", Class, onclick, html, displaytext));
                    }

                }
                return helper.Raw(String.Format("<button class=\"{0}\" type=\"button\" onclick=\"{2}\"  {3}  disabled=\"disabled\" />{4}</button>", Class, onclick, html, displaytext));
            }

        }
    }
}