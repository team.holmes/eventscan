﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using MyApp.Business.DomainObjects.Models;

namespace MyApp.Web.Helpers
{
    //http://blogs.msdn.com/b/miah/archive/2008/11/10/checkboxlist-helper-for-mvc.aspx

    public static class CheckBoxExtensions
    {

        public static IHtmlString CheckBoxListExt(this HtmlHelper htmlHelper, string name, List<CBoxListInfo> listInfo)
        {
            return htmlHelper.CheckBoxListExt(name, listInfo,
                   ((IDictionary<string, object>)null));
        }

        public static IHtmlString CheckBoxListExt(this HtmlHelper htmlHelper, string name, List<CBoxListInfo> listInfo,
            object htmlAttributes)
        {
            return htmlHelper.CheckBoxListExt(name, listInfo,
               ((IDictionary<string, object>)new RouteValueDictionary(htmlAttributes)));
        }

        public static IHtmlString CheckBoxListExt(this HtmlHelper htmlHelper, string name, List<CBoxListInfo> listInfo,
           IDictionary<string, object> htmlAttributes)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentException("The argument must have a value", "name");
            if (listInfo == null)
                throw new ArgumentNullException("listInfo");
            if (listInfo.Count < 1)
                throw new ArgumentException("The list must contain at least one value", "listInfo");

            StringBuilder sb = new StringBuilder();

            foreach (CBoxListInfo info in listInfo)
            {
                TagBuilder builderinput = new TagBuilder("label");
                builderinput.AddCssClass("checkbox");

                TagBuilder builder = new TagBuilder("input");
                if (info.IsChecked) builder.MergeAttribute("checked", "checked");
                builder.MergeAttributes<string, object>(htmlAttributes);
                builder.MergeAttribute("type", "checkbox");
                builder.MergeAttribute("value", info.Value);
                builder.MergeAttribute("name", name);
                builder.InnerHtml = info.DisplayText;
                builderinput.InnerHtml = builder.ToString(TagRenderMode.Normal);

                sb.Append(builderinput.ToString(TagRenderMode.Normal));
            }

            return htmlHelper.Raw(sb.ToString());
        }
    }



}