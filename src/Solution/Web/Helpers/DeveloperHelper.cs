﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyApp.Web.Helpers
{
    public static class DeveloperHelper
    {
        public static IHtmlString BuildComment(this HtmlHelper helper, string message)
        {
            return helper.Raw(String.Format("<div class=\"alert alert-info\"><strong>Development Question</strong><br/>{0}</div>", message));
        }

    }
}