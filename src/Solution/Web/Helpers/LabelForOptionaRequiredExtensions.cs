﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace MyApp.Web.Helpers
{
    public static class LabelForOptionaRequiredExtensions
    {
        // public static IHtmlString LabelForOptionaRequired<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes)
        public static IHtmlString LabelForOptionaRequired<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {

            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            string labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();

            if (String.IsNullOrEmpty(labelText))
            {
                return MvcHtmlString.Empty;
            }

            TagBuilder tag = new TagBuilder("label");

            string required = "form-required";
            // add or insert attributes
            IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();

            Boolean isRequiredIf = false;
            Boolean isRequiredIfTrue = false;


            // TODO: This could be better done. try catch used for speed.
            var foolproofvalidation = (expression.Body as MemberExpression);
            if (foolproofvalidation != null)
            {
                try
                {
                    var requiredifattribute = foolproofvalidation.Member.GetCustomAttributes(typeof(Foolproof.RequiredIfAttribute), false).Cast<Foolproof.RequiredIfEmptyAttribute>().FirstOrDefault();
                    if (requiredifattribute != null) { isRequiredIf = true; }
                }
                catch (Exception err)
                {

                }

                try
                {
                    var requirediftrue = foolproofvalidation.Member.GetCustomAttributes(typeof(Foolproof.RequiredIfTrueAttribute), false).Cast<Foolproof.RequiredIfTrueAttribute>().FirstOrDefault();
                    if (requirediftrue != null) { isRequiredIfTrue = true; }
                }
                catch (Exception err)
                {

                }

            }



            // TODO - add required if requiredif or requirediftrue used
            if (htmlAttributes == null && (metadata.IsRequired == true || isRequiredIf == true || isRequiredIfTrue == true))
            {
                htmlAttributes = new Dictionary<string, object>();
                htmlAttributes.Add("class", required);
            }
            else if (htmlAttributes != null && (metadata.IsRequired == true || isRequiredIf == true || isRequiredIfTrue == true))
            {
                htmlAttributes.Add("class", required);
            }


            tag.MergeAttributes(htmlAttributes);
            tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));

            TagBuilder span = new TagBuilder("span");
           // tag.SetInnerText(labelText); // remarked this out to allow <br/>
            tag.InnerHtml = labelText;

            // assign <span> to <label> inner html
            //tag.InnerHtml = span.ToString(TagRenderMode.Normal);

           
            return MvcHtmlString.Create((tag.ToString(TagRenderMode.Normal)));
        }

    }
}