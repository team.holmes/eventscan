﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using OpenPlatform.General.Model;
using System.Web.Mvc.Html;

namespace MyApp.Web.Helpers
{
    //http://blogs.msdn.com/b/miah/archive/2008/11/10/checkboxlist-helper-for-mvc.aspx

    public static class BootstrapModalExtension
    {


        public static IHtmlString BootstrapModal(this HtmlHelper htmlHelper, string divid, string header, string body, string closecancelfunction, List<HtmlHelper> listofButtons)
        {
            //http://twitter.github.io/bootstrap/javascript.html#modals

            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format("<div id=\"{0}\" class=\"modal hide fade\">", divid));
            sb.Append("<div class=\"modal-header\">");
            //sb.Append("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>");
            sb.Append(String.Format("<h3>{0}</h3>", header));
            sb.Append("</div>");
            sb.Append("<div class=\"modal-body\">");
            sb.Append(String.Format("<p>{0}</p>", body));
            sb.Append("</div>");
            sb.Append(" <div class=\"modal-footer\">");
            if (!String.IsNullOrEmpty(closecancelfunction))
            {
                sb.Append(String.Format("<a href=\"#\" class=\"btn\" onclick=\"{0}(); return false;\">{1}</a>",closecancelfunction,"Close"));
            }
            foreach (HtmlHelper helper in listofButtons)
            {
                //sb.Append(helper.ToString());
                sb.Append("<a href=\"#\" class=\"btn\">Save changes</a>");
            }
            sb.Append("</div>");
            sb.Append("</div>");

            return htmlHelper.Raw(sb.ToString());
        }

        public static IHtmlString BootstrapModel(this HtmlHelper htmlHelper, string divId, string header, string closeFunction)
        {

            return htmlHelper.Raw("");
        }
    }



}