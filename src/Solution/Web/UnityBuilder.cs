﻿using Microsoft.Practices.Unity;
using MyApp.Business.DomainObjects.Models;
using MyApp.Business.Services;
using MyApp.DAL.Repository;
using OpenPlatform.General.Dal;
using System.Web;
using System.Web.SessionState;

namespace MyApp.Web
{
    public class UnityBuilder
    {
        private static IUnityContainer _instance;

        public static IUnityContainer Instance
        {
            get
            {
                if (_instance == null)
                    _instance = GetConfiguredContainer();

                return _instance;
            }
        }

        private static UnityContainer GetConfiguredContainer()
        {
            var container = new UnityContainer();


            container.RegisterType<IAppConfigurationService, AppConfigurationService>();

            container.RegisterType<IConfiguration, Configuration>();
            container.RegisterType<ILog, Log>();
            container.RegisterType<ISessionService, SessionService>();

            container.RegisterType<IAccountService, AccountService>();

            container.RegisterType<IEmailService, EmailService>();
            container.RegisterType<ISmtpService, SmtpService>();

            container.RegisterType<IUserService, UserService>();

            container.RegisterType<ICaptchaService, CaptchaService>();

            container.RegisterType<IZoneService, ZoneService>();

            container.RegisterType<IZoneEngagementService, ZoneEngagementService>();

            container.RegisterType<IUnregisteredService, UnregisteredService>();


            MyApp.Business.Services.UnityBuilder.ConfigureContainer(container);

          


            return container;
        }
    }
}

