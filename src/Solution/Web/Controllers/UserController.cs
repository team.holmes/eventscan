﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyApp.Business.DomainObjects.Models;
using MyApp.Business.Services;
using MyApp.Web.ViewModels;
using OpenPlatform.General.Serialisation;
using OpenPlatform.General.Extensions;
using System.Collections;
using System.Resources;
using AutoMapper;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Web.Security;
using Microsoft.Practices.Unity;
using System.IO;
using System.Text;

namespace MyApp.Web.Controllers
{

    [Authorize(Roles = "DEV,USER")]
    public class UserController : BaseController
    {
        private IAccountService _AccountService;
        private MyApp.Business.Services.IConfiguration _Configuration;
        private IEmailService _EmailService;
        private IQRCodeService _QRCodeService;


        public UserController(IAccountService accountService, MyApp.Business.Services.IConfiguration configuration, IEmailService emailservice, IQRCodeService qrcodeservice)
        {
            _AccountService = accountService;
            _Configuration = configuration;
            _EmailService = emailservice;
            _QRCodeService = qrcodeservice;


        }


        public ActionResult Index()
        {

            Log.Debug(string.Format("Usercontroller ActionResult Index() {0} ", ""));

            IndexViewModel viewmodel = new IndexViewModel();

            viewmodel.QrCodeScale = 5;

            viewmodel.Isencrypted = 1;

            viewmodel.QrCodeData = int.Parse(User.Identity.Name).Encrypt();

            return View(viewmodel);
        }

        [HttpGet]
        public ActionResult DownloadUserPDF(string encryptedId)
        {
            Log.Debug(string.Format("ZoneController ActionResult DownloadUserPDF() {0} ", encryptedId));

            UserProfile selecteduser = _AccountService.GetUserCache(GetLoggedInUserUserName());

            StringBuilder sb = new StringBuilder();

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = String.Format("QRCode_{0}.pdf", selecteduser.Id),
                Inline = false,
            };

            Response.Clear();
            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(_QRCodeService.GeneratePDFForUser(Server.MapPath("~/") + "Content\\Template\\UserPDFSheet.doc", selecteduser).ToArray(), "application/pdf");
        }




        #region helper methods


        #endregion



    }

}
