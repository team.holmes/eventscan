﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using MyApp.Business.DomainObjects.Models;
using MyApp.Business.Services;
using OpenPlatform.General.Extensions;

namespace MyApp.Web.Controllers
{
    public class BaseController : Controller
    {
        public ILog Log;
        public IUnityContainer Container;


        public BaseController()
        {
            Container = UnityBuilder.Instance;
            Log = Container.Resolve<ILog>();
        }


        public string GetBaseURL()
        {
            return string.Format(@"{0}://{1}", Request.Url.Scheme, Request.Url.Authority);
        }

        public string GetLoggedInUserUserName()
        {
            return User.Identity.Name;
        }

    }
}
