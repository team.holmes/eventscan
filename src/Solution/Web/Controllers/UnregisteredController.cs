﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MvcContrib;
using MyApp.Business.Services;
using MyApp.DAL.Repository;
using MyApp.Web.ViewModels;
using WebMatrix.WebData;
using OpenPlatform.General.Extensions;
using MyApp.Business.DomainObjects.Models;
using OpenPlatform.General.Encryption;
using System.Security;
using System.Text;
using System.Security.Cryptography;

namespace MyApp.Web.Controllers
{

    [Authorize(Roles = "DEV,SUPERUSER")]
    public class UnregisteredController : BaseController
    {
        private IAccountService _AccountService;
        private IConfiguration _Configuration;
        private ILog _Log;
        private IUnregisteredService _UnregisteredService;
        private IQRCodeService _QRCodeService;


        public UnregisteredController(IAccountService accountService, IConfiguration configuration, ILog log, IUnregisteredService unregisteredservice, IQRCodeService qrcodeservice)
        {
            _AccountService = accountService;
            _Configuration = configuration;
            _Log = Log;
            _UnregisteredService = unregisteredservice;
            _QRCodeService = qrcodeservice;
        }

        [HttpGet]
        public ActionResult Index()
        {
            Log.Debug(string.Format("UnregisteredController ActionResult Index() {0} ", ""));

            UnregisteredIndexViewModel viewmodel = new UnregisteredIndexViewModel();

            viewmodel.ListOfUnregistered = new List<UnregisteredListViewModel>();

            foreach (Unregistered item in _UnregisteredService.GetUnregisteredOrderedByName(_AccountService.GetUserCache(GetLoggedInUserUserName()).Id, true))
            {
                UnregisteredListViewModel model = new UnregisteredListViewModel()
                {
                    Name = item.Name,
                    EncryptedId = item.Id.Encrypt(),
                    UnregisteredCodesRequested = item.UnregisteredCodeIndex,
                    TotalCodesAvailable = item.MaxUnregisteredCodesAllowed,
                    DoesZoneHaveEngagements = (1 > 0 ? true : false),
                    CanDownloadPDF = (item.UnregisteredCodeIndex > 0 ? true : false),
                    CanAllocateMore = item.UnregisteredCodeIndex < item.MaxUnregisteredCodesAllowed,
                    CanDeleteUnregisteredGroup = !_UnregisteredService.DoesUnregisteredGroupHaveAnyEngagements(item.Id)
                };

                model.DoesZoneHaveEngagements = true;
                viewmodel.ListOfUnregistered.Add(model);
            }

            return View(viewmodel);

        }


        [HttpGet]
        public ActionResult AddEditUnregistered(string encid)
        {
            Log.Debug(string.Format("UnregisteredController ActionResult AddEditUnregistered() {0} ", encid));

            Unregistered selectedunregisteredgroup = new Unregistered() { IsActive = true };

            if (encid.IsNotNullOrEmpty())
            {
                selectedunregisteredgroup = _UnregisteredService.GetUnregisteredById(encid.DecryptToInt());
            }

            UnregisteredAddEditViewModel viewmodel = new UnregisteredAddEditViewModel();
            viewmodel.EncryptedId = selectedunregisteredgroup.Id.Encrypt();
            viewmodel.IsActive = selectedunregisteredgroup.IsActive;
            viewmodel.Name = selectedunregisteredgroup.Name;

            return View(viewmodel);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEditUnregistered(UnregisteredAddEditViewModel viewmodel)
        {
            Log.Debug(string.Format("UnregisteredController ActionResult AddEditUnregistered() {0} ", ""));

            BusinessEnum.UnregisteredAdd addresult = BusinessEnum.UnregisteredAdd.Fail;

            if (ModelState.IsValid)
            {

                // edit
                if (viewmodel.EncryptedId.DecryptToInt() > 0)
                {
                    Unregistered editedgroup = _UnregisteredService.GetUnregisteredById(viewmodel.EncryptedId.DecryptToInt());
                    editedgroup.IsActive = viewmodel.IsActive;
                    editedgroup.Name = viewmodel.Name;

                    addresult = _UnregisteredService.Update(editedgroup);
                }

                // add
                if (viewmodel.EncryptedId.DecryptToInt() == 0)
                {
                    Unregistered addedunregistered = new Unregistered();
                    addedunregistered.IsActive = viewmodel.IsActive;
                    addedunregistered.Name = viewmodel.Name.TrimCheckForNull();
                    addedunregistered.UserProfileId = _AccountService.GetUserCache(GetLoggedInUserUserName()).Id;

                    addresult = _UnregisteredService.Add(addedunregistered);

                }

                if (addresult == BusinessEnum.UnregisteredAdd.DuplicateName)
                {
                    viewmodel.Feedbackmessage = MyApp.Web.Resources.EVAResource.groupalreadyexists;
                }
                else if (addresult == BusinessEnum.UnregisteredAdd.Fail)
                {
                    viewmodel.Feedbackmessage = MyApp.Web.Resources.EVAResource.failedtocompleterequest;
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }

            return View(viewmodel);
        }




        [HttpGet]
        public ActionResult DownloadUnregisteredGroupPDF(string encid)
        {
            Log.Debug(string.Format("UnregisteredController ActionResult DownloadUnregisteredGroupPDF() {0} ", encid));

            Unregistered selectedgroup = _UnregisteredService.GetUnregisteredById(encid.DecryptToInt());

            StringBuilder sb = new StringBuilder();

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = String.Format("Unreg_{0}.zip", selectedgroup.Id),
                Inline = false,
            };

            Response.Clear();
            Response.AppendHeader("Content-Disposition", cd.ToString());



            return File(_QRCodeService.GenerateMultiPartPDFZipFOrGroupUnregistered(Server.MapPath("~/") + "Content\\Template\\Unregistered.doc", selectedgroup, GetBaseURL()).ToArray(), "application/zip");
           // return File(_QRCodeService.GeneratePDFForGroupUnregistered(Server.MapPath("~/") + "Content\\Template\\Unregistered.doc", selectedgroup, GetBaseURL()).ToArray(), "application/pdf");


        }




        [HttpGet]
        public ActionResult DeleteUnregistered(string encid)
        {
            Log.Debug(string.Format("UnregisteredController ActionResult DeleteUnregistered() {0} ", encid));

            _UnregisteredService.DeleteByUnregisteredId(encid.DecryptToInt());

            return RedirectToAction("Index");
        }




        [HttpGet]
        public ActionResult GenerateUnregisteredGroupCodes(string encid)
        {
            Log.Debug(string.Format("UnregisteredController ActionResult GenerateUnregisteredGroupCodes() {0} ", encid));

            Unregistered selectedgroup = _UnregisteredService.GetUnregisteredById(encid.DecryptToInt());

            UnregisteredGroupCodesViewModel model = new UnregisteredGroupCodesViewModel();

            model.TotalRequestedUnregisteredCodes = selectedgroup.UnregisteredCodeIndex;

            model.MaxRequestedUnregisteredCodes = selectedgroup.MaxUnregisteredCodesAllowed;

            model.CanAddMoreUnregistered = _UnregisteredService.IsUnregisteredAmountRequestBelowMaxAllowed(0, selectedgroup);

            model.EncryptedId = encid.DecryptToInt().Encrypt();

            return View(model);

        }





        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GenerateUnregisteredGroupCodes(UnregisteredGroupCodesViewModel viewmodel)
        {
            Log.Debug(string.Format("UnregisteredController ActionResult GenerateUnregisteredGroupCodes() {0} ", ""));

            if (ModelState.IsValid)
            {
                Unregistered selectedgroup = _UnregisteredService.GetUnregisteredById(viewmodel.EncryptedId.DecryptToInt());

                BusinessEnum.UnregisteredRequestIndex result = _UnregisteredService.UpdateUnregisteredRequestIndex(viewmodel.Newlyrequested.Value, selectedgroup);

                if (result == BusinessEnum.UnregisteredRequestIndex.TooMuchRequested)
                {
                    ModelState.AddModelError("Newlyrequested", String.Format(Resources.EVAResource.toomuchrequested, _Configuration.MaxUnregisteredZonesAllowedPerRequest));
                }
                else if (result == BusinessEnum.UnregisteredRequestIndex.ExceedMax)
                {
                    ModelState.AddModelError("Newlyrequested", Resources.EVAResource.exceeededmaxunregisteredrequested);
                }
                else if (result == BusinessEnum.UnregisteredRequestIndex.Success)
                {
                    return RedirectToAction("GenerateUnregisteredGroupCodes", new { encid = viewmodel.EncryptedId });
                }
                else
                {
                    viewmodel.Feedbackmessage = MyApp.Web.Resources.EVAResource.unabletoprocessrequest;
                }
            }

            return View(viewmodel);

        }


        //[HttpGet]
        //public ActionResult ShowEngagementsByZone(string zoneid)
        //{
        //    Log.Debug(string.Format("UnregisteredController ActionResult ShowEngagementsByZone() {0} ", zoneid));

        //    EngagementListViewModel viewmodel = new EngagementListViewModel();

        //    viewmodel.ListOfEngagements = new List<EngagementViewModel>();

        //    List<ZoneEngagement> listofengagements = _ZoneService.GetEngagementsByZone(zoneid.DecryptToInt());

        //    foreach (ZoneEngagement engagement in listofengagements)
        //    {
        //        EngagementViewModel model = new EngagementViewModel()
        //        {
        //            DateAdded = engagement.DateAdded,
        //            Emailaddress = engagement.UserProfile.EmailAddress
        //        };

        //        viewmodel.ListOfEngagements.Add(model);

        //    }


        //    return View(viewmodel);

        //}



        #region Helpers



        #endregion
    }
}
