﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyApp.Business.Services;
using MyApp.Web.ViewModels;
using MyApp.Business.DomainObjects.Models;
using MyApp.Web.Resources;
using OpenPlatform.General.Encryption;
using System.Text;
using System.Configuration;
using System.Collections;
using OpenPlatform.General.Extensions;
using System.Resources;
using Aspose.Pdf.Generator;
using Aspose.Pdf;
using System.IO;


namespace MyApp.Web.Controllers
{
    // [Authorize(Roles = "DEV")]
    public class DevController : BaseController
    {

        private IAccountService _AccountService;
        private IConfiguration _Configuration;
        private IEmailService _EmailService;
        private ISmtpService _SmtpService;
        private IZoneEngagementService _ZoneEngagementService;


        public DevController(IAccountService accountService, IConfiguration configuration, IEmailService emailservice, ISmtpService smtpservice, IZoneEngagementService zoneengservice)
        {
            _AccountService = accountService;

            _Configuration = configuration;

            _EmailService = emailservice;

            _SmtpService = smtpservice;

            _ZoneEngagementService = zoneengservice;
        }


        [HttpGet]
        public ActionResult WriteOutApplicationLog()
        {

            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format("{0}Writing out application method trace {0}{0}", "</br>"));

            string applogkey = "BACKLOGKEYFORAPP";

            if (HttpContext.Application[applogkey] != null)
            {
                foreach (string str in (List<string>)HttpContext.Application[applogkey])
                {
                    sb.Append(String.Format("{0}{1}", str, "</br>"));
                }
            }

            return Content(sb.ToString());
        }



    }
}
