﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MvcContrib;
using MyApp.Business.Services;
using MyApp.DAL.Repository;
using MyApp.Web.ViewModels;
using WebMatrix.WebData;
using OpenPlatform.General.Extensions;
using MyApp.Business.DomainObjects.Models;
using OpenPlatform.General.Encryption;
using System.Security;
using System.Text;
using System.Security.Cryptography;

namespace MyApp.Web.Controllers
{

    [Authorize(Roles = "DEV,SUPERUSER")]
    public class ZoneController : BaseController
    {
        private IAccountService _AccountService;
        private IConfiguration _Configuration;
        private ILog _Log;
        private IZoneService _ZoneService;
        private IQRCodeService _QRCodeService;


        public ZoneController(IAccountService accountService, IConfiguration configuration, ILog log, IZoneService zoneservice, IQRCodeService qrcodeservice)
        {
            _AccountService = accountService;
            _Configuration = configuration;
            _Log = Log;
            _ZoneService = zoneservice;
            _QRCodeService = qrcodeservice;
        }

        [HttpGet]
        public ActionResult Index()
        {
            Log.Debug(string.Format("ZoneController ActionResult Index() {0} ", ""));

            ZoneIndexViewModel viewmodel = new ZoneIndexViewModel();

            viewmodel.ListOfZones = new List<ZoneListViewModel>();

            foreach (Zone currentzone in _ZoneService.GetZonesOrderedByName(_AccountService.GetUserCache(GetLoggedInUserUserName()).Id, true))
            {
                ZoneListViewModel model = new ZoneListViewModel()
                {
                    Identifier = currentzone.Identifier,
                    IsActive = currentzone.IsActive ? "Y" : "N",
                    Name = currentzone.Name,
                    EncryptedId = currentzone.Id.Encrypt(),
                    TotalEngagement = _ZoneService.GetTotalEngagementsByZoneId(currentzone.Id),
                    TotalRegisteredEngagement = _ZoneService.GetTotalRegisteredEngagementsByZoneId(currentzone.Id),
                    TotalUnregisteredEngagement = _ZoneService.GetTotalUnregisteredEngagementsByZoneId(currentzone.Id),
                    DoesZoneHaveEngagements = false
                };

                if (model.TotalEngagement > 0)
                {
                    model.DoesZoneHaveEngagements = true;
                }


                viewmodel.ListOfZones.Add(model);
            }

            return View(viewmodel);

        }


        [HttpGet]
        public ActionResult AddEditZone(string zoneid)
        {
            Log.Debug(string.Format("ZoneController ActionResult AddEditZone() {0} ", zoneid));

            Zone selectezone = new Zone() { IsActive = true };

            if (zoneid.IsNotNullOrEmpty())
            {
                selectezone = _ZoneService.GetZoneById(zoneid.DecryptToInt());
            }

            ZoneAddEditViewModel viewmodel = new ZoneAddEditViewModel();
            viewmodel.EncryptedId = selectezone.Id.Encrypt();
            viewmodel.Identifier = selectezone.Identifier;
            viewmodel.IsActive = selectezone.IsActive;
            viewmodel.Name = selectezone.Name;

            return View(viewmodel);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEditZone(ZoneAddEditViewModel viewmodel)
        {
            Log.Debug(string.Format("ZoneController ActionResult AddEditZone() {0} ", ""));

            BusinessEnum.ZoneAdd addresult = BusinessEnum.ZoneAdd.Fail;

            if (ModelState.IsValid)
            {

                // edit
                if (viewmodel.EncryptedId.DecryptToInt() > 0)
                {
                    Zone editedzone = _ZoneService.GetZoneById(viewmodel.EncryptedId.DecryptToInt());
                    editedzone.IsActive = viewmodel.IsActive;
                    editedzone.Name = viewmodel.Name;

                    addresult = _ZoneService.Update(editedzone);
                }

                // add
                if (viewmodel.EncryptedId.DecryptToInt() == 0)
                {
                    Zone addedzone = new Zone();
                    addedzone.IsActive = true;
                    addedzone.Name = viewmodel.Name.TrimCheckForNull();
                    addedzone.Identifier = "--";
                    addedzone.UserProfileId = _AccountService.GetUserCache(GetLoggedInUserUserName()).Id;

                    addresult = _ZoneService.Add(addedzone);

                }

                if (addresult == BusinessEnum.ZoneAdd.DuplicateName)
                {
                    viewmodel.Feedbackmessage = MyApp.Web.Resources.EVAResource.zonenamealreadyexists;
                }
                else if (addresult == BusinessEnum.ZoneAdd.Fail)
                {
                    viewmodel.Feedbackmessage = MyApp.Web.Resources.EVAResource.failedtocompleterequest;
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }

            return View(viewmodel);
        }



        [HttpGet]
        public ActionResult DownloadZonePDF(string zoneid)
        {
            Log.Debug(string.Format("ZoneController ActionResult DownloadZonePDF() {0} ", zoneid));

            Zone selectedzone = _ZoneService.GetZoneById(zoneid.DecryptToInt());

            StringBuilder sb = new StringBuilder();

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = String.Format("Zone_{0}.pdf", selectedzone.Identifier),
                Inline = false,
            };

            Response.Clear();
            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(_QRCodeService.GeneratePDFForZone(Server.MapPath("~/") + "Content\\Template\\ZonePDFSheet.doc", selectedzone).ToArray(), "application/pdf");
        }


        [HttpGet]
        public ActionResult DeleteZone(string zoneid)
        {
            Log.Debug(string.Format("ZoneController ActionResult DeleteZone() {0} ", zoneid));

            _ZoneService.DeleteByZoneId(zoneid.DecryptToInt());

            return RedirectToAction("Index");
        }



        [HttpGet]
        public ActionResult ShowEngagementsByZone(string zoneid)
        {
            Log.Debug(string.Format("ZoneController ActionResult ShowEngagementsByZone() {0} ", zoneid));

            EngagementListViewModel viewmodel = new EngagementListViewModel();

            viewmodel.ListOfEngagements = new List<EngagementViewModel>();

            List<ZoneEngagement> listofengagements = _ZoneService.GetEngagementsByZone(zoneid.DecryptToInt());

            foreach (ZoneEngagement engagement in listofengagements)
            {
                EngagementViewModel model = new EngagementViewModel()
                {
                    Id = engagement.Id,
                    DateAdded = engagement.DateAdded,
                    Emailaddress = (engagement.UserProfileId.HasValue ? engagement.UserProfile.EmailAddress : MyApp.Web.Resources.EVAResource.unregistered),
                    UnregisteredCode = (engagement.UnregisteredId)
                };

                viewmodel.ListOfEngagements.Add(model);

            }


            return View(viewmodel);

        }



        #region Helpers



        #endregion
    }
}
