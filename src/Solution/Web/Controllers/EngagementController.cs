﻿using Microsoft.Practices.Unity;
using MyApp.Business.DomainObjects.Models;
using MyApp.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OpenPlatform.General.Extensions;

namespace MyApp.Web.Controllers
{
    public class EngagementController : ApiController
    {

        private IAccountService _AccountService;
        private IConfiguration _Configuration;
        private IEmailService _EmailService;
        public ILog Log;
        public IUnityContainer Container;
        public IZoneEngagementService _ZoneEngagementService;


        public EngagementController()
        {

            _AccountService = (IAccountService)UnityBuilder.Instance.Resolve(typeof(IAccountService), null);
            _Configuration = (IConfiguration)UnityBuilder.Instance.Resolve(typeof(IConfiguration), null);
            _EmailService = (IEmailService)UnityBuilder.Instance.Resolve(typeof(IEmailService), null);
            _ZoneEngagementService = (IZoneEngagementService)UnityBuilder.Instance.Resolve(typeof(IZoneEngagementService), null);

            Container = UnityBuilder.Instance;
            Log = Container.Resolve<ILog>();

        }

        [HttpGet, HttpPost]
        public List<string> Help()
        {
            List<String> methods = new List<string>();
            methods.Add("List<Zone> GetAllActiveZones()");
            methods.Add("List<ZoneEngagement> GetAllZoneEngagements()");
            methods.Add("ResponsePacket SaveZoneEngagement(string qrcode, string zone)");
            methods.Add("string Encrypt(int id)");

            return methods;
        }

        [HttpGet, HttpPost]
        public string Encrypt(int id)
        {
            Log.Debug(string.Format("EngagementController Encrypt {0}", id));
            return id.Encrypt();
        }

        [HttpGet, HttpPost]
        public List<Zone> GetAllActiveZones()
        {
            Log.Debug(string.Format("EngagementController GetAllActiveZones {0}", ""));
            return _ZoneEngagementService.GetAllZones(false);
        }

        [HttpGet, HttpPost]
        public List<ZoneEngagement> GetAllZoneEngagements()
        {
            Log.Debug(string.Format("EngagementController GetAllZoneEngagements {0}", ""));
            return _ZoneEngagementService.GetAllZoneEngagements();
        }

        /*
         * [Id] [int] IDENTITY(1,1) NOT NULL,
	[EngagementId] [nvarchar](255) NULL,
	[Key] [nvarchar](128) NOT NULL,
	[Value] [nvarchar](1024) NULL,
	[DateAdded] [datetime] NOT NULL,
         * */

        [HttpGet, HttpPost]
        public ResponsePacket SaveKeyValuePair(string engagementId, string key, string value, string dateadded)
        {
            Log.Debug(string.Format("EngagementController SaveKeyValuePair {0} | {1} | {2} | {3}", engagementId, key, value, dateadded));

            BusinessEnum.ResponseCodes result = _ZoneEngagementService.AddKeyValuePair(engagementId, key, value, dateadded);

            ResponsePacket response = new ResponsePacket();
            response.ResultCode = (int)result;

            // not correct paramaters
            if (result == BusinessEnum.ResponseCodes.Result_InvalidParamaters)
            {
                response.ResultMessage = "Missing required paramater(s)";
                return response;
            }



            response.ResultMessage = "Zone engagement failed to be added";
            return response;

        }

        //http://localhost:53362/api/Bust/SaveZoneEngagement?qrcode=AS&zone=BAS
        [HttpGet, HttpPost]
        public ResponsePacket SaveZoneEngagement(string qrcode, string zone, string dateadded)
        {
            Log.Debug(string.Format("EngagementController SaveZoneEngagement {0} | {1} | {2}", qrcode, zone, dateadded));

            BusinessEnum.ResponseCodes result = _ZoneEngagementService.AddZoneEngagement(qrcode, zone, dateadded);

            ResponsePacket response = new ResponsePacket();
            response.ResultCode = (int)result;

            // not correct paramaters
            if (result == BusinessEnum.ResponseCodes.Result_InvalidParamaters)
            {
                response.ResultMessage = "Missing required paramater(s)";
                return response;
            }

            // paramaters dont decrypt
            if (result == BusinessEnum.ResponseCodes.Result_DecryptionError)
            {
                response.ResultMessage = "Cannot decrypt one or all of the parameters";
                return response;
            }

            // decryopted zone doesnt start with Z_
            if (result == BusinessEnum.ResponseCodes.Result_ZonePrefixIsntCorrect)
            {
                response.ResultMessage = " Decrypted qrcode/zone is not in correct format";
                return response;
            }


            if (result == BusinessEnum.ResponseCodes.Result_ZoneDoesntExist)
            {
                response.ResultMessage = "Zone does not exist";
                return response;
            }

            if (result == BusinessEnum.ResponseCodes.Result_UserProfilesDoesntExist)
            {
                response.ResultMessage = "User profile does not exist";
                return response;
            }

            if (result == BusinessEnum.ResponseCodes.Result_Duplication)
            {
                response.ResultMessage = "Zone engagement already exists";
                return response;
            }

            if (result == BusinessEnum.ResponseCodes.Result_Success)
            {
                response.ResultMessage = "Zone engagement added";
                return response;
            }

            response.ResultMessage = "Zone engagement failed to be added";
            return response;

        }




        #region helper methods


        #endregion



    }
}