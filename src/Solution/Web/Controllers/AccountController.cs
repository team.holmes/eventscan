﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MvcContrib;
using MyApp.Business.Services;
using MyApp.DAL.Repository;
using MyApp.Web.ViewModels;
using WebMatrix.WebData;
using OpenPlatform.General.Extensions;
using MyApp.Business.DomainObjects.Models;
using OpenPlatform.General.Encryption;
using System.Security;
using System.Text;
using System.Security.Cryptography;

namespace MyApp.Web.Controllers
{

    public class AccountController : BaseController
    {
        private IAccountService _AccountService;
        private IConfiguration _Configuration;
        private ICaptchaService _CaptchaService;
        private IEmailService _EmailService;
        private IQRCodeService _QRCodeService;
        private ILog _Log;


        public AccountController(IAccountService accountService, IConfiguration configuration, ICaptchaService captchaservice, IEmailService emailservice, ILog log, IQRCodeService qrcodeservice)
        {
            _AccountService = accountService;
            _Configuration = configuration;
            _CaptchaService = captchaservice;
            _EmailService = emailservice;
            _Log = Log;
            _QRCodeService = qrcodeservice;
        }

        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult KeepAlive(long? ticks, long? randomiser)
        {
            Log.Debug(string.Format("AccountController ActionResult KeepAlive() {0} ", ""));

            return Content(DateTime.Now.ToShortTimeString());
        }


        [AllowAnonymous]
        public FileContentResult GetCaptchaImage(string enid, string cachecode, string rnd)
        {
            return new FileContentResult(_CaptchaService.GenerateCaptchaImageAsJpeg(enid, 140, 30), "image/jpeg");  // was 185 50 
        }


        [AllowAnonymous]
        //[ClearAllTokensFilter]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Login(int? redirect, string rid)
        {

            Log.Debug(string.Format("AccountController ActionResult Login() {0} ", ""));
            try
            {
                FormsAuthentication.SignOut();

                Response.Cookies.Clear();

                Session.Clear();
                Session.Abandon();
            }
            catch (Exception err) { }

            try
            {
                Response.Cookies.Clear();
                if (Session[_Configuration.SessionKeyForUserProfile] != null)
                {
                    return this.RedirectToAction("LogOff");
                }
            }
            catch (Exception err) { }

            _AccountService.LogoutUser();


            /// section that redirects to clear the cookie to help resolve the antiforgerytoken issues
            if (!redirect.HasValue)
            {
                return RedirectToAction("Login", new { redirect = 1, rid = DateTime.Now.Ticks });
            }


            string captchacodeused = _CaptchaService.GenerateRandomCode();

            LoginViewModel viewmodel = new LoginViewModel();
            viewmodel.EncryptedCaptcha = captchacodeused.Encrypt();


            //  _AccountService.EncryptPassphraseDB();

            // _AccountService.GenerateNewPassphraseDB();


            return View(viewmodel);
        }


        [HttpGet]
        public ActionResult Templogin()
        {
            _AccountService.Login("d@openplatform.co.uk","d@openplatform.co.uk");
            return  RedirectToAction("Index","SuperUser");//   http://localhost:53362/SuperUser/Index
        }

        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel viewmodel, string returnUrl)
        {
            Log.Debug(string.Format("AccountController ActionResult Login() {0} ", returnUrl));

            BusinessEnum.AuthenticationResult AuthenticationResult;

            if (ModelState.IsValid)
            {
                if (viewmodel.Captcha.TrimCheckForNull() != viewmodel.EncryptedCaptcha.Decrypt())
                {
                    ModelState.AddModelError("Captcha", Resources.EVAResource.invalidacaptchacode);
                }
                else
                {

                    AuthenticationResult = _AccountService.Login(viewmodel.EmailAddress, viewmodel.Passphrase);

                    if (AuthenticationResult == BusinessEnum.AuthenticationResult.Passed)
                    {
                        // Bounce to teh relevant home page
                        return RedirectToAction("DetermineHomePage");
                    }
                    else if (AuthenticationResult == BusinessEnum.AuthenticationResult.AccountNotActive)
                    {
                        ModelState.AddModelError("EmailAddress", String.Format(Resources.EVAResource.accounthasbeendisabled, _Configuration.AccountLockoutPeriodInMins));
                    }
                    else if (AuthenticationResult == BusinessEnum.AuthenticationResult.Locked)
                    {
                        ModelState.AddModelError("EmailAddress", String.Format(Resources.EVAResource.accounthasbeenlocked, _Configuration.AccountLockoutPeriodInMins));
                    }
                    else if (AuthenticationResult == BusinessEnum.AuthenticationResult.AccountNotConfirmed_Validated)
                    {
                        ModelState.AddModelError("EmailAddress", Resources.EVAResource.Accoutnotvalidated);
                    }
                    else if (AuthenticationResult == BusinessEnum.AuthenticationResult.PassphraseExpired || AuthenticationResult == BusinessEnum.AuthenticationResult.TempPasswordIssued)
                    {
                        UserProfile currentuser = _AccountService.GetUser(viewmodel.EmailAddress);

                        UpdatePassPhraseAccountViewModel vmodelpass = new UpdatePassPhraseAccountViewModel();
                        vmodelpass.P1 = currentuser.EmailAddress.Encrypt();
                        vmodelpass.P2 = currentuser.Id.Encrypt();
                        vmodelpass.ReasonEnum = AuthenticationResult;
                        vmodelpass.ShowPassPhraseUpdateSuccess = false;
                        vmodelpass.HasComeFromHomePage = true;

                        string captchacodeused = _CaptchaService.GenerateRandomCode();
                        vmodelpass.EncryptedCaptcha = captchacodeused.Encrypt();


                        try
                        {
                            ModelState["Captcha"].Value = new ValueProviderResult("", "", System.Globalization.CultureInfo.CurrentUICulture);
                            ModelState["EncryptedCaptcha"].Value = new ValueProviderResult(captchacodeused.Encrypt(), captchacodeused.Encrypt(), System.Globalization.CultureInfo.CurrentUICulture);
                        }
                        catch (Exception err) { }


                        return View("UpdatePassphrase", vmodelpass);
                    }
                    else
                    {
                        ModelState.AddModelError("EmailAddress", Resources.EVAResource.logindetailsincorrect);
                    }
                }
            }

            string captchacodeused1 = _CaptchaService.GenerateRandomCode();
            viewmodel.EncryptedCaptcha = captchacodeused1.Encrypt();

            try
            {
                ModelState["Captcha"].Value = new ValueProviderResult("", "", System.Globalization.CultureInfo.CurrentUICulture);
                ModelState["EncryptedCaptcha"].Value = new ValueProviderResult(captchacodeused1.Encrypt(), captchacodeused1.Encrypt(), System.Globalization.CultureInfo.CurrentUICulture);
            }
            catch (Exception err)
            { }
            return View(viewmodel);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult CreateAccount(CreateAccountViewModel viewmodel)
        {
            Log.Debug(string.Format("AccountController ActionResult CreateAccount {0} ", ""));

            if (viewmodel.Captcha.TrimCheckForNull() != viewmodel.EncryptedCaptch.Decrypt())
            {
                ModelState.AddModelError("Captcha", Resources.EVAResource.invalidacaptchacode);
            }


            //if (!string.IsNullOrEmpty(viewmodel.UniqueCode))
            //{
            //    try
            //    {
            //        string decrypted = viewmodel.UniqueCode.Decrypt();
            //    }
            //    catch
            //    {
            //        // big errors
            //        ModelState.AddModelError("Captcha", Resources.EVAResource.invalidacaptchacode);
            //    }
            //}

            if (ModelState.IsValid)
            {

                BusinessEnum.UnconfirmedTeamCreateAccountIssues accountcreationresult;

                //if (!string.IsNullOrEmpty(viewmodel.UniqueCode))
                //{

                //    accountcreationresult = _AccountService.CreateNewAccount(viewmodel.EmailAddress, viewmodel.Passphrasepart1, _Configuration.RoleUserID, true);
                //}

                accountcreationresult = _AccountService.CreateNewAccount(viewmodel.EmailAddress, viewmodel.Passphrasepart1, _Configuration.RoleSuperUserID, true);

                if (accountcreationresult == BusinessEnum.UnconfirmedTeamCreateAccountIssues.EmailAlreadyExists)
                {
                    ModelState.AddModelError("EmailAddress", Resources.EVAResource.emailalreadyexists);
                }
                else if (accountcreationresult == BusinessEnum.UnconfirmedTeamCreateAccountIssues.InvalidPassphrase)
                {
                    ModelState.AddModelError("Passphrasepart1", Resources.EVAResource.invalidpassphrasecreation);
                }
                else
                {
                    _AccountService.Login(viewmodel.EmailAddress, viewmodel.Passphrasepart1);

                    _EmailService.SendAccountCreationConfirmation(_AccountService.GetUserByEmailAddress(viewmodel.EmailAddress).Id, viewmodel.EmailAddress, GetBaseURL());

                    return RedirectToAction("DetermineHomePage");
                }

            }

            string captchacodeused = _CaptchaService.GenerateRandomCode();
            viewmodel.EncryptedCaptch = captchacodeused.Encrypt();
            viewmodel.Captcha = string.Empty;

            try
            {
                ModelState["EncryptedCaptch"].Value = new ValueProviderResult(captchacodeused.Encrypt(), captchacodeused.Encrypt(), System.Globalization.CultureInfo.CurrentUICulture);
                ModelState["Captcha"].Value = new ValueProviderResult("", "", System.Globalization.CultureInfo.CurrentUICulture);
            }
            catch (Exception err)
            { }

            return View("CreateAccount", viewmodel);
        }


        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult CreateAccount()
        {
            Log.Debug(string.Format("AccountController ActionResult CreateAccount {0} ", ""));

            CreateAccountViewModel viewmodel = new CreateAccountViewModel();

            string captchacodeused = _CaptchaService.GenerateRandomCode();

            viewmodel.EncryptedCaptch = captchacodeused.Encrypt();

            return View(viewmodel);

        }

        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public FileContentResult GetQrCode(string data, string ticks, string scale, int isencrypted)
        {
            Log.Debug(string.Format("AccountController FileContentResult GetQrCode {0} ", isencrypted));

            return new FileContentResult(_QRCodeService.GenerateQRCode((isencrypted == 1 ? data.Decrypt().Encrypt() : data), scale.DecryptToInt()), "image/jpeg");

        }



        // TODO : Implementing menus correctly http://techbrij.com/role-based-menu-asp-net-mvc
        public ActionResult LogOff()
        {
            Log.Debug(string.Format("AccountController ActionResult LogOff() {0} ", ""));

            FormsAuthentication.SignOut();

            try
            {
                Session[_Configuration.SessionKeyForUserProfile] = null;
            }
            catch (Exception)
            {

            }

            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult ForgottenPassPhrase()
        {
            Log.Debug(string.Format("AccountController ActionResult ForgottenPassPhrase() {0} ", ""));

            ForgottenPassPhraseAccountViewModel viewmodel = new ForgottenPassPhraseAccountViewModel();

            string captchacodeused = _CaptchaService.GenerateRandomCode();

            viewmodel.CaptchaEncrypted = captchacodeused;

            return View(viewmodel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgottenPassPhrase(ForgottenPassPhraseAccountViewModel viewmodel, string submitbutton)
        {
            Log.Debug(string.Format("AccountController ActionResult ForgottenPassPhrase() {0} ", submitbutton));


            if (viewmodel.Captcha.TrimCheckForNull() != viewmodel.CaptchaEncrypted.Decrypt())
            {
                ModelState.AddModelError("Captcha", Resources.EVAResource.invalidacaptchacode);
            }

            if (ModelState.IsValid)
            {
                if (_AccountService.DoesAccountExistByEmailAddress(viewmodel.EmailAddress))
                {
                    // user wants to get their passphrase emailed
                    if (submitbutton == @MyApp.Web.Resources.EVAResource.sendmemypassphrase)
                    {
                        BusinessEnum.PassphraseReset result = _AccountService.EmailTemporaryPassPhrase(viewmodel.EmailAddress, GetBaseURL());

                        if (result == BusinessEnum.PassphraseReset.Success)
                        {
                            viewmodel.IsResetSuccessful = true;
                        }
                        else if (result == BusinessEnum.PassphraseReset.NoSuchUser)
                        {
                            ModelState.AddModelError("EmailAddress", Resources.EVAResource.nosuchuser);
                        }
                        else if (result == BusinessEnum.PassphraseReset.Accountnotvalidated)
                        {
                            ModelState.AddModelError("EmailAddress", Resources.EVAResource.accountnotvalidated);
                        }
                        else
                        {
                            ModelState.AddModelError("EmailAddress", Resources.EVAResource.unabletoreset);
                        }
                    }
                    else if (submitbutton == @MyApp.Web.Resources.EVAResource.resendaccountconfirmation)
                    {
                        // determine the account
                        BusinessEnum.ResendAccountSignupEmails result = _AccountService.SendAccountConfirmationEmailBasedOnEmailAddress(viewmodel.EmailAddress, GetBaseURL());

                        if (result == BusinessEnum.ResendAccountSignupEmails.Accountalreadyvalidated)
                        {
                            ModelState.AddModelError("EmailAddress", Resources.EVAResource.accountalreadyactivated);
                        }
                        else if (result == BusinessEnum.ResendAccountSignupEmails.Fail)
                        {
                            ModelState.AddModelError("EmailAddress", Resources.EVAResource.problemobtainingyouraccount);
                        }
                        else
                        {

                            viewmodel.Feedbackmessage = @Resources.EVAResource.waitforemailaccountproblems;
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("EmailAddress", Resources.EVAResource.forgottenpassphraseemaildoesntexists);
                }

            }

            string captchacodeused = _CaptchaService.GenerateRandomCode();

            viewmodel.CaptchaEncrypted = captchacodeused;

            try
            {
                ModelState["CaptchaEncrypted"].Value = new ValueProviderResult(captchacodeused.Encrypt(), captchacodeused.Encrypt(), System.Globalization.CultureInfo.CurrentUICulture);
                ModelState["Captcha"].Value = new ValueProviderResult("", "", System.Globalization.CultureInfo.CurrentUICulture);
            }
            catch (Exception err)
            { }

            return View(viewmodel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePassphrase(UpdatePassPhraseAccountViewModel viewmodel)
        {
            Log.Debug(string.Format("AccountController ActionResult UpdatePassphrase() {0} ", ""));

            if (String.IsNullOrEmpty(viewmodel.P1) || string.IsNullOrEmpty(viewmodel.P2)) return Content("Invalid parameters");


            if (viewmodel.Captcha.TrimCheckForNull() != viewmodel.EncryptedCaptcha.Decrypt())
            {
                ModelState.AddModelError("Captcha", Resources.EVAResource.invalidacaptchacode);
            }

            if (ModelState.IsValid)
            {
                string username = viewmodel.P1.Decrypt();
                long userId = viewmodel.P2.DecryptToLong();
                string passwordtoken = viewmodel.P3;

                if (!viewmodel.EmailAddress.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                {
                    ModelState.AddModelError("EmailAddress", Resources.EVAResource.theemailaddresseneteredwasnotexpected);
                    return View(viewmodel);
                }

                BusinessEnum.PassphraseCreation creationstatus = _AccountService.UpdatePassPhrase(passwordtoken, viewmodel.EmailAddress, viewmodel.ExistingPassword, userId, viewmodel.NewPassPhrase);

                if (creationstatus == BusinessEnum.PassphraseCreation.PreviouslyUsed)
                {
                    ModelState.AddModelError("NewPassPhrase", Resources.AccountResource.previouslyused);
                }
                else if (creationstatus == BusinessEnum.PassphraseCreation.SameAsExisiting)
                {
                    ModelState.AddModelError("NewPassPhrase", Resources.AccountResource.sameasexisitng);
                }
                else if (creationstatus == BusinessEnum.PassphraseCreation.NotValidPassPhrase)
                {
                    ModelState.AddModelError("NewPassPhrase", Resources.GeneralResource.invalidpassphrasecreation);
                }
                else if (creationstatus == BusinessEnum.PassphraseCreation.Existingpasswordnotmatched)
                {
                    ModelState.AddModelError("ExistingPassword", Resources.GeneralResource.existingpasswordisnotcorrect);
                }
                else if (creationstatus == BusinessEnum.PassphraseCreation.Success)
                {
                    viewmodel.Shell_Homepage = GetBaseURL();
                    viewmodel.ShowPassPhraseUpdateSuccess = true;

                }
                else
                {
                    ModelState.AddModelError("NewPassPhrase", Resources.EVAResource.passphrasefailedtochange);
                }

            }

            string captchacodeused = _CaptchaService.GenerateRandomCode();

            viewmodel.EncryptedCaptcha = captchacodeused;

            try
            {
                ModelState["EncryptedCaptcha"].Value = new ValueProviderResult(captchacodeused.Encrypt(), captchacodeused.Encrypt(), System.Globalization.CultureInfo.CurrentUICulture);
                ModelState["Captcha"].Value = new ValueProviderResult("", "", System.Globalization.CultureInfo.CurrentUICulture);
            }
            catch (Exception err)
            { }

            return View(viewmodel);
        }




        [HttpGet]
        public ActionResult DetermineHomePage()
        {
            Log.Debug(string.Format("AccountController ActionResult DetermineHomePage() {0} ", ""));


            // if (User.IsInRole(_Configuration.RoleDev)) _EmailService.SendDeveloperLoginEmailNotification("Developer login " + User.Identity.Name, "Ecomarathon app", Request.ServerVariables["REMOTE_ADDR"]);

            if (User.IsInRole(_Configuration.RoleSuperUser)) return RedirectToAction("Index", "SuperUser");

            return Content("Unable to process user request");
        }




        #region Helpers



        #endregion
    }
}
