﻿using System;
using MyApp.Business.DomainObjects.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Collections.Generic;
using Rhino.Mocks;
using MyApp.Business.Services;
using OpenPlatform.General.Dal;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Reflection;
using OpenPlatform.General.Extensions;

namespace MyApp.Tests
{

    [TestClass]
    public class ZoneServiceTest : BusinessTestBase
    {

        private Zone _Zone1 = new Zone
        {
            Name = "testzone",
            IsActive = true,
            UserProfileId = 1
        };

        private Zone _Zone2 = new Zone
        {
            Name = "testzone2",
            IsActive = false,
            UserProfileId = 1
        };

        private Zone _Zone3 = new Zone
        {
            Name = "testzone3",
            IsActive = false,
            UserProfileId = 2
        };

        private Zone _Zone4 = new Zone
            {
                Name = "testzone4",
                IsActive = true
            };

        private Zone _Zone5 = new Zone
        {
            Name = "testzone5",
            IsActive = true
        };

        [TestMethod]
        public void WillAddZone()
        {
            BusinessEnum.ZoneAdd result = TestZoneService.Add(_Zone1);
            Assert.IsTrue(result == BusinessEnum.ZoneAdd.Success);
        }

        [TestMethod]
        public void WillAddZone_2()
        {
            BusinessEnum.ZoneAdd result = TestZoneService.Add(_Zone2);
            Assert.IsTrue(result == BusinessEnum.ZoneAdd.Success);
        }


        [TestMethod]
        public void WillGetAllActiveZones()
        {
            WillAddZone();
            WillAddZone_2();
            List<Zone> result = TestZoneService.GetZonesOrderedByName(_Zone1.UserProfileId, false);
            Assert.IsTrue(result.Count == 1);
        }



        [TestMethod]
        public void WillGetAllActiveAndInactiveZones()
        {
            WillAddZone();
            WillAddZone_2();
            List<Zone> result = TestZoneService.GetZonesOrderedByName(_Zone1.UserProfileId, true);
            Assert.IsTrue(result.Count == 2);
        }

        [TestMethod]
        public void WillNotAddZoneDueToDuplication()
        {
            BusinessEnum.ZoneAdd result = TestZoneService.Add(_Zone1);
            result = TestZoneService.Add(_Zone1);
            Assert.IsTrue(result == BusinessEnum.ZoneAdd.DuplicateName);
        }


        [TestMethod]
        public void WillDeleteByZoneId()
        {
            WillAddZone();
            WillAddZone_2();

            int deforedelete = TestZoneService.GetZonesOrderedByName(_Zone2.UserProfileId, true).Count;
            TestZoneService.DeleteByZoneId(_Zone2.Id);
            int afterdelete = TestZoneService.GetZonesOrderedByName(_Zone2.UserProfileId, true).Count;
            Assert.IsTrue(afterdelete == deforedelete - 1);
        }




        private void SetupTestEnvironment()
        {

        }





        [TestCleanup]
        public void ClassCleanUp()
        {

        }

    }
}
