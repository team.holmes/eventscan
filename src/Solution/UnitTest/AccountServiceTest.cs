﻿using System;
using MyApp.Business.DomainObjects.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Collections.Generic;
using Rhino.Mocks;
using MyApp.Business.Services;
using OpenPlatform.General.Dal;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Reflection;
using OpenPlatform.General.Extensions;

namespace MyApp.Tests
{

    [TestClass]
    public class AccountServiceTest : BusinessTestBase
    {

        private UserProfile _User_ID1 = new UserProfile
        {
            EmailAddress = "1russell.holmes@openplatform.co.uk",
            GID = "xxxx",
            Username = "1russell.holmes@openplatform.co.uk",
            Id = 1,
            IsActive = true,
            IsTemporaryPassPhrase = false

        };

        private UserProfile _User_ID2 = new UserProfile
        {
            EmailAddress = "testuser2@openplatform.co.uk",
            GID = "xxxx",
            Username = "testuser2@openplatform.co.uk",
            Id = 2,
            IsActive = true,
            IsTemporaryPassPhrase = true

        };

        private UserProfile _User_ID3 = new UserProfile
        {
            EmailAddress = "tony3.mcKenzie@openplatform.co.uk",
            GID = "xxxx3",
            Username = "tony3.mcKenzie@openplatform.co.uk",
            Id = 3,
            IsActive = true,
            IsTemporaryPassPhrase = false

        };

        private UserProfile _User_ID4 = new UserProfile
        {
            EmailAddress = "tony4.mcKenzie@openplatform.co.uk",
            GID = "xxxx4",
            Username = "tony4.mcKenzie@openplatform.co.uk",
            Id = 4,
            IsActive = true,
            IsTemporaryPassPhrase = false

        };

        private UserProfile _User_ID5 = new UserProfile
        {
            EmailAddress = "tony5.mcKenzie@openplatform.co.uk",
            GID = "xxxx5",
            Username = "tony5.mcKenzie@openplatform.co.uk",
            Id = 5,
            IsActive = true,
            IsTemporaryPassPhrase = false

        };

        private UserProfile _User_ID6 = new UserProfile
        {
            EmailAddress = "tony6@openplatform.co.uk",
            GID = "xxxx6",
            Username = "tony6@openplatform.co.uk",
            Id = 6,
            IsActive = false,
            IsTemporaryPassPhrase = false

        };

        private UserProfile _User_ID7 = new UserProfile
        {
            EmailAddress = "tony7@openplatform.co.uk",
            GID = "xxxx6",
            Username = "tony7@openplatform.co.uk",
            Id = 7,
            IsActive = true,
            IsTemporaryPassPhrase = false
        };

        private UserProfile _User_ID8 = new UserProfile
        {
            EmailAddress = "tony8@openplatform.co.uk",
            GID = "xxxx6",
            Username = "tony8@openplatform.co.uk",
            Id = 8,
            IsActive = true,
            IsTemporaryPassPhrase = false
        };

        private UserProfile _User_ID9 = new UserProfile
        {
            EmailAddress = "tony9@openplatform.co.uk",
            GID = "xxxx6",
            Username = "tony9@openplatform.co.uk",
            Id = 9,
            IsActive = true,
            IsTemporaryPassPhrase = false
        };

        private MembershipTable _User_ID1Membership = new MembershipTable
        {
            CreateDate = DateTime.UtcNow,
            PasswordFailuresSinceLastSuccess = 0,
            Id = 1,
            Password = "na",
            PasswordChangedDate = DateTime.UtcNow.AddMonths(-1),
            IsConfirmed = true,
            UserProfileId = 1
        };

        private MembershipTable _User_ID2Membership = new MembershipTable
       {
           CreateDate = DateTime.UtcNow,
           PasswordFailuresSinceLastSuccess = 0,
           Id = 2,
           Password = "na",
           PasswordChangedDate = DateTime.UtcNow.AddMonths(-13),
           IsConfirmed = true,
           UserProfileId = 2
       };

        private MembershipTable _User_ID3Membership = new MembershipTable
        {
            CreateDate = DateTime.UtcNow,
            PasswordFailuresSinceLastSuccess = 7,
            Id = 3,
            Password = "na",
            PasswordChangedDate = DateTime.UtcNow.AddMonths(-5),
            LastPasswordFailureDate = DateTime.UtcNow.AddMinutes(-14),
            IsConfirmed = true,
            UserProfileId = 3
        };

        private MembershipTable _User_ID4Membership = new MembershipTable
        {
            CreateDate = DateTime.UtcNow,
            PasswordFailuresSinceLastSuccess = 1,
            Id = 4,
            Password = "na",
            PasswordChangedDate = DateTime.UtcNow.AddMonths(-13),
            LastPasswordFailureDate = DateTime.UtcNow.AddMinutes(-14),
            IsConfirmed = true,
            UserProfileId = 4

        };

        private MembershipTable _User_ID5Membership = new MembershipTable
        {
            CreateDate = DateTime.UtcNow,
            PasswordFailuresSinceLastSuccess = 7,
            Id = 5,
            Password = "na",
            PasswordChangedDate = DateTime.UtcNow.AddMonths(-5),
            LastPasswordFailureDate = DateTime.UtcNow.AddMinutes(-20),
            IsConfirmed = true,
            UserProfileId = 5
        };

        private MembershipTable _User_ID6Membership = new MembershipTable
        {
            CreateDate = DateTime.UtcNow,
            PasswordFailuresSinceLastSuccess = 1,
            Id = 6,
            Password = "na",
            PasswordChangedDate = DateTime.UtcNow.AddMonths(-1),
            LastPasswordFailureDate = DateTime.UtcNow.AddMinutes(-20),
            IsConfirmed = true,
            UserProfileId = 6
        };

        private MembershipTable _User_ID7Membership = new MembershipTable
        {
            CreateDate = DateTime.UtcNow,
            PasswordFailuresSinceLastSuccess = 1,
            Id = 7,
            Password = "na",
            PasswordChangedDate = DateTime.UtcNow.AddMonths(-1),
            LastPasswordFailureDate = DateTime.UtcNow.AddMinutes(-20),
            IsConfirmed = false,
            UserProfileId = 7
        };

        private MembershipTable _User_ID8Membership = new MembershipTable
        {
            CreateDate = DateTime.UtcNow,
            PasswordFailuresSinceLastSuccess = 1,
            Id = 8,
            Password = "na",
            PasswordChangedDate = DateTime.UtcNow.AddMonths(-1),
            LastPasswordFailureDate = DateTime.UtcNow.AddMinutes(-1),
            IsConfirmed = true,
            UserProfileId = 8,
        };

        private MembershipTable _User_ID9Membership = new MembershipTable
        {
            CreateDate = DateTime.UtcNow,
            PasswordFailuresSinceLastSuccess = 99,
            Id = 9,
            Password = "na",
            PasswordChangedDate = DateTime.UtcNow.AddMonths(-1),
            LastPasswordFailureDate = DateTime.UtcNow.AddMinutes(-1),
            IsConfirmed = true,
            UserProfileId = 9,
        };


        private PassphraseWord _PassWord1 = new PassphraseWord
        {
            Id = 1,
            Group1 = "myword1".Encrypt(),
            Group2 = "myword2".Encrypt(),
            Group3 = "myword3".Encrypt(),
            Group4 = "myword".Encrypt()
        };

        private PassphraseWord _PassWord2 = new PassphraseWord
        {
            Id = 2,
            Group1 = "myword12".Encrypt(),
            Group2 = "myword13".Encrypt(),
            Group3 = "myword14".Encrypt(),
            Group4 = "myword15".Encrypt()
        };

        private PassphraseWord _PassWord3 = new PassphraseWord
        {
            Id = 4,
            Group1 = "myword42".Encrypt(),
            Group2 = "myword43".Encrypt(),
            Group3 = "myword44".Encrypt(),
            Group4 = "myword145".Encrypt()
        };

        private AppConfiguration _AppConfig1 = new AppConfiguration
        {
            Id = 1,
            Key = "FromEmailAddress",
            Value = "test@openplatform.co.uk"
        };

        private AppConfiguration _AppConfig2 = new AppConfiguration
        {
            Id = 1,
            Key = "SmtpSenderName",
            Value = "test"
        };





        private PassphraseHistory _PHistory1 = new PassphraseHistory
        {
            DateOfChange = DateTime.UtcNow.AddMonths(-1),
            Id = 1,
            UserId = 2
        };

        private PassphraseHistory _PHistory2 = new PassphraseHistory
        {
            DateOfChange = DateTime.UtcNow.AddMonths(-2),
            Id = 2,
            UserId = 2
        };

        private PassphraseHistory _PHistory3 = new PassphraseHistory
        {
            DateOfChange = DateTime.UtcNow.AddMonths(-3),
            Id = 3,
            UserId = 3
        };

        private UserInRole _UroleIn1 = new UserInRole
        {
            RoleId = 3,
            UserId = 1
        };

        private UserInRole _UroleIn2 = new UserInRole
        {
            RoleId = 3,
            UserId = 2
        };

        private Role _Urole1 = new Role
        {
            RoleId = 1
        };

        private Role _Urole2 = new Role
        {
            RoleId = 2
        };

        private Role _Urole3 = new Role
        {
            RoleId = 3
        };

        private PassphraseWord ppw1 = new PassphraseWord() { Id = 1, Group1 = "gp1", Group2 = "gp2", Group3 = "gp3", Group4 = "gp4" };

        private PassphraseWord ppw2 = new PassphraseWord() { Id = 2, Group1 = "gp2", Group2 = "gp3", Group3 = "gp4", Group4 = "gp5" };

        private PassphraseWord ppw3 = new PassphraseWord() { Id = 3, Group1 = "1gp2", Group2 = "1gp3", Group3 = "1gp4", Group4 = "1gp5" };

        private UserProfile _Testuserprofile = new UserProfile()
        {
            EmailAddress = "russell@openplatform.co.uk",
            Firstname = "russell",
            GID = "123456",
            IsActive = true,
            IsTemporaryPassPhrase = false,
            Surname = "Holmes",
            Username = "russellusername"
        };



        private List<PassphraseWord> _ListofPassphraseWords;

        private List<UserProfile> _ListofUserProfiles;

        private List<MembershipTable> _ListofMemberships;

        private List<PassphraseHistory> _ListofPassHistory;

        private List<UserInRole> _ListOfUserinRoles;

        private List<Role> _ListofRoles;


        [TestInitialize]
        public void ClassInitialise()
        {

            SetupTestEnvironment();
          
        }



        [TestMethod]
        public void WillNotLoginNonExistantAccount()
        {
            BusinessEnum.AuthenticationResult result = _TestAccountService.Login("bogus@bogus.com", "account");
            Assert.IsTrue(result == BusinessEnum.AuthenticationResult.Failed);
        }

        [TestMethod]
        public void WIllLoginaValidAccount()
        {
            BusinessEnum.AuthenticationResult result = _TestAccountService.Login(_User_ID1.EmailAddress, "password");
            Assert.IsTrue(result == BusinessEnum.AuthenticationResult.Passed);
        }


        [TestMethod]
        public void WIllFailToLoginWithIncorrectPassword()
        {
            BusinessEnum.AuthenticationResult result = _TestAccountService.Login(_User_ID1.EmailAddress, "incorrectpassword");
            Assert.IsTrue(result == BusinessEnum.AuthenticationResult.Failed);
        }

        [TestMethod]
        public void WillRequireToResetPassPhraseDueToTemporary()
        {
            BusinessEnum.AuthenticationResult result = _TestAccountService.Login(_User_ID2.EmailAddress, "thisismycurrentpassphrase");
            Assert.IsTrue(result == BusinessEnum.AuthenticationResult.TempPasswordIssued);
        }

        [TestMethod]
        public void WillNotLoginAsAccountNotActive()
        {
            BusinessEnum.AuthenticationResult result = _TestAccountService.Login(_User_ID6.EmailAddress, "password");
            Assert.IsTrue(result == BusinessEnum.AuthenticationResult.AccountNotActive);
        }

        [TestMethod]
        public void WillNotLoginAsAccountNotConfirmedOrActivated()
        {
            BusinessEnum.AuthenticationResult result = _TestAccountService.Login(_User_ID7.EmailAddress, "password");
            Assert.IsTrue(result == BusinessEnum.AuthenticationResult.AccountNotConfirmed_Validated);
        }

        [TestMethod]
        public void WillNotLoginALockedAccount()
        {
            BusinessEnum.AuthenticationResult result = _TestAccountService.Login(_User_ID8.EmailAddress, "wrongpassword");
            Assert.IsTrue(result == BusinessEnum.AuthenticationResult.Locked);
        }

        [TestMethod]
        public void WIllRequireToSetNewPassPhrase()
        {

            BusinessEnum.AuthenticationResult result = _TestAccountService.Login(_User_ID9.EmailAddress, "password");
            Assert.IsTrue(result == BusinessEnum.AuthenticationResult.PassphraseExpired);
        }

        [TestMethod]
        public void CannotCreateAccountWithDuplicateEmailAddress()
        {
            BusinessEnum.UnconfirmedTeamCreateAccountIssues result = _TestAccountService.CreateNewAccount(_User_ID1.EmailAddress, "password", Configuration.RoleUserID, true);
            Assert.IsTrue(result == BusinessEnum.UnconfirmedTeamCreateAccountIssues.EmailAlreadyExists);
        }

        [TestMethod]
        public void CannotCreateAccountWithInvalidPassphrase()
        {
            BusinessEnum.UnconfirmedTeamCreateAccountIssues result = _TestAccountService.CreateNewAccount("unique@emailaddress.com", "1", Configuration.RoleUserID, true);
            Assert.IsTrue(result == BusinessEnum.UnconfirmedTeamCreateAccountIssues.InvalidPassphrase);
        }

        [TestMethod]
        public void WillCreateAccountWIthValidEmailAndPassPhrase()
        {
            string emailaddress = "addedunique@emailaddress.com";
            BusinessEnum.UnconfirmedTeamCreateAccountIssues result = _TestAccountService.CreateNewAccount(emailaddress, "thisisagoodpassphrase", Configuration.RoleUserID, true);
            UserProfile selecteduser = _TestAccountService.GetUserByEmailAddress(emailaddress);
            Assert.IsTrue(selecteduser.EmailAddress == emailaddress);
        }

        [TestMethod]
        public void WillNotResetPassPhraseForNonExistentUser()
        {
            string emailaddress = "nonexistent@emailaddress.com";
            // _MockAccountService.Expect(x => x.GenerateRandomPassPhraseFromDatabase()).Return("xxx-xxx-xxx-xxx-xxx");
            BusinessEnum.PassphraseReset result = _TestAccountService.EmailTemporaryPassPhrase(emailaddress, "base url");
            Assert.IsTrue(result == BusinessEnum.PassphraseReset.NoSuchUser);
        }

        [TestMethod]
        public void WillNotPasswordResetAnUnconfirmedAccount()
        {
            string emailaddress = "nonexistent@emailaddress.com";
            BusinessEnum.PassphraseReset result = _TestAccountService.EmailTemporaryPassPhrase(_User_ID7.EmailAddress, "base url");
            Assert.IsTrue(result == BusinessEnum.PassphraseReset.Accountnotvalidated);
        }

        [TestMethod]
        public void WillResetPassPhrase()
        {
            BusinessEnum.PassphraseReset result = _TestAccountService.EmailTemporaryPassPhrase(_User_ID2.EmailAddress, "base url");
            Assert.IsTrue(result == BusinessEnum.PassphraseReset.Success);
        }



        [TestMethod]
        public void WillRequireTOResetPassphrase()
        {
            BusinessEnum.PassphraseReset result = _TestAccountService.EmailTemporaryPassPhrase(_User_ID2.EmailAddress, "base url");
            Assert.IsTrue(result == BusinessEnum.PassphraseReset.Success);
        }

        [TestMethod]
        public void CannotChangePassPhraseAsPassphrasePreviouslyUsed()
        {
            BusinessEnum.PassphraseCreation result = _TestAccountService.UpdatePassPhrase("TBD", _User_ID2.EmailAddress, "password", _User_ID2.Id, "existingpassphrase");
            Assert.IsTrue(result == BusinessEnum.PassphraseCreation.PreviouslyUsed);
        }

        [TestMethod]
        public void CanotChangePassPhraseTooShort()
        {
            BusinessEnum.PassphraseCreation result = _TestAccountService.UpdatePassPhrase("TBD", _User_ID2.EmailAddress, "password", _User_ID2.Id, "short");
            Assert.IsTrue(result == BusinessEnum.PassphraseCreation.NotValidPassPhrase);
        }


        [TestMethod]
        public void CanotChangePassPhraseSameAsExisting()
        {
            BusinessEnum.PassphraseCreation result = _TestAccountService.UpdatePassPhrase("TBD", _User_ID2.EmailAddress, "thisismycurrentpassphrase", _User_ID2.Id, "thisismycurrentpassphrase");
            Assert.IsTrue(result == BusinessEnum.PassphraseCreation.SameAsExisiting);
        }

        [TestMethod]
        public void CanotChangePassPhraseNotValidNewPassphrase()
        {
            BusinessEnum.PassphraseCreation result = _TestAccountService.UpdatePassPhrase("TBD", _User_ID2.EmailAddress, "thisismycurrentpassphrase", _User_ID2.Id, "invalid");
            Assert.IsTrue(result == BusinessEnum.PassphraseCreation.NotValidPassPhrase);
        }

        [TestMethod]
        public void CanotChangePassPhraseExistingPassphrtaseNotCorrect()
        {
            BusinessEnum.PassphraseCreation result = _TestAccountService.UpdatePassPhrase("TBD", _User_ID2.EmailAddress, "notcorrectexisting", _User_ID2.Id, "thisisanewvalidpassphrase");
            Assert.IsTrue(result == BusinessEnum.PassphraseCreation.Existingpasswordnotmatched);
        }

        [TestMethod]
        public void CanUpdatePassphrase()
        {
            BusinessEnum.PassphraseCreation result = _TestAccountService.UpdatePassPhrase("TBD", _User_ID2.EmailAddress, "thisismycurrentpassphrase", _User_ID2.Id, "thisismycurrentpassphraseupdated");
            Assert.IsTrue(result == BusinessEnum.PassphraseCreation.Success);
        }

        private void SetupTestEnvironment()
        {
            _User_ID1.MembershipTables = new List<MembershipTable>();
            _User_ID2.MembershipTables = new List<MembershipTable>();
            _User_ID3.MembershipTables = new List<MembershipTable>();
            _User_ID4.MembershipTables = new List<MembershipTable>();
            _User_ID5.MembershipTables = new List<MembershipTable>();
            _User_ID6.MembershipTables = new List<MembershipTable>();
            _User_ID7.MembershipTables = new List<MembershipTable>();
            _User_ID8.MembershipTables = new List<MembershipTable>();
            _User_ID9.MembershipTables = new List<MembershipTable>();

            _User_ID1Membership.HashedPassphrase = _TestAccountService.GetHashedPassPhraseForString("password");
            _User_ID2Membership.HashedPassphrase = _TestAccountService.GetHashedPassPhraseForString("thisismycurrentpassphrase");
            _User_ID3Membership.HashedPassphrase = _TestAccountService.GetHashedPassPhraseForString("password");
            _User_ID4Membership.HashedPassphrase = _TestAccountService.GetHashedPassPhraseForString("password");
            _User_ID5Membership.HashedPassphrase = _TestAccountService.GetHashedPassPhraseForString("password");
            _User_ID6Membership.HashedPassphrase = _TestAccountService.GetHashedPassPhraseForString("password");
            _User_ID7Membership.HashedPassphrase = _TestAccountService.GetHashedPassPhraseForString("password");
            _User_ID8Membership.HashedPassphrase = _TestAccountService.GetHashedPassPhraseForString("password");
            _User_ID9Membership.HashedPassphrase = _TestAccountService.GetHashedPassPhraseForString("password");

            _User_ID8Membership.PasswordFailuresSinceLastSuccess = Configuration.MaximumNumberOfFailedLoginAttempts;
            _User_ID9Membership.PasswordChangedDate = DateTime.UtcNow.AddMonths((Configuration.MonthsPassphraseisValidFor + 1) * -1);


            _User_ID1.MembershipTables.Add(_User_ID1Membership);
            _User_ID2.MembershipTables.Add(_User_ID2Membership);
            _User_ID3.MembershipTables.Add(_User_ID3Membership);
            _User_ID4.MembershipTables.Add(_User_ID4Membership);
            _User_ID5.MembershipTables.Add(_User_ID5Membership);
            _User_ID6.MembershipTables.Add(_User_ID6Membership);
            _User_ID7.MembershipTables.Add(_User_ID7Membership);
            _User_ID8.MembershipTables.Add(_User_ID8Membership);
            _User_ID9.MembershipTables.Add(_User_ID9Membership);

            _TestRepository.Add(_User_ID1);
            _TestRepository.Add(_User_ID2);
            _TestRepository.Add(_User_ID3);
            _TestRepository.Add(_User_ID4);
            _TestRepository.Add(_User_ID5);
            _TestRepository.Add(_User_ID6);
            _TestRepository.Add(_User_ID7);
            _TestRepository.Add(_User_ID8);
            _TestRepository.Add(_User_ID9);

            _TestRepository.Add(_PassWord1);
            _TestRepository.Add(_PassWord2);
            _TestRepository.Add(_PassWord3);

            _TestRepository.Add(_AppConfig2);
            _TestRepository.Add(_AppConfig1);


            _PHistory1.PassphraseHash = _TestAccountService.GetHashedPassPhraseForString("existingpassphrase");
            _PHistory2.PassphraseHash = _TestAccountService.GetHashedPassPhraseForString("existingpassphrase1");
            _PHistory3.PassphraseHash = _TestAccountService.GetHashedPassPhraseForString("anotheruser");

            _TestRepository.Add(_PHistory1);
            _TestRepository.Add(_PHistory2);
            _TestRepository.Add(_PHistory3);

            // mock the smtp service
            MockSMTPService.Expect(x => x.Send("xx", "xx", "xx", "xx", "xx", null, true)).Return(true).IgnoreArguments();



        }





        [TestCleanup]
        public void ClassCleanUp()
        {
          
        }

    }
}
