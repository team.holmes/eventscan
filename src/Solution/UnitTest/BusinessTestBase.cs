﻿using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenPlatform.General.Dal;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyApp.Business.Services;
using MyApp.Business.DomainObjects.Models;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Reflection;


namespace MyApp.Tests
{
    public class BusinessTestBase
    {

        public IRepository _RepositoryMock;


        protected IConfiguration MockConfiguration;
        protected IConfiguration Configuration;

        protected IAppConfigurationService TestAppConfigurationService;
        protected IAppConfigurationService MockAppConfigurationService;



        protected ISmtpService SmtpService;
        protected ISmtpService MockSmtpService;

        protected IQRCodeService _TestQRCodeService;
        protected IQRCodeService _MockQRCodeService;

        protected IEmailService TestEmailService;
        protected IEmailService MockEmailService;

        protected ISmtpService TestSMTPService;
        protected ISmtpService MockSMTPService;

        protected IZoneService TestZoneService;
        protected IZoneService MockZoneService;

        protected IUnregisteredService TestUnregisteredService;
        protected IUnregisteredService MockUnregisteredService;

        protected IZoneEngagementService TestZoneEngagementService;
        protected IZoneEngagementService MockZoneEngagementService;


        protected ILog _TestLog;
        protected ILog _MockLog;


        protected IAccountService _TestAccountService;
        protected IAccountService _MockAccountService;

        protected IUserService _MockUserService;
        protected IUserService _UserService;


        protected ISessionService _TestSessionService;
        protected ISessionService _MockSessionService;


        protected ICaptchaService _CaptchaService;

        protected IRepository _TestRepository;

        public BusinessTestBase()
        {
            // setup the mock session
            // Step 1: Setup the HTTP Request
            // see http://stackoverflow.com/questions/9624242/setting-the-httpcontext-current-session-in-unit-test
            var httpRequest = new HttpRequest("", "http://localhost/", "");

            // Step 2: Setup the HTTP Response
            var httpResponce = new HttpResponse(new StringWriter());

            // Step 3: Setup the Http Context
            var httpContext = new HttpContext(httpRequest, httpResponce);
            var sessionContainer =
                new HttpSessionStateContainer("id",
                                               new SessionStateItemCollection(),
                                               new HttpStaticObjectsCollection(),
                                               10,
                                               true,
                                               HttpCookieMode.AutoDetect,
                                               SessionStateMode.InProc,
                                               false);
            httpContext.Items["AspSession"] =
                typeof(HttpSessionState)
                .GetConstructor(
                                    BindingFlags.NonPublic | BindingFlags.Instance,
                                    null,
                                    CallingConventions.Standard,
                                    new[] { typeof(HttpSessionStateContainer) },
                                    null)
                .Invoke(new object[] { sessionContainer });

            // Step 4: Assign the Context
            HttpContext.Current = httpContext;
            //



            ///
            //real

            _TestLog = new MyApp.Business.DomainObjects.Models.Log();

            IUnityContainer container = new UnityContainer();

            MyApp.Business.Services.UnityBuilder.ConfigureContainer(container);

            _TestRepository = new TestRepository();

            _RepositoryMock = MockRepository.GenerateMock<IRepository>();
            _MockAccountService = MockRepository.GenerateMock<IAccountService>();

            MockConfiguration = MockRepository.GenerateMock<IConfiguration>();

            TestAppConfigurationService = new AppConfigurationService(_TestRepository, _TestLog);
            MockAppConfigurationService = MockRepository.GenerateMock<IAppConfigurationService>();
            Configuration = new Configuration(TestAppConfigurationService, _MockLog);





            // _TestSessionService = new SessionService(HttpContext.Current, Configuration, _TestLog);

            TestSMTPService = new SmtpService(_TestLog, Configuration);
            MockSMTPService = MockRepository.GenerateMock<ISmtpService>();


            _TestQRCodeService = new QRCodeService(_TestLog, Configuration);

            TestEmailService = new EmailService(MockSMTPService, Configuration, _TestLog, _TestRepository, _TestQRCodeService);

            _MockAccountService = new AccountService(_RepositoryMock, _MockLog, Configuration, TestEmailService); //
            _TestAccountService = new AccountService(_TestRepository, _TestLog, Configuration, TestEmailService); //

            _CaptchaService = new CaptchaService(_TestRepository, _TestLog, Configuration);

            TestZoneEngagementService = new ZoneEngagementService(Configuration, _TestLog, _TestRepository);

            TestZoneService = new ZoneService(Configuration, _TestLog, _TestRepository);

            TestUnregisteredService = new UnregisteredService(Configuration, _TestLog, _TestRepository);
        }
    }
}
