﻿using System;
using MyApp.Business.DomainObjects.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Rhino.Mocks;

namespace MyApp.Tests
{



    [TestClass]
    public class RegistrationFormTest : BusinessTestBase
    {

        public static EventTeam Eventeam1 = new EventTeam
          {
              EventId = 1,
              Id = 1,
              Phase1StatusId = (int)BusinessEnum.PhaseStatus.Undefined,
              Phase2StatusId = (int)BusinessEnum.PhaseStatus.Undefined,
              TeamId = 1
          };

        public static EventTeam Eventeam2 = new EventTeam
        {
            EventId = 1,
            Id = 2,
            Phase1StatusId = (int)BusinessEnum.PhaseStatus.Submitted,
            Phase2StatusId = (int)BusinessEnum.PhaseStatus.Undefined,
            TeamId = 1
        };

        public static EventTeam Eventeam3 = new EventTeam
        {
            EventId = 1,
            Id = 3,
            Phase1StatusId = (int)BusinessEnum.PhaseStatus.Approved,
            Phase2StatusId = (int)BusinessEnum.PhaseStatus.Undefined,
            TeamId = 1
        };

        public static EventTeam Eventeam4 = new EventTeam
        {
            EventId = 1,
            Id = 4,
            Phase1StatusId = (int)BusinessEnum.PhaseStatus.InReview,
            Phase2StatusId = (int)BusinessEnum.PhaseStatus.Undefined,
            TeamId = 1
        };

        public static List<EventTeam> ListofEventTeams = new List<EventTeam>() { Eventeam1, Eventeam2, Eventeam3, Eventeam4 };

        [TestInitialize]
        public void ClassInitialise()
        {

        }

        [TestCleanup]
        public void ClassCleanUp()
        {
            //TestRepository.GetAll<User>()
            //    .Select(x => x.Id)
            //    .ToList()
            //    .ForEach(x =>
            //    {
            //        TestRepository.DeleteById<User>(x);
            //    });
        }


        [TestMethod]
        public void CanPartiallySaveOrSubmitUndefinedPhase1Status()
        {
            SetupTestEnvironment();

            Boolean result = _MockRegistrationFormService.CanPartiallySaveOrSubmitPhase1Form(Eventeam1.Id);

            Assert.IsTrue(result == true);
        }

        [TestMethod]
        public void CanPartiallySaveOrSubmitSubmittedPhase1Status()
        {
            SetupTestEnvironment();

            Boolean result = _MockRegistrationFormService.CanPartiallySaveOrSubmitPhase1Form(Eventeam2.Id);

            Assert.IsTrue(result == true);
        }

        [TestMethod]
        public void WillNotPartiallySaveOrSubmitApprovedPhase1RegistrationForm()
        {
            SetupTestEnvironment();

            Boolean result = _MockRegistrationFormService.CanPartiallySaveOrSubmitPhase1Form(Eventeam3.Id);

            Assert.IsTrue(result == false);
        }

        [TestMethod]
        public void WillNotPartiallySaveOrSubmitInReviewPhase1RegistrationForm()
        {
            SetupTestEnvironment();

            Boolean result = _MockRegistrationFormService.CanPartiallySaveOrSubmitPhase1Form(Eventeam4.Id);

            Assert.IsTrue(result == false);
        }

     

      



        private void SetupTestEnvironment()
        {
            _RepositoryMock.Expect(x => x.GetAll<EventTeam>("xx")).Return(ListofEventTeams).IgnoreArguments();
        }




    }
}
