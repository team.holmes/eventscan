﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System.Web.Routing;



public static class UnitTestHelper
{
    public static void ShouldEqual<T>(this T actualValue, T expectedValue)
    {
        Assert.AreEqual(expectedValue, actualValue);
    }

    public static void ShouldBeRedirectionTo(this ActionResult actionResult, object expectedRouteValues)
    {
        RouteValueDictionary actualValues = ((RedirectToRouteResult)actionResult).RouteValues;
        var expectedValues = new RouteValueDictionary(expectedRouteValues);

        foreach (string key in expectedValues.Keys)
        {
            Assert.AreEqual(expectedValues[key], actualValues[key]);
        }
    }
}