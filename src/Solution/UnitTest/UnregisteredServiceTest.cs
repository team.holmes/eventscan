﻿using System;
using MyApp.Business.DomainObjects.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Collections.Generic;
using Rhino.Mocks;
using MyApp.Business.Services;
using OpenPlatform.General.Dal;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Reflection;
using OpenPlatform.General.Extensions;

namespace MyApp.Tests
{

    [TestClass]
    public class UnregisteredServiceTest : BusinessTestBase
    {

        private Unregistered _Zone1 = new Unregistered
        {
            Name = "testzone",
            IsActive = true,
            UserProfileId = 1
        };

        private Unregistered _Zone2 = new Unregistered
        {
            Name = "testzone2",
            IsActive = false,
            UserProfileId = 1
        };

        private Unregistered _Zone3 = new Unregistered
        {
            Name = "testzone3",
            IsActive = false,
            UserProfileId = 2
        };

        private Unregistered _Zone4 = new Unregistered
            {
                Name = "testzone4",
                IsActive = true,
                UnregisteredCodeIndex = 0,
                MaxUnregisteredCodesAllowed = 10
            };

        private Unregistered _Zone5 = new Unregistered
        {
            Name = "testzone5",
            IsActive = true,
            UnregisteredCodeIndex = 100,
            MaxUnregisteredCodesAllowed = 10
        };

        [TestMethod]
        public void WillAddUnregistered()
        {
            BusinessEnum.UnregisteredAdd result = TestUnregisteredService.Add(_Zone1);
            Assert.IsTrue(result == BusinessEnum.UnregisteredAdd.Success);
        }

        [TestMethod]
        public void WillAddUnregistered_2()
        {
            BusinessEnum.UnregisteredAdd result = TestUnregisteredService.Add(_Zone2);
            Assert.IsTrue(result == BusinessEnum.UnregisteredAdd.Success);
        }

        [TestMethod]
        public void WillGetAllActiveZones()
        {
            WillAddUnregistered();
            WillAddUnregistered_2();
            List<Unregistered> result = TestUnregisteredService.GetUnregisteredOrderedByName(_Zone1.UserProfileId, false);
            Assert.IsTrue(result.Count == 1);
        }



        [TestMethod]
        public void WillGetAllActiveAndInactiveZones()
        {
            WillAddUnregistered();
            WillAddUnregistered_2();
            List<Unregistered> result = TestUnregisteredService.GetUnregisteredOrderedByName(_Zone1.UserProfileId, true);
            Assert.IsTrue(result.Count == 2);
        }

        [TestMethod]
        public void WillNotAddZoneDueToDuplication()
        {
            BusinessEnum.UnregisteredAdd result = TestUnregisteredService.Add(_Zone1);
            result = TestUnregisteredService.Add(_Zone1);
            Assert.IsTrue(result == BusinessEnum.UnregisteredAdd.DuplicateName);
        }


        [TestMethod]
        public void WillDeleteByZoneId()
        {
            WillAddUnregistered();
            WillAddUnregistered_2();

            int deforedelete = TestUnregisteredService.GetUnregisteredOrderedByName(_Zone2.UserProfileId, true).Count;
            TestUnregisteredService.DeleteByUnregisteredId(_Zone2.Id);
            int afterdelete = TestUnregisteredService.GetUnregisteredOrderedByName(_Zone2.UserProfileId, true).Count;
            Assert.IsTrue(afterdelete == deforedelete - 1);
        }

        [TestMethod]
        public void WillNotAddOverMaRequestAmountForUnregisteredQRCodes()
        {
            TestUnregisteredService.Add(_Zone4);
            BusinessEnum.UnregisteredRequestIndex result = TestUnregisteredService.UpdateUnregisteredRequestIndex(10000, _Zone4);
            Assert.IsTrue(result == BusinessEnum.UnregisteredRequestIndex.TooMuchRequested);
        }

        [TestMethod]
        public void WillNotAddUnregisteredQRCOdesToExceedMaxAllowed()
        {
            _Zone5.UnregisteredCodeIndex = Configuration.MaxUnregisteredZonesAllowed - 2;
            TestUnregisteredService.Add(_Zone5);
            BusinessEnum.UnregisteredRequestIndex result = TestUnregisteredService.UpdateUnregisteredRequestIndex(3, _Zone5);
            Assert.IsTrue(result == BusinessEnum.UnregisteredRequestIndex.ExceedMax);
        }

        [TestMethod]
        public void WillAddUnregisteredQRCodesRequest()
        {
            TestUnregisteredService.Add(_Zone4);
            int before = _Zone4.UnregisteredCodeIndex;
            int toadd = 3;
            BusinessEnum.UnregisteredRequestIndex result = TestUnregisteredService.UpdateUnregisteredRequestIndex(toadd, _Zone4);
            Unregistered retzone = TestUnregisteredService.GetUnregisteredById(1);
            Assert.IsTrue(retzone.UnregisteredCodeIndex == before + toadd);
        }


        [TestMethod]
        public void UnregisteredZoneDoesNotHaveAnyEngagements()
        {
            TestUnregisteredService.Add(_Zone4);
            Boolean result = TestUnregisteredService.DoesUnregisteredGroupHaveAnyEngagements(1);
            Assert.IsTrue(result == false);
        }

    

        private void SetupTestEnvironment()
        {

            //_TestRepository.Add(_PHistory1);
            //_TestRepository.Add(_PHistory2);
            //_TestRepository.Add(_PHistory3);

            // mock the smtp service
            // MockSMTPService.Expect(x => x.Send("xx", "xx", "xx", "xx", "xx", null, true)).Return(true).IgnoreArguments();



        }





        [TestCleanup]
        public void ClassCleanUp()
        {

        }

    }
}
