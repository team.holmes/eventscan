﻿using System;
using MyApp.Business.DomainObjects.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
using System.Collections.Generic;
using Rhino.Mocks;
using MyApp.Business.Services;
using OpenPlatform.General.Dal;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Reflection;
using OpenPlatform.General.Extensions;

namespace MyApp.Tests
{

    [TestClass]
    public class ZoneEngagementTest : BusinessTestBase
    {

        private Zone _Zone1;
        private Zone _Zone2;
        private Zone _Zone3;
        private Zone _Zone4;
        private UserProfile _UserProfile1;
        private UserProfile _UserProfile2;

        [TestInitialize]
        public void ClassInitialise()
        {

            SetupTestEnvironment();

        }



        [TestMethod]
        public void WillNotAddInvalidParameterdZoneEntry()
        {
            BusinessEnum.ResponseCodes result = TestZoneEngagementService.AddZoneEngagement(null, "somedata", "");
            Assert.IsTrue(result == BusinessEnum.ResponseCodes.Result_InvalidParamaters);
        }

        [TestMethod]
        public void WillNotAddInvalidParamaterdZoneEntry_1()
        {
            BusinessEnum.ResponseCodes result = TestZoneEngagementService.AddZoneEngagement("somedata", null, "");
            Assert.IsTrue(result == BusinessEnum.ResponseCodes.Result_InvalidParamaters);
        }

        [TestMethod]
        public void WillNotAddINvalidUNprefixedEncryptedParamaters()
        {
            BusinessEnum.ResponseCodes result = TestZoneEngagementService.AddZoneEngagement("RANDOM", "ENCRYPTEDVALUES", "");
            Assert.IsTrue(result == BusinessEnum.ResponseCodes.Result_ZonePrefixIsntCorrect);
        }

        [TestMethod]
        public void WillAddAValidZoneEngagement()
        {
            string qrcode = string.Format("{0}{1}", Configuration.QRCodeUserPrefix, _UserProfile1.Id).Encrypt();
            BusinessEnum.ResponseCodes result = TestZoneEngagementService.AddZoneEngagement(qrcode, _Zone1.Identifier, "");
            Assert.IsTrue(result == BusinessEnum.ResponseCodes.Result_Success);

        }

        [TestMethod]
        public void WIllAddValidUnregisteredZoneEngagement()
        {
            string qrcode = _TestQRCodeService.GenerateUnregisteredEncryptedLink(1, 1, "http://www.google.com");
            BusinessEnum.ResponseCodes result = TestZoneEngagementService.AddZoneEngagement(qrcode, _Zone1.Identifier, "");
            Assert.IsTrue(result == BusinessEnum.ResponseCodes.Result_Success);
        }

        [TestMethod]
        public void WIllNotAddInValidUnregisteredZoneEngagement()
        {
            string qrcode = "rubbishcode";
            BusinessEnum.ResponseCodes result = TestZoneEngagementService.AddZoneEngagement(qrcode, _Zone1.Identifier, "");
            Assert.IsTrue(result == BusinessEnum.ResponseCodes.Result_ZonePrefixIsntCorrect);
        }

        [TestMethod]
        public void WillCheckForDuppicateZonEngagement()
        {
            WillAddAValidZoneEngagement();
            string qrcode = string.Format("{0}{1}", Configuration.QRCodeUserPrefix, _UserProfile1.Id).Encrypt();
            BusinessEnum.ResponseCodes result = TestZoneEngagementService.AddZoneEngagement(qrcode, _Zone1.Identifier, "");
            Assert.IsTrue(result == BusinessEnum.ResponseCodes.Result_Duplication);

        }


        [TestMethod]
        public void WilLCheckZoneExistsInDB()
        {
            WillAddAValidZoneEngagement();
            string qrcode = string.Format("{0}{1}", Configuration.QRCodeUserPrefix, 1).Encrypt();
            BusinessEnum.ResponseCodes result = TestZoneEngagementService.AddZoneEngagement(qrcode, _Zone4.Identifier, "");
            Assert.IsTrue(result == BusinessEnum.ResponseCodes.Result_ZoneDoesntExist);

        }


        [TestMethod]
        public void IsNotUnregisteredQRCode()
        {
            // _Configuration.UnregisteredUrlContains
            Boolean result = TestZoneEngagementService.IsUnregisteredQRCode("rubbitsh");
            Assert.IsTrue(result == false);
        }



        [TestMethod]
        public void IsUnregisteredQRCode()
        {
            Boolean result = TestZoneEngagementService.IsUnregisteredQRCode(Configuration.UnregisteredUrlContains);
            Assert.IsTrue(result == true);
        }


        [TestMethod]
        public void KeyValueMustHaveParamaters()
        {
            BusinessEnum.ResponseCodes result = TestZoneEngagementService.AddKeyValuePair(null,null,null,null);
            Assert.IsTrue(result == BusinessEnum.ResponseCodes.Result_InvalidParamaters);
        }



        [TestMethod]
        public void CanAddKeyValuePair()
        {
            BusinessEnum.ResponseCodes result = TestZoneEngagementService.AddKeyValuePair(null, "uniquekey", "uniquevalue", "2014_02_10_10_10_10");
            Assert.IsTrue(result == BusinessEnum.ResponseCodes.Result_Success);
        }

        [TestMethod]
        public void WillCheckForDUplicatyeKeyValuePair()
        {
            BusinessEnum.ResponseCodes result = TestZoneEngagementService.AddKeyValuePair(null, "uniquekey", "uniquevalue", "2014_02_10_10_10_10");
            result = TestZoneEngagementService.AddKeyValuePair(null, "uniquekey", "uniquevalue", "2014_02_10_10_10_10");
            Assert.IsTrue(result == BusinessEnum.ResponseCodes.Result_KeyValuePairsMustBeUnique);
        }



        private void SetupTestEnvironment()
        {


            _UserProfile1 = new UserProfile()
            {

                Id = 4,
                EmailAddress = "test@openplatform.co.uk",
                Username = "test@openplatform.co.uk"
            };

            _UserProfile2 = new UserProfile()
         {

             Id = 5,
             EmailAddress = "test2@openplatform.co.uk",
             Username = "test2@openplatform.co.uk"
         };

            _Zone1 = new Zone()
           {
               Id = 1,
               IsActive = true,
               Name = "Zone1",
               UserProfileId = _UserProfile1.Id,
               Identifier = string.Format("{0}{1}", Configuration.QRCodeZonePrefix, 1).Encrypt()
           };


            _Zone2 = new Zone()
           {
               Id = 2,
               IsActive = true,
               Name = "Zone2",
               Identifier = string.Format("{0}{1}", Configuration.QRCodeZonePrefix, 2).Encrypt(),
               UserProfileId = _UserProfile1.Id
           };

            _Zone3 = new Zone()
            {
                Id = 3,
                IsActive = true,
                Name = "Zone3",
                Identifier = string.Format("{0}{1}", Configuration.QRCodeZonePrefix, 3).Encrypt(),
                UserProfileId = _UserProfile2.Id
            };

            _Zone4 = new Zone()
            {
                Id = 99,
                IsActive = true,
                Name = "Zone3",
                Identifier = string.Format("{0}{1}", Configuration.QRCodeZonePrefix, 99).Encrypt(),
                UserProfileId = _UserProfile2.Id
            };

            _TestRepository.Add(_Zone1);
            _TestRepository.Add(_Zone2);
            _TestRepository.Add(_Zone3);

            _TestRepository.Add(_UserProfile1);
            _TestRepository.Add(_UserProfile2);



        }

        // TODO : TEST ParseStringFormattedDateTime(string datetime);
        [TestMethod]
        public void InvalidDatesSendFromIPhoneWillReturnTodaysDate()
        {
            DateTime result = TestZoneEngagementService.ParseStringFormattedDateTime(string.Empty);
            Assert.IsTrue(result.Hour == DateTime.Now.Hour);
        }

        [TestMethod]
        public void InvalidDatesSendFromIPhoneWillReturnTodaysDate_1()
        {
            DateTime result = TestZoneEngagementService.ParseStringFormattedDateTime("apples_and_pears");
            Assert.IsTrue(result.Hour == DateTime.Now.Hour);
        }

        [TestMethod]
        public void ValidDatesSendFromIPhoneWillReturnTodaysDate()
        {
            DateTime result = TestZoneEngagementService.ParseStringFormattedDateTime("2011_11_01_10_11_30");
            Assert.IsTrue(result.Hour == 10 && result.Year == 2011 && result.Day == 1);
        }





        [TestCleanup]
        public void ClassCleanUp()
        {

        }

    }
}
