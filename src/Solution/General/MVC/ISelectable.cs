﻿
namespace OpenPlatform.General
{
    public interface ISelectable
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}
