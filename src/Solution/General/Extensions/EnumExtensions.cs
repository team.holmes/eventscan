﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OpenPlatform.General.Extensions
{
    public static class EnumExtensions
    {
        public static int ConvertToInt(this Enum e)
        {
            return Convert.ToInt32(e);
        }

        public static SelectList ToEnumSelectList<TEnum>(this TEnum enumObj) where TEnum : struct, IConvertible
        {
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new { Id = e, Name = e.ToString() };

            return new SelectList(values, "Id", "Name", enumObj);
        }
    }
}
