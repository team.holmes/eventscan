﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace OpenPlatform.General.Extensions
{
    public static class StringExtensions
    {
        public static String TrimforUI(this string input, int numchars, Boolean addtrailing = true)
        {
            try
            {
                string retval = string.Empty;

                string appender = "...";

                if (String.IsNullOrEmpty(input)) return retval;

                if (input.Length < numchars) return input;

                if (input.Length >= numchars) retval = input.Substring(0, numchars);

                if (addtrailing) retval += "...";

                return retval;
            }
            catch (Exception)
            {
                return input;
            }
        }

        public static String TrimCheckForNull(this string input)
        {

            string retval = input;

            if (!String.IsNullOrEmpty(retval))
            {
                retval = retval.Trim();
            }

            return retval;

        }

        public static Boolean IsInteger(this string input)
        {
            bool isNumeric = false;
            if (!String.IsNullOrEmpty(input))
            {
                int n;
                isNumeric = int.TryParse(input, out n);
            }

            return isNumeric;

        }

        public static String ToLowerCheckForNull(this string input)
        {

            string retval = input;

            if (!String.IsNullOrEmpty(retval))
            {
                retval = retval.ToLower();
            }

            return retval;

        }

        public static String ToUpperCheckForNull(this string input)
        {

            string retval = input;

            if (!String.IsNullOrEmpty(retval))
            {
                retval = retval.ToUpper();
            }

            return retval;

        }

        public static Boolean IsNotNullOrEmpty(this string input)
        {

            if (String.IsNullOrEmpty(input)) return false;

            return true;
        }

        public static string ReplaceIfNullOrEmpty(this string input, string replacementtext)
        {

            if (String.IsNullOrEmpty(input))
            {
                input = replacementtext;
                return replacementtext;
            }

            return input;
        }


        public static string Encrypt(this string input)
        {

            if (String.IsNullOrEmpty(input)) return input;

            return Encryption.Encryption.EncryptTripleDES(input, true, true);
        }

        public static string Encrypt(this string input, Boolean addrandomcharacter)
        {

            if (String.IsNullOrEmpty(input)) return input;

            return Encryption.Encryption.EncryptTripleDES(input, true, addrandomcharacter);
        }

        public static string Decrypt(this string input)
        {
            if (String.IsNullOrEmpty(input)) return input;

            return Encryption.Encryption.DecryptTripleDES(input, true, true);
        }

        public static string Decrypt(this string input, Boolean addrandomcharacter)
        {
            if (String.IsNullOrEmpty(input)) return input;

            return Encryption.Encryption.DecryptTripleDES(input, true, addrandomcharacter);
        }

        public static int DecryptToInt(this string input)
        {

            if (String.IsNullOrEmpty(input))
            {
                throw new Exception("Cannot decrypt an empty string");
            }

            string decryptedretval = Encryption.Encryption.DecryptTripleDES(input, true, true);

            return int.Parse(decryptedretval);
        }

        public static int DecryptToInt(this string input, Boolean addrandomcharacter)
        {

            if (String.IsNullOrEmpty(input))
            {
                throw new Exception("Cannot decrypt an empty string");
            }

            string decryptedretval = Encryption.Encryption.DecryptTripleDES(input, true, addrandomcharacter);

            return int.Parse(decryptedretval);
        }

        public static Boolean DecryptToBoolean(this string input)
        {

            if (String.IsNullOrEmpty(input))
            {
                throw new Exception("Cannot decrypt an empty string");
            }

            string decryptedretval = Encryption.Encryption.DecryptTripleDES(input, true, true);

            return Boolean.Parse(decryptedretval);
        }

        public static long DecryptToLong(this string input)
        {

            if (String.IsNullOrEmpty(input))
            {
                throw new Exception("Cannot decrypt an empty string");
            }

            return long.Parse(Encryption.Encryption.DecryptTripleDES(input, true, true));
        }

        public static string[] SplitOnPipe(this string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                throw new Exception("Cannot Split on a empty string");
            }

            string[] obitems = input.Split('|');
            return obitems;
        }

        public static int[] ConvertStringArrayToIntArray(this string[] input)
        {
            if (input == null || input.Length == 0)
            {
                throw new Exception("Cannot Split on a empty string");
            }

            List<int> listofints = new List<int>();
            foreach (string str in input)
            {
                listofints.Add(int.Parse(str));
            }
            return listofints.ToArray();
        }

        public static string RemoveAllInvalidFilenameChars(this string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return input;
            }

            return Path.GetInvalidFileNameChars().Aggregate(input, (current, c) => current.Replace(c.ToString(), string.Empty));


        }

        public static string GetFileExtention(this string input)
        {
            if (!String.IsNullOrEmpty(input))
            {
                return input.Remove(0, input.LastIndexOf('.') + 1);
            }
            else
            {
                return null;
            }

        }

        public static string RemoveFileExtention(this string input)
        {
            if (!String.IsNullOrEmpty(input))
            {
                return input.Remove(input.LastIndexOf('.'), input.Length - input.LastIndexOf('.'));
            }
            else
            {
                return null;
            }
        }

    }
}
