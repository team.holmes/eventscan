﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenPlatform.General.Extensions
{
    public static class IntExtensions
    {


        public static string Encrypt(this int input)
        {

            return Encryption.Encryption.EncryptTripleDES(input.ToString(), true, true);
        }

        public static string Encrypt(this int input, Boolean addrandomcharacter)
        {

            return Encryption.Encryption.EncryptTripleDES(input.ToString(), true, addrandomcharacter);
        }


    }
}
