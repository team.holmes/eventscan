USE master;

if exists (SELECT 1 FROM sys.databases WHERE name = '##DBNAME##')
begin
	ALTER DATABASE ##DBNAME## SET SINGLE_USER WITH ROLLBACK IMMEDIATE 
	drop database ##DBNAME##
end

create database ##DBNAME##